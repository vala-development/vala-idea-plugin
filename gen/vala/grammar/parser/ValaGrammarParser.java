// This is a generated file. Not intended for manual editing.
package vala.grammar.parser;

import com.intellij.lang.PsiBuilder;
import com.intellij.lang.PsiBuilder.Marker;
import static vala.grammar.parser.psi.ValaTypes.*;
import static vala.grammar.parser.ValaParserUtil.*;
import com.intellij.psi.tree.IElementType;
import com.intellij.lang.ASTNode;
import com.intellij.psi.tree.TokenSet;
import com.intellij.lang.PsiParser;

@SuppressWarnings({"SimplifiableIfStatement", "UnusedAssignment"})
public class ValaGrammarParser implements PsiParser {

  public ASTNode parse(IElementType type, PsiBuilder builder) {
    parseLight(type, builder);
    return builder.getTreeBuilt();
  }

  public void parseLight(IElementType type, PsiBuilder builder) {
    boolean result;
    builder = adapt_builder_(type, builder, this, null);
    Marker marker = enter_section_(builder, 0, _COLLAPSE_, null);
    if (type == ACCESS_MODIFIER) {
      result = access_modifier(builder, 0);
    }
    else if (type == ADDITIVE_EXPRESSION) {
      result = additive_expression(builder, 0);
    }
    else if (type == AND_EXPRESSION) {
      result = and_expression(builder, 0);
    }
    else if (type == ARGUMENT) {
      result = argument(builder, 0);
    }
    else if (type == ARGUMENTS) {
      result = arguments(builder, 0);
    }
    else if (type == ARRAY_CREATION_EXPRESSION) {
      result = array_creation_expression(builder, 0);
    }
    else if (type == ARRAY_SIZE) {
      result = array_size(builder, 0);
    }
    else if (type == ARRAY_TYPE) {
      result = array_type(builder, 0);
    }
    else if (type == ASSIGNMENT_OPERATOR) {
      result = assignment_operator(builder, 0);
    }
    else if (type == ATTRIBUTE) {
      result = attribute(builder, 0);
    }
    else if (type == ATTRIBUTE_ARGUMENT) {
      result = attribute_argument(builder, 0);
    }
    else if (type == ATTRIBUTE_ARGUMENTS) {
      result = attribute_arguments(builder, 0);
    }
    else if (type == ATTRIBUTES) {
      result = attributes(builder, 0);
    }
    else if (type == BASE_ACCESS) {
      result = base_access(builder, 0);
    }
    else if (type == BASE_TYPES) {
      result = base_types(builder, 0);
    }
    else if (type == BLOCK) {
      result = block(builder, 0);
    }
    else if (type == BREAK_STATEMENT) {
      result = break_statement(builder, 0);
    }
    else if (type == CAST_OPERATOR) {
      result = cast_operator(builder, 0);
    }
    else if (type == CATCH_CLAUSE) {
      result = catch_clause(builder, 0);
    }
    else if (type == CLASS_DECLARATION) {
      result = class_declaration(builder, 0);
    }
    else if (type == CLASS_MEMBER) {
      result = class_member(builder, 0);
    }
    else if (type == COALESCING_EXPRESSION) {
      result = coalescing_expression(builder, 0);
    }
    else if (type == CONDITIONAL_AND_EXPRESSION) {
      result = conditional_and_expression(builder, 0);
    }
    else if (type == CONDITIONAL_EXPRESSION) {
      result = conditional_expression(builder, 0);
    }
    else if (type == CONDITIONAL_OR_EXPRESSION) {
      result = conditional_or_expression(builder, 0);
    }
    else if (type == CONSTANT_DECLARATION) {
      result = constant_declaration(builder, 0);
    }
    else if (type == CONSTRUCTOR_DECLARATION) {
      result = constructor_declaration(builder, 0);
    }
    else if (type == CONSTRUCTOR_DECLARATION_MODIFIER) {
      result = constructor_declaration_modifier(builder, 0);
    }
    else if (type == CONSTRUCTOR_DECLARATION_MODIFIERS) {
      result = constructor_declaration_modifiers(builder, 0);
    }
    else if (type == CONTINUE_STATEMENT) {
      result = continue_statement(builder, 0);
    }
    else if (type == CREATION_METHOD_DECLARATION) {
      result = creation_method_declaration(builder, 0);
    }
    else if (type == DELEGATE_DECLARATION) {
      result = delegate_declaration(builder, 0);
    }
    else if (type == DELEGATE_DECLARATION_MODIFIER) {
      result = delegate_declaration_modifier(builder, 0);
    }
    else if (type == DELEGATE_DECLARATION_MODIFIERS) {
      result = delegate_declaration_modifiers(builder, 0);
    }
    else if (type == DELETE_STATEMENT) {
      result = delete_statement(builder, 0);
    }
    else if (type == DESTRUCTOR_DECLARATION) {
      result = destructor_declaration(builder, 0);
    }
    else if (type == DO_STATEMENT) {
      result = do_statement(builder, 0);
    }
    else if (type == ELEMENT_ACCESS) {
      result = element_access(builder, 0);
    }
    else if (type == EMBEDDED_STATEMENT) {
      result = embedded_statement(builder, 0);
    }
    else if (type == EMBEDDED_STATEMENT_WITHOUT_BLOCK) {
      result = embedded_statement_without_block(builder, 0);
    }
    else if (type == ENSURES_DECL) {
      result = ensures_decl(builder, 0);
    }
    else if (type == ENUM_DECLARATION) {
      result = enum_declaration(builder, 0);
    }
    else if (type == ENUM_MEMBER) {
      result = enum_member(builder, 0);
    }
    else if (type == ENUM_VALUE) {
      result = enum_value(builder, 0);
    }
    else if (type == ENUM_VALUES) {
      result = enum_values(builder, 0);
    }
    else if (type == EQUALITY_EXPRESSION) {
      result = equality_expression(builder, 0);
    }
    else if (type == ERRORCODE) {
      result = errorcode(builder, 0);
    }
    else if (type == ERRORCODES) {
      result = errorcodes(builder, 0);
    }
    else if (type == ERRORDOMAIN_DECLARATION) {
      result = errordomain_declaration(builder, 0);
    }
    else if (type == EXCLUSIVE_OR_EXPRESSION) {
      result = exclusive_or_expression(builder, 0);
    }
    else if (type == EXPRESSION) {
      result = expression(builder, 0);
    }
    else if (type == EXPRESSION_STATEMENT) {
      result = expression_statement(builder, 0);
    }
    else if (type == FIELD_DECLARATION) {
      result = field_declaration(builder, 0);
    }
    else if (type == FINALLY_CLAUSE) {
      result = finally_clause(builder, 0);
    }
    else if (type == FOR_INITIALIZER) {
      result = for_initializer(builder, 0);
    }
    else if (type == FOR_ITERATOR) {
      result = for_iterator(builder, 0);
    }
    else if (type == FOR_STATEMENT) {
      result = for_statement(builder, 0);
    }
    else if (type == FOREACH_STATEMENT) {
      result = foreach_statement(builder, 0);
    }
    else if (type == IF_STATEMENT) {
      result = if_statement(builder, 0);
    }
    else if (type == IN_EXPRESSION) {
      result = in_expression(builder, 0);
    }
    else if (type == INCLUSIVE_OR_EXPRESSION) {
      result = inclusive_or_expression(builder, 0);
    }
    else if (type == INITIALIZER) {
      result = initializer(builder, 0);
    }
    else if (type == INLINE_ARRAY_TYPE) {
      result = inline_array_type(builder, 0);
    }
    else if (type == INTERFACE_DECLARATION) {
      result = interface_declaration(builder, 0);
    }
    else if (type == INTERFACE_MEMBER) {
      result = interface_member(builder, 0);
    }
    else if (type == LAMBDA_EXPRESSION) {
      result = lambda_expression(builder, 0);
    }
    else if (type == LAMBDA_EXPRESSION_BODY) {
      result = lambda_expression_body(builder, 0);
    }
    else if (type == LAMBDA_EXPRESSION_PARAMS) {
      result = lambda_expression_params(builder, 0);
    }
    else if (type == LITERAL) {
      result = literal(builder, 0);
    }
    else if (type == LOCAL_TUPLE_DECLARATION) {
      result = local_tuple_declaration(builder, 0);
    }
    else if (type == LOCAL_VARIABLE) {
      result = local_variable(builder, 0);
    }
    else if (type == LOCAL_VARIABLE_DECLARATION) {
      result = local_variable_declaration(builder, 0);
    }
    else if (type == LOCAL_VARIABLE_DECLARATIONS) {
      result = local_variable_declarations(builder, 0);
    }
    else if (type == LOCK_STATEMENT) {
      result = lock_statement(builder, 0);
    }
    else if (type == MEMBER) {
      result = member(builder, 0);
    }
    else if (type == MEMBER_ACCESS) {
      result = member_access(builder, 0);
    }
    else if (type == MEMBER_DECLARATION_MODIFIER) {
      result = member_declaration_modifier(builder, 0);
    }
    else if (type == MEMBER_DECLARATION_MODIFIERS) {
      result = member_declaration_modifiers(builder, 0);
    }
    else if (type == MEMBER_INITIALIZER) {
      result = member_initializer(builder, 0);
    }
    else if (type == METHOD_CALL) {
      result = method_call(builder, 0);
    }
    else if (type == METHOD_DECLARATION) {
      result = method_declaration(builder, 0);
    }
    else if (type == MULTIPLICATIVE_EXPRESSION) {
      result = multiplicative_expression(builder, 0);
    }
    else if (type == NAMESPACE_DECLARATION) {
      result = namespace_declaration(builder, 0);
    }
    else if (type == NAMESPACE_MEMBER) {
      result = namespace_member(builder, 0);
    }
    else if (type == OBJECT_CREATION_EXPRESSION) {
      result = object_creation_expression(builder, 0);
    }
    else if (type == OBJECT_INITIALIZER) {
      result = object_initializer(builder, 0);
    }
    else if (type == OBJECT_OR_ARRAY_CREATION_EXPRESSION) {
      result = object_or_array_creation_expression(builder, 0);
    }
    else if (type == PARAMETER) {
      result = parameter(builder, 0);
    }
    else if (type == PARAMETERS) {
      result = parameters(builder, 0);
    }
    else if (type == PARAMETERS_DECL) {
      result = parameters_decl(builder, 0);
    }
    else if (type == POINTER_MEMBER_ACCESS) {
      result = pointer_member_access(builder, 0);
    }
    else if (type == POST_DECREMENT_EXPRESSION) {
      result = post_decrement_expression(builder, 0);
    }
    else if (type == POST_INCREMENT_EXPRESSION) {
      result = post_increment_expression(builder, 0);
    }
    else if (type == PRIMARY_EXPRESSION) {
      result = primary_expression(builder, 0);
    }
    else if (type == PROPERTY_ACCESSOR) {
      result = property_accessor(builder, 0);
    }
    else if (type == PROPERTY_DECLARATION) {
      result = property_declaration(builder, 0);
    }
    else if (type == PROPERTY_DECLARATION_MODIFIER) {
      result = property_declaration_modifier(builder, 0);
    }
    else if (type == PROPERTY_DECLARATION_MODIFIERS) {
      result = property_declaration_modifiers(builder, 0);
    }
    else if (type == PROPERTY_DECLARATION_PART) {
      result = property_declaration_part(builder, 0);
    }
    else if (type == PROPERTY_GET_ACCESSOR) {
      result = property_get_accessor(builder, 0);
    }
    else if (type == PROPERTY_SET_CONSTRUCT_ACCESSOR) {
      result = property_set_construct_accessor(builder, 0);
    }
    else if (type == RELATIONAL_EXPRESSION) {
      result = relational_expression(builder, 0);
    }
    else if (type == REQUIRES_DECL) {
      result = requires_decl(builder, 0);
    }
    else if (type == RETURN_STATEMENT) {
      result = return_statement(builder, 0);
    }
    else if (type == SHIFT_EXPRESSION) {
      result = shift_expression(builder, 0);
    }
    else if (type == SIGNAL_DECLARATION) {
      result = signal_declaration(builder, 0);
    }
    else if (type == SIGNAL_DECLARATION_MODIFIER) {
      result = signal_declaration_modifier(builder, 0);
    }
    else if (type == SIGNAL_DECLARATION_MODIFIERS) {
      result = signal_declaration_modifiers(builder, 0);
    }
    else if (type == SIMPLE_NAME) {
      result = simple_name(builder, 0);
    }
    else if (type == SIZEOF_EXPRESSION) {
      result = sizeof_expression(builder, 0);
    }
    else if (type == SLICE_ARRAY) {
      result = slice_array(builder, 0);
    }
    else if (type == STATEMENT) {
      result = statement(builder, 0);
    }
    else if (type == STATEMENT_EXPRESSION) {
      result = statement_expression(builder, 0);
    }
    else if (type == STRUCT_DECLARATION) {
      result = struct_declaration(builder, 0);
    }
    else if (type == STRUCT_MEMBER) {
      result = struct_member(builder, 0);
    }
    else if (type == SWITCH_SECTION) {
      result = switch_section(builder, 0);
    }
    else if (type == SWITCH_STATEMENT) {
      result = switch_statement(builder, 0);
    }
    else if (type == SYMBOL) {
      result = symbol(builder, 0);
    }
    else if (type == SYMBOL_PART) {
      result = symbol_part(builder, 0);
    }
    else if (type == TEMPLATE) {
      result = template(builder, 0);
    }
    else if (type == THIS_ACCESS) {
      result = this_access(builder, 0);
    }
    else if (type == THROW_STATEMENT) {
      result = throw_statement(builder, 0);
    }
    else if (type == THROWS_PART) {
      result = throws_part(builder, 0);
    }
    else if (type == TRY_STATEMENT) {
      result = try_statement(builder, 0);
    }
    else if (type == TUPLE) {
      result = tuple(builder, 0);
    }
    else if (type == TYPE) {
      result = type(builder, 0);
    }
    else if (type == TYPE_ARGUMENTS) {
      result = type_arguments(builder, 0);
    }
    else if (type == TYPE_DECLARATION_MODIFIER) {
      result = type_declaration_modifier(builder, 0);
    }
    else if (type == TYPE_DECLARATION_MODIFIERS) {
      result = type_declaration_modifiers(builder, 0);
    }
    else if (type == TYPE_PARAMETERS) {
      result = type_parameters(builder, 0);
    }
    else if (type == TYPEOF_EXPRESSION) {
      result = typeof_expression(builder, 0);
    }
    else if (type == UNARY_EXPRESSION) {
      result = unary_expression(builder, 0);
    }
    else if (type == UNARY_OPERATOR) {
      result = unary_operator(builder, 0);
    }
    else if (type == USING_DIRECTIVE) {
      result = using_directive(builder, 0);
    }
    else if (type == WHILE_STATEMENT) {
      result = while_statement(builder, 0);
    }
    else if (type == YIELD_EXPRESSION) {
      result = yield_expression(builder, 0);
    }
    else if (type == YIELD_STATEMENT) {
      result = yield_statement(builder, 0);
    }
    else {
      result = parse_root_(type, builder, 0);
    }
    exit_section_(builder, 0, marker, type, result, true, TRUE_CONDITION);
  }

  protected boolean parse_root_(IElementType type, PsiBuilder builder, int level) {
    return input(builder, level + 1);
  }

  /* ********************************************************** */
  // KW_PRIVATE | KW_PROTECTED | KW_INTERNAL | KW_PUBLIC
  public static boolean access_modifier(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "access_modifier")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<access modifier>");
    result = consumeToken(builder, KW_PRIVATE);
    if (!result) result = consumeToken(builder, KW_PROTECTED);
    if (!result) result = consumeToken(builder, KW_INTERNAL);
    if (!result) result = consumeToken(builder, KW_PUBLIC);
    exit_section_(builder, level, marker, ACCESS_MODIFIER, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // multiplicative_expression ( ( PLUS | MINUS ) multiplicative_expression )*
  public static boolean additive_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "additive_expression")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<additive expression>");
    result = multiplicative_expression(builder, level + 1);
    result = result && additive_expression_1(builder, level + 1);
    exit_section_(builder, level, marker, ADDITIVE_EXPRESSION, result, false, null);
    return result;
  }

  // ( ( PLUS | MINUS ) multiplicative_expression )*
  private static boolean additive_expression_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "additive_expression_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!additive_expression_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "additive_expression_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // ( PLUS | MINUS ) multiplicative_expression
  private static boolean additive_expression_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "additive_expression_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = additive_expression_1_0_0(builder, level + 1);
    result = result && multiplicative_expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // PLUS | MINUS
  private static boolean additive_expression_1_0_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "additive_expression_1_0_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, PLUS);
    if (!result) result = consumeToken(builder, MINUS);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // equality_expression (AND equality_expression)*
  public static boolean and_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "and_expression")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<and expression>");
    result = equality_expression(builder, level + 1);
    result = result && and_expression_1(builder, level + 1);
    exit_section_(builder, level, marker, AND_EXPRESSION, result, false, null);
    return result;
  }

  // (AND equality_expression)*
  private static boolean and_expression_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "and_expression_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!and_expression_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "and_expression_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // AND equality_expression
  private static boolean and_expression_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "and_expression_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, AND);
    result = result && equality_expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // (IDENTIFIER ":")? (KW_REF | KW_OUT)? expression
  public static boolean argument(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "argument")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<argument>");
    result = argument_0(builder, level + 1);
    result = result && argument_1(builder, level + 1);
    result = result && expression(builder, level + 1);
    exit_section_(builder, level, marker, ARGUMENT, result, false, null);
    return result;
  }

  // (IDENTIFIER ":")?
  private static boolean argument_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "argument_0")) return false;
    argument_0_0(builder, level + 1);
    return true;
  }

  // IDENTIFIER ":"
  private static boolean argument_0_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "argument_0_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, IDENTIFIER);
    result = result && consumeToken(builder, COLON);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (KW_REF | KW_OUT)?
  private static boolean argument_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "argument_1")) return false;
    argument_1_0(builder, level + 1);
    return true;
  }

  // KW_REF | KW_OUT
  private static boolean argument_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "argument_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_REF);
    if (!result) result = consumeToken(builder, KW_OUT);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // argument (COMMA argument)*
  public static boolean arguments(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "arguments")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<arguments>");
    result = argument(builder, level + 1);
    result = result && arguments_1(builder, level + 1);
    exit_section_(builder, level, marker, ARGUMENTS, result, false, null);
    return result;
  }

  // (COMMA argument)*
  private static boolean arguments_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "arguments_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!arguments_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "arguments_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // COMMA argument
  private static boolean arguments_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "arguments_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, COMMA);
    result = result && argument(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // (LBRACKET RBRACKET)* (LBRACKET array_size? RBRACKET)? initializer?
  public static boolean array_creation_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "array_creation_expression")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<array creation expression>");
    result = array_creation_expression_0(builder, level + 1);
    result = result && array_creation_expression_1(builder, level + 1);
    result = result && array_creation_expression_2(builder, level + 1);
    exit_section_(builder, level, marker, ARRAY_CREATION_EXPRESSION, result, false, null);
    return result;
  }

  // (LBRACKET RBRACKET)*
  private static boolean array_creation_expression_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "array_creation_expression_0")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!array_creation_expression_0_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "array_creation_expression_0", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // LBRACKET RBRACKET
  private static boolean array_creation_expression_0_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "array_creation_expression_0_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, LBRACKET, RBRACKET);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (LBRACKET array_size? RBRACKET)?
  private static boolean array_creation_expression_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "array_creation_expression_1")) return false;
    array_creation_expression_1_0(builder, level + 1);
    return true;
  }

  // LBRACKET array_size? RBRACKET
  private static boolean array_creation_expression_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "array_creation_expression_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, LBRACKET);
    result = result && array_creation_expression_1_0_1(builder, level + 1);
    result = result && consumeToken(builder, RBRACKET);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // array_size?
  private static boolean array_creation_expression_1_0_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "array_creation_expression_1_0_1")) return false;
    array_size(builder, level + 1);
    return true;
  }

  // initializer?
  private static boolean array_creation_expression_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "array_creation_expression_2")) return false;
    initializer(builder, level + 1);
    return true;
  }

  /* ********************************************************** */
  // expression (COMMA expression)*
  public static boolean array_size(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "array_size")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<array size>");
    result = expression(builder, level + 1);
    result = result && array_size_1(builder, level + 1);
    exit_section_(builder, level, marker, ARRAY_SIZE, result, false, null);
    return result;
  }

  // (COMMA expression)*
  private static boolean array_size_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "array_size_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!array_size_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "array_size_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // COMMA expression
  private static boolean array_size_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "array_size_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, COMMA);
    result = result && expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // LBRACKET (array_size)? RBRACKET (QUESTION)?
  public static boolean array_type(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "array_type")) return false;
    if (!nextTokenIs(builder, LBRACKET)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, LBRACKET);
    result = result && array_type_1(builder, level + 1);
    result = result && consumeToken(builder, RBRACKET);
    result = result && array_type_3(builder, level + 1);
    exit_section_(builder, marker, ARRAY_TYPE, result);
    return result;
  }

  // (array_size)?
  private static boolean array_type_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "array_type_1")) return false;
    array_type_1_0(builder, level + 1);
    return true;
  }

  // (array_size)
  private static boolean array_type_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "array_type_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = array_size(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (QUESTION)?
  private static boolean array_type_3(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "array_type_3")) return false;
    consumeToken(builder, QUESTION);
    return true;
  }

  /* ********************************************************** */
  // ASSIGNMENT | INCR_ASSIGN | DECR_ASSIGN | OR_ASSIGN | AND_ASSIGN | XOR_ASSIGN |
  //                         DIV_ASSIGN | MUL_ASSIGN | MOD_ASSIGN | SHL_ASSIGN | SHR_ASSIGN
  public static boolean assignment_operator(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "assignment_operator")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<assignment operator>");
    result = consumeToken(builder, ASSIGNMENT);
    if (!result) result = consumeToken(builder, INCR_ASSIGN);
    if (!result) result = consumeToken(builder, DECR_ASSIGN);
    if (!result) result = consumeToken(builder, OR_ASSIGN);
    if (!result) result = consumeToken(builder, AND_ASSIGN);
    if (!result) result = consumeToken(builder, XOR_ASSIGN);
    if (!result) result = consumeToken(builder, DIV_ASSIGN);
    if (!result) result = consumeToken(builder, MUL_ASSIGN);
    if (!result) result = consumeToken(builder, MOD_ASSIGN);
    if (!result) result = consumeToken(builder, SHL_ASSIGN);
    if (!result) result = consumeToken(builder, SHR_ASSIGN);
    exit_section_(builder, level, marker, ASSIGNMENT_OPERATOR, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // LBRACKET IDENTIFIER attribute_arguments? RBRACKET
  public static boolean attribute(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "attribute")) return false;
    if (!nextTokenIs(builder, LBRACKET)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, LBRACKET, IDENTIFIER);
    result = result && attribute_2(builder, level + 1);
    result = result && consumeToken(builder, RBRACKET);
    exit_section_(builder, marker, ATTRIBUTE, result);
    return result;
  }

  // attribute_arguments?
  private static boolean attribute_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "attribute_2")) return false;
    attribute_arguments(builder, level + 1);
    return true;
  }

  /* ********************************************************** */
  // IDENTIFIER ASSIGNMENT expression
  public static boolean attribute_argument(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "attribute_argument")) return false;
    if (!nextTokenIs(builder, IDENTIFIER)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, IDENTIFIER, ASSIGNMENT);
    result = result && expression(builder, level + 1);
    exit_section_(builder, marker, ATTRIBUTE_ARGUMENT, result);
    return result;
  }

  /* ********************************************************** */
  // LPAREN (attribute_argument (COMMA attribute_argument)*)? RPAREN
  public static boolean attribute_arguments(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "attribute_arguments")) return false;
    if (!nextTokenIs(builder, LPAREN)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, LPAREN);
    result = result && attribute_arguments_1(builder, level + 1);
    result = result && consumeToken(builder, RPAREN);
    exit_section_(builder, marker, ATTRIBUTE_ARGUMENTS, result);
    return result;
  }

  // (attribute_argument (COMMA attribute_argument)*)?
  private static boolean attribute_arguments_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "attribute_arguments_1")) return false;
    attribute_arguments_1_0(builder, level + 1);
    return true;
  }

  // attribute_argument (COMMA attribute_argument)*
  private static boolean attribute_arguments_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "attribute_arguments_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = attribute_argument(builder, level + 1);
    result = result && attribute_arguments_1_0_1(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (COMMA attribute_argument)*
  private static boolean attribute_arguments_1_0_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "attribute_arguments_1_0_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!attribute_arguments_1_0_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "attribute_arguments_1_0_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // COMMA attribute_argument
  private static boolean attribute_arguments_1_0_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "attribute_arguments_1_0_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, COMMA);
    result = result && attribute_argument(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // attribute*
  public static boolean attributes(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "attributes")) return false;
    Marker marker = enter_section_(builder, level, _NONE_, "<attributes>");
    int pos = current_position_(builder);
    while (true) {
      if (!attribute(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "attributes", pos)) break;
      pos = current_position_(builder);
    }
    exit_section_(builder, level, marker, ATTRIBUTES, true, false, null);
    return true;
  }

  /* ********************************************************** */
  // KW_BASE
  public static boolean base_access(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "base_access")) return false;
    if (!nextTokenIs(builder, KW_BASE)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_BASE);
    exit_section_(builder, marker, BASE_ACCESS, result);
    return result;
  }

  /* ********************************************************** */
  // type (COMMA type)*
  public static boolean base_types(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "base_types")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<base types>");
    result = type(builder, level + 1);
    result = result && base_types_1(builder, level + 1);
    exit_section_(builder, level, marker, BASE_TYPES, result, false, null);
    return result;
  }

  // (COMMA type)*
  private static boolean base_types_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "base_types_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!base_types_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "base_types_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // COMMA type
  private static boolean base_types_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "base_types_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, COMMA);
    result = result && type(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // LCURL statement* RCURL
  public static boolean block(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "block")) return false;
    if (!nextTokenIs(builder, LCURL)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, LCURL);
    result = result && block_1(builder, level + 1);
    result = result && consumeToken(builder, RCURL);
    exit_section_(builder, marker, BLOCK, result);
    return result;
  }

  // statement*
  private static boolean block_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "block_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!statement(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "block_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  /* ********************************************************** */
  // KW_BREAK SEMICOLON
  public static boolean break_statement(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "break_statement")) return false;
    if (!nextTokenIs(builder, KW_BREAK)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, KW_BREAK, SEMICOLON);
    exit_section_(builder, marker, BREAK_STATEMENT, result);
    return result;
  }

  /* ********************************************************** */
  // LPAREN type RPAREN | LPAREN KW_UNOWNED RPAREN | LPAREN KW_OWNED RPAREN | LPAREN KW_WEAK RPAREN
  public static boolean cast_operator(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "cast_operator")) return false;
    if (!nextTokenIs(builder, LPAREN)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = cast_operator_0(builder, level + 1);
    if (!result) result = parseTokens(builder, 0, LPAREN, KW_UNOWNED, RPAREN);
    if (!result) result = parseTokens(builder, 0, LPAREN, KW_OWNED, RPAREN);
    if (!result) result = parseTokens(builder, 0, LPAREN, KW_WEAK, RPAREN);
    exit_section_(builder, marker, CAST_OPERATOR, result);
    return result;
  }

  // LPAREN type RPAREN
  private static boolean cast_operator_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "cast_operator_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, LPAREN);
    result = result && type(builder, level + 1);
    result = result && consumeToken(builder, RPAREN);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // KW_CATCH (LPAREN type IDENTIFIER RPAREN)? block
  public static boolean catch_clause(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "catch_clause")) return false;
    if (!nextTokenIs(builder, KW_CATCH)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_CATCH);
    result = result && catch_clause_1(builder, level + 1);
    result = result && block(builder, level + 1);
    exit_section_(builder, marker, CATCH_CLAUSE, result);
    return result;
  }

  // (LPAREN type IDENTIFIER RPAREN)?
  private static boolean catch_clause_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "catch_clause_1")) return false;
    catch_clause_1_0(builder, level + 1);
    return true;
  }

  // LPAREN type IDENTIFIER RPAREN
  private static boolean catch_clause_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "catch_clause_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, LPAREN);
    result = result && type(builder, level + 1);
    result = result && consumeTokens(builder, 0, IDENTIFIER, RPAREN);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // (access_modifier)? (type_declaration_modifiers)? KW_CLASS symbol (type_arguments)? (COLON base_types)?
  //                     LCURL (class_member)* RCURL
  public static boolean class_declaration(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "class_declaration")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<class declaration>");
    result = class_declaration_0(builder, level + 1);
    result = result && class_declaration_1(builder, level + 1);
    result = result && consumeToken(builder, KW_CLASS);
    result = result && symbol(builder, level + 1);
    result = result && class_declaration_4(builder, level + 1);
    result = result && class_declaration_5(builder, level + 1);
    result = result && consumeToken(builder, LCURL);
    result = result && class_declaration_7(builder, level + 1);
    result = result && consumeToken(builder, RCURL);
    exit_section_(builder, level, marker, CLASS_DECLARATION, result, false, null);
    return result;
  }

  // (access_modifier)?
  private static boolean class_declaration_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "class_declaration_0")) return false;
    class_declaration_0_0(builder, level + 1);
    return true;
  }

  // (access_modifier)
  private static boolean class_declaration_0_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "class_declaration_0_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = access_modifier(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (type_declaration_modifiers)?
  private static boolean class_declaration_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "class_declaration_1")) return false;
    class_declaration_1_0(builder, level + 1);
    return true;
  }

  // (type_declaration_modifiers)
  private static boolean class_declaration_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "class_declaration_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = type_declaration_modifiers(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (type_arguments)?
  private static boolean class_declaration_4(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "class_declaration_4")) return false;
    class_declaration_4_0(builder, level + 1);
    return true;
  }

  // (type_arguments)
  private static boolean class_declaration_4_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "class_declaration_4_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = type_arguments(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (COLON base_types)?
  private static boolean class_declaration_5(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "class_declaration_5")) return false;
    class_declaration_5_0(builder, level + 1);
    return true;
  }

  // COLON base_types
  private static boolean class_declaration_5_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "class_declaration_5_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, COLON);
    result = result && base_types(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (class_member)*
  private static boolean class_declaration_7(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "class_declaration_7")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!class_declaration_7_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "class_declaration_7", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // (class_member)
  private static boolean class_declaration_7_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "class_declaration_7_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = class_member(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // (attributes)? ( class_declaration | struct_declaration | enum_declaration
  //                 | delegate_declaration | method_declaration | signal_declaration | field_declaration
  //                 | constant_declaration | property_declaration | constructor_declaration
  //                 | creation_method_declaration | destructor_declaration )
  public static boolean class_member(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "class_member")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<class member>");
    result = class_member_0(builder, level + 1);
    result = result && class_member_1(builder, level + 1);
    exit_section_(builder, level, marker, CLASS_MEMBER, result, false, null);
    return result;
  }

  // (attributes)?
  private static boolean class_member_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "class_member_0")) return false;
    class_member_0_0(builder, level + 1);
    return true;
  }

  // (attributes)
  private static boolean class_member_0_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "class_member_0_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = attributes(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // class_declaration | struct_declaration | enum_declaration
  //                 | delegate_declaration | method_declaration | signal_declaration | field_declaration
  //                 | constant_declaration | property_declaration | constructor_declaration
  //                 | creation_method_declaration | destructor_declaration
  private static boolean class_member_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "class_member_1")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = class_declaration(builder, level + 1);
    if (!result) result = struct_declaration(builder, level + 1);
    if (!result) result = enum_declaration(builder, level + 1);
    if (!result) result = delegate_declaration(builder, level + 1);
    if (!result) result = method_declaration(builder, level + 1);
    if (!result) result = signal_declaration(builder, level + 1);
    if (!result) result = field_declaration(builder, level + 1);
    if (!result) result = constant_declaration(builder, level + 1);
    if (!result) result = property_declaration(builder, level + 1);
    if (!result) result = constructor_declaration(builder, level + 1);
    if (!result) result = creation_method_declaration(builder, level + 1);
    if (!result) result = destructor_declaration(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // conditional_or_expression (COALESCE coalescing_expression)?
  public static boolean coalescing_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "coalescing_expression")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<coalescing expression>");
    result = conditional_or_expression(builder, level + 1);
    result = result && coalescing_expression_1(builder, level + 1);
    exit_section_(builder, level, marker, COALESCING_EXPRESSION, result, false, null);
    return result;
  }

  // (COALESCE coalescing_expression)?
  private static boolean coalescing_expression_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "coalescing_expression_1")) return false;
    coalescing_expression_1_0(builder, level + 1);
    return true;
  }

  // COALESCE coalescing_expression
  private static boolean coalescing_expression_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "coalescing_expression_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, COALESCE);
    result = result && coalescing_expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // in_expression (BOOL_AND in_expression)*
  public static boolean conditional_and_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "conditional_and_expression")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<conditional and expression>");
    result = in_expression(builder, level + 1);
    result = result && conditional_and_expression_1(builder, level + 1);
    exit_section_(builder, level, marker, CONDITIONAL_AND_EXPRESSION, result, false, null);
    return result;
  }

  // (BOOL_AND in_expression)*
  private static boolean conditional_and_expression_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "conditional_and_expression_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!conditional_and_expression_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "conditional_and_expression_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // BOOL_AND in_expression
  private static boolean conditional_and_expression_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "conditional_and_expression_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, BOOL_AND);
    result = result && in_expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // coalescing_expression (QUESTION expression COLON expression)?
  public static boolean conditional_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "conditional_expression")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<conditional expression>");
    result = coalescing_expression(builder, level + 1);
    result = result && conditional_expression_1(builder, level + 1);
    exit_section_(builder, level, marker, CONDITIONAL_EXPRESSION, result, false, null);
    return result;
  }

  // (QUESTION expression COLON expression)?
  private static boolean conditional_expression_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "conditional_expression_1")) return false;
    conditional_expression_1_0(builder, level + 1);
    return true;
  }

  // QUESTION expression COLON expression
  private static boolean conditional_expression_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "conditional_expression_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, QUESTION);
    result = result && expression(builder, level + 1);
    result = result && consumeToken(builder, COLON);
    result = result && expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // conditional_and_expression (BOOL_OR conditional_and_expression)*
  public static boolean conditional_or_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "conditional_or_expression")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<conditional or expression>");
    result = conditional_and_expression(builder, level + 1);
    result = result && conditional_or_expression_1(builder, level + 1);
    exit_section_(builder, level, marker, CONDITIONAL_OR_EXPRESSION, result, false, null);
    return result;
  }

  // (BOOL_OR conditional_and_expression)*
  private static boolean conditional_or_expression_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "conditional_or_expression_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!conditional_or_expression_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "conditional_or_expression_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // BOOL_OR conditional_and_expression
  private static boolean conditional_or_expression_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "conditional_or_expression_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, BOOL_OR);
    result = result && conditional_and_expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // access_modifier? member_declaration_modifiers? KW_CONST type
  //             IDENTIFIER inline_array_type? (ASSIGNMENT expression)? SEMICOLON
  public static boolean constant_declaration(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "constant_declaration")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<constant declaration>");
    result = constant_declaration_0(builder, level + 1);
    result = result && constant_declaration_1(builder, level + 1);
    result = result && consumeToken(builder, KW_CONST);
    result = result && type(builder, level + 1);
    result = result && consumeToken(builder, IDENTIFIER);
    result = result && constant_declaration_5(builder, level + 1);
    result = result && constant_declaration_6(builder, level + 1);
    result = result && consumeToken(builder, SEMICOLON);
    exit_section_(builder, level, marker, CONSTANT_DECLARATION, result, false, null);
    return result;
  }

  // access_modifier?
  private static boolean constant_declaration_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "constant_declaration_0")) return false;
    access_modifier(builder, level + 1);
    return true;
  }

  // member_declaration_modifiers?
  private static boolean constant_declaration_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "constant_declaration_1")) return false;
    member_declaration_modifiers(builder, level + 1);
    return true;
  }

  // inline_array_type?
  private static boolean constant_declaration_5(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "constant_declaration_5")) return false;
    inline_array_type(builder, level + 1);
    return true;
  }

  // (ASSIGNMENT expression)?
  private static boolean constant_declaration_6(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "constant_declaration_6")) return false;
    constant_declaration_6_0(builder, level + 1);
    return true;
  }

  // ASSIGNMENT expression
  private static boolean constant_declaration_6_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "constant_declaration_6_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, ASSIGNMENT);
    result = result && expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // (constructor_declaration_modifiers)? KW_CONSTRUCT block
  public static boolean constructor_declaration(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "constructor_declaration")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<constructor declaration>");
    result = constructor_declaration_0(builder, level + 1);
    result = result && consumeToken(builder, KW_CONSTRUCT);
    result = result && block(builder, level + 1);
    exit_section_(builder, level, marker, CONSTRUCTOR_DECLARATION, result, false, null);
    return result;
  }

  // (constructor_declaration_modifiers)?
  private static boolean constructor_declaration_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "constructor_declaration_0")) return false;
    constructor_declaration_0_0(builder, level + 1);
    return true;
  }

  // (constructor_declaration_modifiers)
  private static boolean constructor_declaration_0_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "constructor_declaration_0_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = constructor_declaration_modifiers(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // KW_ASYNC | KW_CLASS | KW_EXTERN | KW_INLINE | KW_STATIC
  //                         | KW_ABSTRACT | KW_VIRTUAL | KW_OVERRIDE
  public static boolean constructor_declaration_modifier(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "constructor_declaration_modifier")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<constructor declaration modifier>");
    result = consumeToken(builder, KW_ASYNC);
    if (!result) result = consumeToken(builder, KW_CLASS);
    if (!result) result = consumeToken(builder, KW_EXTERN);
    if (!result) result = consumeToken(builder, KW_INLINE);
    if (!result) result = consumeToken(builder, KW_STATIC);
    if (!result) result = consumeToken(builder, KW_ABSTRACT);
    if (!result) result = consumeToken(builder, KW_VIRTUAL);
    if (!result) result = consumeToken(builder, KW_OVERRIDE);
    exit_section_(builder, level, marker, CONSTRUCTOR_DECLARATION_MODIFIER, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // constructor_declaration_modifier+
  public static boolean constructor_declaration_modifiers(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "constructor_declaration_modifiers")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<constructor declaration modifiers>");
    result = constructor_declaration_modifier(builder, level + 1);
    int pos = current_position_(builder);
    while (result) {
      if (!constructor_declaration_modifier(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "constructor_declaration_modifiers", pos)) break;
      pos = current_position_(builder);
    }
    exit_section_(builder, level, marker, CONSTRUCTOR_DECLARATION_MODIFIERS, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // KW_CONTINUE SEMICOLON
  public static boolean continue_statement(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "continue_statement")) return false;
    if (!nextTokenIs(builder, KW_CONTINUE)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, KW_CONTINUE, SEMICOLON);
    exit_section_(builder, marker, CONTINUE_STATEMENT, result);
    return result;
  }

  /* ********************************************************** */
  // access_modifier? constructor_declaration_modifiers? symbol parameters
  //             throws_part? requires_decl? ensures_decl? (SEMICOLON | block)
  public static boolean creation_method_declaration(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "creation_method_declaration")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<creation method declaration>");
    result = creation_method_declaration_0(builder, level + 1);
    result = result && creation_method_declaration_1(builder, level + 1);
    result = result && symbol(builder, level + 1);
    result = result && parameters(builder, level + 1);
    result = result && creation_method_declaration_4(builder, level + 1);
    result = result && creation_method_declaration_5(builder, level + 1);
    result = result && creation_method_declaration_6(builder, level + 1);
    result = result && creation_method_declaration_7(builder, level + 1);
    exit_section_(builder, level, marker, CREATION_METHOD_DECLARATION, result, false, null);
    return result;
  }

  // access_modifier?
  private static boolean creation_method_declaration_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "creation_method_declaration_0")) return false;
    access_modifier(builder, level + 1);
    return true;
  }

  // constructor_declaration_modifiers?
  private static boolean creation_method_declaration_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "creation_method_declaration_1")) return false;
    constructor_declaration_modifiers(builder, level + 1);
    return true;
  }

  // throws_part?
  private static boolean creation_method_declaration_4(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "creation_method_declaration_4")) return false;
    throws_part(builder, level + 1);
    return true;
  }

  // requires_decl?
  private static boolean creation_method_declaration_5(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "creation_method_declaration_5")) return false;
    requires_decl(builder, level + 1);
    return true;
  }

  // ensures_decl?
  private static boolean creation_method_declaration_6(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "creation_method_declaration_6")) return false;
    ensures_decl(builder, level + 1);
    return true;
  }

  // SEMICOLON | block
  private static boolean creation_method_declaration_7(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "creation_method_declaration_7")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, SEMICOLON);
    if (!result) result = block(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // access_modifier? delegate_declaration_modifiers? KW_DELEGATE type symbol
  //                 type_parameters? parameters throws_part? SEMICOLON
  public static boolean delegate_declaration(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "delegate_declaration")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<delegate declaration>");
    result = delegate_declaration_0(builder, level + 1);
    result = result && delegate_declaration_1(builder, level + 1);
    result = result && consumeToken(builder, KW_DELEGATE);
    result = result && type(builder, level + 1);
    result = result && symbol(builder, level + 1);
    result = result && delegate_declaration_5(builder, level + 1);
    result = result && parameters(builder, level + 1);
    result = result && delegate_declaration_7(builder, level + 1);
    result = result && consumeToken(builder, SEMICOLON);
    exit_section_(builder, level, marker, DELEGATE_DECLARATION, result, false, null);
    return result;
  }

  // access_modifier?
  private static boolean delegate_declaration_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "delegate_declaration_0")) return false;
    access_modifier(builder, level + 1);
    return true;
  }

  // delegate_declaration_modifiers?
  private static boolean delegate_declaration_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "delegate_declaration_1")) return false;
    delegate_declaration_modifiers(builder, level + 1);
    return true;
  }

  // type_parameters?
  private static boolean delegate_declaration_5(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "delegate_declaration_5")) return false;
    type_parameters(builder, level + 1);
    return true;
  }

  // throws_part?
  private static boolean delegate_declaration_7(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "delegate_declaration_7")) return false;
    throws_part(builder, level + 1);
    return true;
  }

  /* ********************************************************** */
  // KW_ASYNC | KW_CLASS | KW_EXTERN | KW_INLINE | KW_ABSTRACT
  //             | KW_VIRTUAL | KW_OVERRIDE | KW_STATIC
  public static boolean delegate_declaration_modifier(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "delegate_declaration_modifier")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<delegate declaration modifier>");
    result = consumeToken(builder, KW_ASYNC);
    if (!result) result = consumeToken(builder, KW_CLASS);
    if (!result) result = consumeToken(builder, KW_EXTERN);
    if (!result) result = consumeToken(builder, KW_INLINE);
    if (!result) result = consumeToken(builder, KW_ABSTRACT);
    if (!result) result = consumeToken(builder, KW_VIRTUAL);
    if (!result) result = consumeToken(builder, KW_OVERRIDE);
    if (!result) result = consumeToken(builder, KW_STATIC);
    exit_section_(builder, level, marker, DELEGATE_DECLARATION_MODIFIER, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // delegate_declaration_modifier+
  public static boolean delegate_declaration_modifiers(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "delegate_declaration_modifiers")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<delegate declaration modifiers>");
    result = delegate_declaration_modifier(builder, level + 1);
    int pos = current_position_(builder);
    while (result) {
      if (!delegate_declaration_modifier(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "delegate_declaration_modifiers", pos)) break;
      pos = current_position_(builder);
    }
    exit_section_(builder, level, marker, DELEGATE_DECLARATION_MODIFIERS, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // KW_DELETE expression SEMICOLON
  public static boolean delete_statement(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "delete_statement")) return false;
    if (!nextTokenIs(builder, KW_DELETE)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_DELETE);
    result = result && expression(builder, level + 1);
    result = result && consumeToken(builder, SEMICOLON);
    exit_section_(builder, marker, DELETE_STATEMENT, result);
    return result;
  }

  /* ********************************************************** */
  // (constructor_declaration_modifiers)? BITWISE_NOT IDENTIFIER LPAREN RPAREN block
  public static boolean destructor_declaration(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "destructor_declaration")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<destructor declaration>");
    result = destructor_declaration_0(builder, level + 1);
    result = result && consumeTokens(builder, 0, BITWISE_NOT, IDENTIFIER, LPAREN, RPAREN);
    result = result && block(builder, level + 1);
    exit_section_(builder, level, marker, DESTRUCTOR_DECLARATION, result, false, null);
    return result;
  }

  // (constructor_declaration_modifiers)?
  private static boolean destructor_declaration_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "destructor_declaration_0")) return false;
    destructor_declaration_0_0(builder, level + 1);
    return true;
  }

  // (constructor_declaration_modifiers)
  private static boolean destructor_declaration_0_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "destructor_declaration_0_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = constructor_declaration_modifiers(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // KW_DO embedded_statement KW_WHILE LPAREN expression RPAREN SEMICOLON
  public static boolean do_statement(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "do_statement")) return false;
    if (!nextTokenIs(builder, KW_DO)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_DO);
    result = result && embedded_statement(builder, level + 1);
    result = result && consumeTokens(builder, 0, KW_WHILE, LPAREN);
    result = result && expression(builder, level + 1);
    result = result && consumeTokens(builder, 0, RPAREN, SEMICOLON);
    exit_section_(builder, marker, DO_STATEMENT, result);
    return result;
  }

  /* ********************************************************** */
  // LBRACKET expression (slice_array)? RBRACKET
  public static boolean element_access(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "element_access")) return false;
    if (!nextTokenIs(builder, LBRACKET)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, LBRACKET);
    result = result && expression(builder, level + 1);
    result = result && element_access_2(builder, level + 1);
    result = result && consumeToken(builder, RBRACKET);
    exit_section_(builder, marker, ELEMENT_ACCESS, result);
    return result;
  }

  // (slice_array)?
  private static boolean element_access_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "element_access_2")) return false;
    element_access_2_0(builder, level + 1);
    return true;
  }

  // (slice_array)
  private static boolean element_access_2_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "element_access_2_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = slice_array(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // block | embedded_statement_without_block
  public static boolean embedded_statement(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "embedded_statement")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<embedded statement>");
    result = block(builder, level + 1);
    if (!result) result = embedded_statement_without_block(builder, level + 1);
    exit_section_(builder, level, marker, EMBEDDED_STATEMENT, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // SEMICOLON | if_statement | switch_statement | while_statement
  //         | do_statement | for_statement | foreach_statement | break_statement | continue_statement
  //         | return_statement | yield_statement | throw_statement | try_statement | lock_statement
  //         | delete_statement | expression_statement
  public static boolean embedded_statement_without_block(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "embedded_statement_without_block")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<embedded statement without block>");
    result = consumeToken(builder, SEMICOLON);
    if (!result) result = if_statement(builder, level + 1);
    if (!result) result = switch_statement(builder, level + 1);
    if (!result) result = while_statement(builder, level + 1);
    if (!result) result = do_statement(builder, level + 1);
    if (!result) result = for_statement(builder, level + 1);
    if (!result) result = foreach_statement(builder, level + 1);
    if (!result) result = break_statement(builder, level + 1);
    if (!result) result = continue_statement(builder, level + 1);
    if (!result) result = return_statement(builder, level + 1);
    if (!result) result = yield_statement(builder, level + 1);
    if (!result) result = throw_statement(builder, level + 1);
    if (!result) result = try_statement(builder, level + 1);
    if (!result) result = lock_statement(builder, level + 1);
    if (!result) result = delete_statement(builder, level + 1);
    if (!result) result = expression_statement(builder, level + 1);
    exit_section_(builder, level, marker, EMBEDDED_STATEMENT_WITHOUT_BLOCK, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // KW_ENSURES LPAREN expression RPAREN (ensures_decl)?
  public static boolean ensures_decl(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "ensures_decl")) return false;
    if (!nextTokenIs(builder, KW_ENSURES)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, KW_ENSURES, LPAREN);
    result = result && expression(builder, level + 1);
    result = result && consumeToken(builder, RPAREN);
    result = result && ensures_decl_4(builder, level + 1);
    exit_section_(builder, marker, ENSURES_DECL, result);
    return result;
  }

  // (ensures_decl)?
  private static boolean ensures_decl_4(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "ensures_decl_4")) return false;
    ensures_decl_4_0(builder, level + 1);
    return true;
  }

  // (ensures_decl)
  private static boolean ensures_decl_4_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "ensures_decl_4_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = ensures_decl(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // (access_modifier)? (type_declaration_modifiers)? KW_ENUM symbol
  //                 LCURL (enum_member)* RCURL
  public static boolean enum_declaration(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "enum_declaration")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<enum declaration>");
    result = enum_declaration_0(builder, level + 1);
    result = result && enum_declaration_1(builder, level + 1);
    result = result && consumeToken(builder, KW_ENUM);
    result = result && symbol(builder, level + 1);
    result = result && consumeToken(builder, LCURL);
    result = result && enum_declaration_5(builder, level + 1);
    result = result && consumeToken(builder, RCURL);
    exit_section_(builder, level, marker, ENUM_DECLARATION, result, false, null);
    return result;
  }

  // (access_modifier)?
  private static boolean enum_declaration_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "enum_declaration_0")) return false;
    enum_declaration_0_0(builder, level + 1);
    return true;
  }

  // (access_modifier)
  private static boolean enum_declaration_0_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "enum_declaration_0_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = access_modifier(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (type_declaration_modifiers)?
  private static boolean enum_declaration_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "enum_declaration_1")) return false;
    enum_declaration_1_0(builder, level + 1);
    return true;
  }

  // (type_declaration_modifiers)
  private static boolean enum_declaration_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "enum_declaration_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = type_declaration_modifiers(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (enum_member)*
  private static boolean enum_declaration_5(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "enum_declaration_5")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!enum_declaration_5_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "enum_declaration_5", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // (enum_member)
  private static boolean enum_declaration_5_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "enum_declaration_5_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = enum_member(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // (attributes? (method_declaration | constant_declaration)) | enum_values
  public static boolean enum_member(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "enum_member")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<enum member>");
    result = enum_member_0(builder, level + 1);
    if (!result) result = enum_values(builder, level + 1);
    exit_section_(builder, level, marker, ENUM_MEMBER, result, false, null);
    return result;
  }

  // attributes? (method_declaration | constant_declaration)
  private static boolean enum_member_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "enum_member_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = enum_member_0_0(builder, level + 1);
    result = result && enum_member_0_1(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // attributes?
  private static boolean enum_member_0_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "enum_member_0_0")) return false;
    attributes(builder, level + 1);
    return true;
  }

  // method_declaration | constant_declaration
  private static boolean enum_member_0_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "enum_member_0_1")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = method_declaration(builder, level + 1);
    if (!result) result = constant_declaration(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // (attributes)? IDENTIFIER (ASSIGNMENT expression)?
  public static boolean enum_value(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "enum_value")) return false;
    if (!nextTokenIs(builder, "<enum value>", IDENTIFIER, LBRACKET)) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<enum value>");
    result = enum_value_0(builder, level + 1);
    result = result && consumeToken(builder, IDENTIFIER);
    result = result && enum_value_2(builder, level + 1);
    exit_section_(builder, level, marker, ENUM_VALUE, result, false, null);
    return result;
  }

  // (attributes)?
  private static boolean enum_value_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "enum_value_0")) return false;
    enum_value_0_0(builder, level + 1);
    return true;
  }

  // (attributes)
  private static boolean enum_value_0_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "enum_value_0_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = attributes(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (ASSIGNMENT expression)?
  private static boolean enum_value_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "enum_value_2")) return false;
    enum_value_2_0(builder, level + 1);
    return true;
  }

  // ASSIGNMENT expression
  private static boolean enum_value_2_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "enum_value_2_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, ASSIGNMENT);
    result = result && expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // enum_value (COMMA enum_value)* COMMA? SEMICOLON?
  public static boolean enum_values(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "enum_values")) return false;
    if (!nextTokenIs(builder, "<enum values>", IDENTIFIER, LBRACKET)) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<enum values>");
    result = enum_value(builder, level + 1);
    result = result && enum_values_1(builder, level + 1);
    result = result && enum_values_2(builder, level + 1);
    result = result && enum_values_3(builder, level + 1);
    exit_section_(builder, level, marker, ENUM_VALUES, result, false, null);
    return result;
  }

  // (COMMA enum_value)*
  private static boolean enum_values_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "enum_values_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!enum_values_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "enum_values_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // COMMA enum_value
  private static boolean enum_values_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "enum_values_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, COMMA);
    result = result && enum_value(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // COMMA?
  private static boolean enum_values_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "enum_values_2")) return false;
    consumeToken(builder, COMMA);
    return true;
  }

  // SEMICOLON?
  private static boolean enum_values_3(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "enum_values_3")) return false;
    consumeToken(builder, SEMICOLON);
    return true;
  }

  /* ********************************************************** */
  // relational_expression (( EQUAL | NOT_EQUAL ) relational_expression)*
  public static boolean equality_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "equality_expression")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<equality expression>");
    result = relational_expression(builder, level + 1);
    result = result && equality_expression_1(builder, level + 1);
    exit_section_(builder, level, marker, EQUALITY_EXPRESSION, result, false, null);
    return result;
  }

  // (( EQUAL | NOT_EQUAL ) relational_expression)*
  private static boolean equality_expression_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "equality_expression_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!equality_expression_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "equality_expression_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // ( EQUAL | NOT_EQUAL ) relational_expression
  private static boolean equality_expression_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "equality_expression_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = equality_expression_1_0_0(builder, level + 1);
    result = result && relational_expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // EQUAL | NOT_EQUAL
  private static boolean equality_expression_1_0_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "equality_expression_1_0_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, EQUAL);
    if (!result) result = consumeToken(builder, NOT_EQUAL);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // (attributes)? IDENTIFIER (ASSIGNMENT expression)?
  public static boolean errorcode(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "errorcode")) return false;
    if (!nextTokenIs(builder, "<errorcode>", IDENTIFIER, LBRACKET)) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<errorcode>");
    result = errorcode_0(builder, level + 1);
    result = result && consumeToken(builder, IDENTIFIER);
    result = result && errorcode_2(builder, level + 1);
    exit_section_(builder, level, marker, ERRORCODE, result, false, null);
    return result;
  }

  // (attributes)?
  private static boolean errorcode_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "errorcode_0")) return false;
    errorcode_0_0(builder, level + 1);
    return true;
  }

  // (attributes)
  private static boolean errorcode_0_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "errorcode_0_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = attributes(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (ASSIGNMENT expression)?
  private static boolean errorcode_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "errorcode_2")) return false;
    errorcode_2_0(builder, level + 1);
    return true;
  }

  // ASSIGNMENT expression
  private static boolean errorcode_2_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "errorcode_2_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, ASSIGNMENT);
    result = result && expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // errorcode (COMMA errorcode)* COMMA?
  public static boolean errorcodes(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "errorcodes")) return false;
    if (!nextTokenIs(builder, "<errorcodes>", IDENTIFIER, LBRACKET)) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<errorcodes>");
    result = errorcode(builder, level + 1);
    result = result && errorcodes_1(builder, level + 1);
    result = result && errorcodes_2(builder, level + 1);
    exit_section_(builder, level, marker, ERRORCODES, result, false, null);
    return result;
  }

  // (COMMA errorcode)*
  private static boolean errorcodes_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "errorcodes_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!errorcodes_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "errorcodes_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // COMMA errorcode
  private static boolean errorcodes_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "errorcodes_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, COMMA);
    result = result && errorcode(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // COMMA?
  private static boolean errorcodes_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "errorcodes_2")) return false;
    consumeToken(builder, COMMA);
    return true;
  }

  /* ********************************************************** */
  // (access_modifier)? (type_declaration_modifiers)? KW_ERRORDOMAIN symbol
  //                 LCURL errorcodes (SEMICOLON (method_declaration)*)? RCURL
  public static boolean errordomain_declaration(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "errordomain_declaration")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<errordomain declaration>");
    result = errordomain_declaration_0(builder, level + 1);
    result = result && errordomain_declaration_1(builder, level + 1);
    result = result && consumeToken(builder, KW_ERRORDOMAIN);
    result = result && symbol(builder, level + 1);
    result = result && consumeToken(builder, LCURL);
    result = result && errorcodes(builder, level + 1);
    result = result && errordomain_declaration_6(builder, level + 1);
    result = result && consumeToken(builder, RCURL);
    exit_section_(builder, level, marker, ERRORDOMAIN_DECLARATION, result, false, null);
    return result;
  }

  // (access_modifier)?
  private static boolean errordomain_declaration_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "errordomain_declaration_0")) return false;
    errordomain_declaration_0_0(builder, level + 1);
    return true;
  }

  // (access_modifier)
  private static boolean errordomain_declaration_0_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "errordomain_declaration_0_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = access_modifier(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (type_declaration_modifiers)?
  private static boolean errordomain_declaration_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "errordomain_declaration_1")) return false;
    errordomain_declaration_1_0(builder, level + 1);
    return true;
  }

  // (type_declaration_modifiers)
  private static boolean errordomain_declaration_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "errordomain_declaration_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = type_declaration_modifiers(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (SEMICOLON (method_declaration)*)?
  private static boolean errordomain_declaration_6(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "errordomain_declaration_6")) return false;
    errordomain_declaration_6_0(builder, level + 1);
    return true;
  }

  // SEMICOLON (method_declaration)*
  private static boolean errordomain_declaration_6_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "errordomain_declaration_6_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, SEMICOLON);
    result = result && errordomain_declaration_6_0_1(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (method_declaration)*
  private static boolean errordomain_declaration_6_0_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "errordomain_declaration_6_0_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!errordomain_declaration_6_0_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "errordomain_declaration_6_0_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // (method_declaration)
  private static boolean errordomain_declaration_6_0_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "errordomain_declaration_6_0_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = method_declaration(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // and_expression (XOR and_expression)?
  public static boolean exclusive_or_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "exclusive_or_expression")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<exclusive or expression>");
    result = and_expression(builder, level + 1);
    result = result && exclusive_or_expression_1(builder, level + 1);
    exit_section_(builder, level, marker, EXCLUSIVE_OR_EXPRESSION, result, false, null);
    return result;
  }

  // (XOR and_expression)?
  private static boolean exclusive_or_expression_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "exclusive_or_expression_1")) return false;
    exclusive_or_expression_1_0(builder, level + 1);
    return true;
  }

  // XOR and_expression
  private static boolean exclusive_or_expression_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "exclusive_or_expression_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, XOR);
    result = result && and_expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // lambda_expression | ( conditional_expression (assignment_operator expression)? )
  public static boolean expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "expression")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<expression>");
    result = lambda_expression(builder, level + 1);
    if (!result) result = expression_1(builder, level + 1);
    exit_section_(builder, level, marker, EXPRESSION, result, false, null);
    return result;
  }

  // conditional_expression (assignment_operator expression)?
  private static boolean expression_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "expression_1")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = conditional_expression(builder, level + 1);
    result = result && expression_1_1(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (assignment_operator expression)?
  private static boolean expression_1_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "expression_1_1")) return false;
    expression_1_1_0(builder, level + 1);
    return true;
  }

  // assignment_operator expression
  private static boolean expression_1_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "expression_1_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = assignment_operator(builder, level + 1);
    result = result && expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // statement_expression SEMICOLON
  public static boolean expression_statement(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "expression_statement")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<expression statement>");
    result = statement_expression(builder, level + 1);
    result = result && consumeToken(builder, SEMICOLON);
    exit_section_(builder, level, marker, EXPRESSION_STATEMENT, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // access_modifier? member_declaration_modifiers? type IDENTIFIER
  //             (LBRACKET RBRACKET)? (ASSIGNMENT expression)? SEMICOLON
  public static boolean field_declaration(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "field_declaration")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<field declaration>");
    result = field_declaration_0(builder, level + 1);
    result = result && field_declaration_1(builder, level + 1);
    result = result && type(builder, level + 1);
    result = result && consumeToken(builder, IDENTIFIER);
    result = result && field_declaration_4(builder, level + 1);
    result = result && field_declaration_5(builder, level + 1);
    result = result && consumeToken(builder, SEMICOLON);
    exit_section_(builder, level, marker, FIELD_DECLARATION, result, false, null);
    return result;
  }

  // access_modifier?
  private static boolean field_declaration_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "field_declaration_0")) return false;
    access_modifier(builder, level + 1);
    return true;
  }

  // member_declaration_modifiers?
  private static boolean field_declaration_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "field_declaration_1")) return false;
    member_declaration_modifiers(builder, level + 1);
    return true;
  }

  // (LBRACKET RBRACKET)?
  private static boolean field_declaration_4(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "field_declaration_4")) return false;
    field_declaration_4_0(builder, level + 1);
    return true;
  }

  // LBRACKET RBRACKET
  private static boolean field_declaration_4_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "field_declaration_4_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, LBRACKET, RBRACKET);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (ASSIGNMENT expression)?
  private static boolean field_declaration_5(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "field_declaration_5")) return false;
    field_declaration_5_0(builder, level + 1);
    return true;
  }

  // ASSIGNMENT expression
  private static boolean field_declaration_5_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "field_declaration_5_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, ASSIGNMENT);
    result = result && expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // KW_FINALLY block
  public static boolean finally_clause(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "finally_clause")) return false;
    if (!nextTokenIs(builder, KW_FINALLY)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_FINALLY);
    result = result && block(builder, level + 1);
    exit_section_(builder, marker, FINALLY_CLAUSE, result);
    return result;
  }

  /* ********************************************************** */
  // local_variable_declarations | (statement_expression (COMMA statement_expression)* SEMICOLON)
  public static boolean for_initializer(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "for_initializer")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<for initializer>");
    result = local_variable_declarations(builder, level + 1);
    if (!result) result = for_initializer_1(builder, level + 1);
    exit_section_(builder, level, marker, FOR_INITIALIZER, result, false, null);
    return result;
  }

  // statement_expression (COMMA statement_expression)* SEMICOLON
  private static boolean for_initializer_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "for_initializer_1")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = statement_expression(builder, level + 1);
    result = result && for_initializer_1_1(builder, level + 1);
    result = result && consumeToken(builder, SEMICOLON);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (COMMA statement_expression)*
  private static boolean for_initializer_1_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "for_initializer_1_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!for_initializer_1_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "for_initializer_1_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // COMMA statement_expression
  private static boolean for_initializer_1_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "for_initializer_1_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, COMMA);
    result = result && statement_expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // statement_expression (COMMA statement_expression)*
  public static boolean for_iterator(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "for_iterator")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<for iterator>");
    result = statement_expression(builder, level + 1);
    result = result && for_iterator_1(builder, level + 1);
    exit_section_(builder, level, marker, FOR_ITERATOR, result, false, null);
    return result;
  }

  // (COMMA statement_expression)*
  private static boolean for_iterator_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "for_iterator_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!for_iterator_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "for_iterator_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // COMMA statement_expression
  private static boolean for_iterator_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "for_iterator_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, COMMA);
    result = result && statement_expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // KW_FOR LPAREN (SEMICOLON | for_initializer) expression? SEMICOLON for_iterator? RPAREN embedded_statement
  public static boolean for_statement(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "for_statement")) return false;
    if (!nextTokenIs(builder, KW_FOR)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, KW_FOR, LPAREN);
    result = result && for_statement_2(builder, level + 1);
    result = result && for_statement_3(builder, level + 1);
    result = result && consumeToken(builder, SEMICOLON);
    result = result && for_statement_5(builder, level + 1);
    result = result && consumeToken(builder, RPAREN);
    result = result && embedded_statement(builder, level + 1);
    exit_section_(builder, marker, FOR_STATEMENT, result);
    return result;
  }

  // SEMICOLON | for_initializer
  private static boolean for_statement_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "for_statement_2")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, SEMICOLON);
    if (!result) result = for_initializer(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // expression?
  private static boolean for_statement_3(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "for_statement_3")) return false;
    expression(builder, level + 1);
    return true;
  }

  // for_iterator?
  private static boolean for_statement_5(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "for_statement_5")) return false;
    for_iterator(builder, level + 1);
    return true;
  }

  /* ********************************************************** */
  // KW_FOREACH LPAREN (type | KW_VAR) IDENTIFIER KW_IN expression RPAREN embedded_statement
  public static boolean foreach_statement(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "foreach_statement")) return false;
    if (!nextTokenIs(builder, KW_FOREACH)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, KW_FOREACH, LPAREN);
    result = result && foreach_statement_2(builder, level + 1);
    result = result && consumeTokens(builder, 0, IDENTIFIER, KW_IN);
    result = result && expression(builder, level + 1);
    result = result && consumeToken(builder, RPAREN);
    result = result && embedded_statement(builder, level + 1);
    exit_section_(builder, marker, FOREACH_STATEMENT, result);
    return result;
  }

  // type | KW_VAR
  private static boolean foreach_statement_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "foreach_statement_2")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = type(builder, level + 1);
    if (!result) result = consumeToken(builder, KW_VAR);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // KW_IF LPAREN expression RPAREN embedded_statement
  //             (KW_ELSE embedded_statement)?
  public static boolean if_statement(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "if_statement")) return false;
    if (!nextTokenIs(builder, KW_IF)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, KW_IF, LPAREN);
    result = result && expression(builder, level + 1);
    result = result && consumeToken(builder, RPAREN);
    result = result && embedded_statement(builder, level + 1);
    result = result && if_statement_5(builder, level + 1);
    exit_section_(builder, marker, IF_STATEMENT, result);
    return result;
  }

  // (KW_ELSE embedded_statement)?
  private static boolean if_statement_5(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "if_statement_5")) return false;
    if_statement_5_0(builder, level + 1);
    return true;
  }

  // KW_ELSE embedded_statement
  private static boolean if_statement_5_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "if_statement_5_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_ELSE);
    result = result && embedded_statement(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // inclusive_or_expression (KW_IN inclusive_or_expression)?
  public static boolean in_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "in_expression")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<in expression>");
    result = inclusive_or_expression(builder, level + 1);
    result = result && in_expression_1(builder, level + 1);
    exit_section_(builder, level, marker, IN_EXPRESSION, result, false, null);
    return result;
  }

  // (KW_IN inclusive_or_expression)?
  private static boolean in_expression_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "in_expression_1")) return false;
    in_expression_1_0(builder, level + 1);
    return true;
  }

  // KW_IN inclusive_or_expression
  private static boolean in_expression_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "in_expression_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_IN);
    result = result && inclusive_or_expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // exclusive_or_expression (OR exclusive_or_expression)*
  public static boolean inclusive_or_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "inclusive_or_expression")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<inclusive or expression>");
    result = exclusive_or_expression(builder, level + 1);
    result = result && inclusive_or_expression_1(builder, level + 1);
    exit_section_(builder, level, marker, INCLUSIVE_OR_EXPRESSION, result, false, null);
    return result;
  }

  // (OR exclusive_or_expression)*
  private static boolean inclusive_or_expression_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "inclusive_or_expression_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!inclusive_or_expression_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "inclusive_or_expression_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // OR exclusive_or_expression
  private static boolean inclusive_or_expression_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "inclusive_or_expression_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, OR);
    result = result && exclusive_or_expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // LCURL (argument (COMMA argument)*)? RCURL
  public static boolean initializer(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "initializer")) return false;
    if (!nextTokenIs(builder, LCURL)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, LCURL);
    result = result && initializer_1(builder, level + 1);
    result = result && consumeToken(builder, RCURL);
    exit_section_(builder, marker, INITIALIZER, result);
    return result;
  }

  // (argument (COMMA argument)*)?
  private static boolean initializer_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "initializer_1")) return false;
    initializer_1_0(builder, level + 1);
    return true;
  }

  // argument (COMMA argument)*
  private static boolean initializer_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "initializer_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = argument(builder, level + 1);
    result = result && initializer_1_0_1(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (COMMA argument)*
  private static boolean initializer_1_0_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "initializer_1_0_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!initializer_1_0_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "initializer_1_0_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // COMMA argument
  private static boolean initializer_1_0_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "initializer_1_0_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, COMMA);
    result = result && argument(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // LBRACKET INTEGER RBRACKET
  public static boolean inline_array_type(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "inline_array_type")) return false;
    if (!nextTokenIs(builder, LBRACKET)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, LBRACKET, INTEGER, RBRACKET);
    exit_section_(builder, marker, INLINE_ARRAY_TYPE, result);
    return result;
  }

  /* ********************************************************** */
  // using_directive* namespace_member*
  static boolean input(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "input")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = input_0(builder, level + 1);
    result = result && input_1(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // using_directive*
  private static boolean input_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "input_0")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!using_directive(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "input_0", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // namespace_member*
  private static boolean input_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "input_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!namespace_member(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "input_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  /* ********************************************************** */
  // access_modifier? type_declaration_modifiers? KW_INTERFACE symbol type_parameters? (COLON base_types)?
  //                 LCURL (interface_member)* RCURL
  public static boolean interface_declaration(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "interface_declaration")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<interface declaration>");
    result = interface_declaration_0(builder, level + 1);
    result = result && interface_declaration_1(builder, level + 1);
    result = result && consumeToken(builder, KW_INTERFACE);
    result = result && symbol(builder, level + 1);
    result = result && interface_declaration_4(builder, level + 1);
    result = result && interface_declaration_5(builder, level + 1);
    result = result && consumeToken(builder, LCURL);
    result = result && interface_declaration_7(builder, level + 1);
    result = result && consumeToken(builder, RCURL);
    exit_section_(builder, level, marker, INTERFACE_DECLARATION, result, false, null);
    return result;
  }

  // access_modifier?
  private static boolean interface_declaration_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "interface_declaration_0")) return false;
    access_modifier(builder, level + 1);
    return true;
  }

  // type_declaration_modifiers?
  private static boolean interface_declaration_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "interface_declaration_1")) return false;
    type_declaration_modifiers(builder, level + 1);
    return true;
  }

  // type_parameters?
  private static boolean interface_declaration_4(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "interface_declaration_4")) return false;
    type_parameters(builder, level + 1);
    return true;
  }

  // (COLON base_types)?
  private static boolean interface_declaration_5(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "interface_declaration_5")) return false;
    interface_declaration_5_0(builder, level + 1);
    return true;
  }

  // COLON base_types
  private static boolean interface_declaration_5_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "interface_declaration_5_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, COLON);
    result = result && base_types(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (interface_member)*
  private static boolean interface_declaration_7(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "interface_declaration_7")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!interface_declaration_7_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "interface_declaration_7", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // (interface_member)
  private static boolean interface_declaration_7_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "interface_declaration_7_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = interface_member(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // attributes? (class_declaration | struct_declaration | enum_declaration
  //             | delegate_declaration | method_declaration | signal_declaration | field_declaration
  //             | constant_declaration | property_declaration)
  public static boolean interface_member(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "interface_member")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<interface member>");
    result = interface_member_0(builder, level + 1);
    result = result && interface_member_1(builder, level + 1);
    exit_section_(builder, level, marker, INTERFACE_MEMBER, result, false, null);
    return result;
  }

  // attributes?
  private static boolean interface_member_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "interface_member_0")) return false;
    attributes(builder, level + 1);
    return true;
  }

  // class_declaration | struct_declaration | enum_declaration
  //             | delegate_declaration | method_declaration | signal_declaration | field_declaration
  //             | constant_declaration | property_declaration
  private static boolean interface_member_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "interface_member_1")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = class_declaration(builder, level + 1);
    if (!result) result = struct_declaration(builder, level + 1);
    if (!result) result = enum_declaration(builder, level + 1);
    if (!result) result = delegate_declaration(builder, level + 1);
    if (!result) result = method_declaration(builder, level + 1);
    if (!result) result = signal_declaration(builder, level + 1);
    if (!result) result = field_declaration(builder, level + 1);
    if (!result) result = constant_declaration(builder, level + 1);
    if (!result) result = property_declaration(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // lambda_expression_params LAMBDA lambda_expression_body
  public static boolean lambda_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "lambda_expression")) return false;
    if (!nextTokenIs(builder, "<lambda expression>", IDENTIFIER, LPAREN)) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<lambda expression>");
    result = lambda_expression_params(builder, level + 1);
    result = result && consumeToken(builder, LAMBDA);
    result = result && lambda_expression_body(builder, level + 1);
    exit_section_(builder, level, marker, LAMBDA_EXPRESSION, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // expression | block
  public static boolean lambda_expression_body(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "lambda_expression_body")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<lambda expression body>");
    result = expression(builder, level + 1);
    if (!result) result = block(builder, level + 1);
    exit_section_(builder, level, marker, LAMBDA_EXPRESSION_BODY, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // IDENTIFIER | (LPAREN (IDENTIFIER (COMMA IDENTIFIER)*)? RPAREN)
  public static boolean lambda_expression_params(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "lambda_expression_params")) return false;
    if (!nextTokenIs(builder, "<lambda expression params>", IDENTIFIER, LPAREN)) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<lambda expression params>");
    result = consumeToken(builder, IDENTIFIER);
    if (!result) result = lambda_expression_params_1(builder, level + 1);
    exit_section_(builder, level, marker, LAMBDA_EXPRESSION_PARAMS, result, false, null);
    return result;
  }

  // LPAREN (IDENTIFIER (COMMA IDENTIFIER)*)? RPAREN
  private static boolean lambda_expression_params_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "lambda_expression_params_1")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, LPAREN);
    result = result && lambda_expression_params_1_1(builder, level + 1);
    result = result && consumeToken(builder, RPAREN);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (IDENTIFIER (COMMA IDENTIFIER)*)?
  private static boolean lambda_expression_params_1_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "lambda_expression_params_1_1")) return false;
    lambda_expression_params_1_1_0(builder, level + 1);
    return true;
  }

  // IDENTIFIER (COMMA IDENTIFIER)*
  private static boolean lambda_expression_params_1_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "lambda_expression_params_1_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, IDENTIFIER);
    result = result && lambda_expression_params_1_1_0_1(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (COMMA IDENTIFIER)*
  private static boolean lambda_expression_params_1_1_0_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "lambda_expression_params_1_1_0_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!lambda_expression_params_1_1_0_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "lambda_expression_params_1_1_0_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // COMMA IDENTIFIER
  private static boolean lambda_expression_params_1_1_0_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "lambda_expression_params_1_1_0_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, COMMA, IDENTIFIER);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // KW_TRUE | KW_FALSE | KW_NULL | INTEGER | REAL | HEX | CHAR | REGEX_LITERAL |
  //             STRING | TEMPLATE_LITERAL | VERBATIM_LITERAL
  public static boolean literal(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "literal")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<literal>");
    result = consumeToken(builder, KW_TRUE);
    if (!result) result = consumeToken(builder, KW_FALSE);
    if (!result) result = consumeToken(builder, KW_NULL);
    if (!result) result = consumeToken(builder, INTEGER);
    if (!result) result = consumeToken(builder, REAL);
    if (!result) result = consumeToken(builder, HEX);
    if (!result) result = consumeToken(builder, CHAR);
    if (!result) result = consumeToken(builder, REGEX_LITERAL);
    if (!result) result = consumeToken(builder, STRING);
    if (!result) result = consumeToken(builder, TEMPLATE_LITERAL);
    if (!result) result = consumeToken(builder, VERBATIM_LITERAL);
    exit_section_(builder, level, marker, LITERAL, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // LPAREN IDENTIFIER (COMMA IDENTIFIER)* RPAREN ASSIGNMENT expression
  public static boolean local_tuple_declaration(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "local_tuple_declaration")) return false;
    if (!nextTokenIs(builder, LPAREN)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, LPAREN, IDENTIFIER);
    result = result && local_tuple_declaration_2(builder, level + 1);
    result = result && consumeTokens(builder, 0, RPAREN, ASSIGNMENT);
    result = result && expression(builder, level + 1);
    exit_section_(builder, marker, LOCAL_TUPLE_DECLARATION, result);
    return result;
  }

  // (COMMA IDENTIFIER)*
  private static boolean local_tuple_declaration_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "local_tuple_declaration_2")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!local_tuple_declaration_2_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "local_tuple_declaration_2", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // COMMA IDENTIFIER
  private static boolean local_tuple_declaration_2_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "local_tuple_declaration_2_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, COMMA, IDENTIFIER);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // IDENTIFIER inline_array_type? (ASSIGNMENT expression)?
  public static boolean local_variable(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "local_variable")) return false;
    if (!nextTokenIs(builder, IDENTIFIER)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, IDENTIFIER);
    result = result && local_variable_1(builder, level + 1);
    result = result && local_variable_2(builder, level + 1);
    exit_section_(builder, marker, LOCAL_VARIABLE, result);
    return result;
  }

  // inline_array_type?
  private static boolean local_variable_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "local_variable_1")) return false;
    inline_array_type(builder, level + 1);
    return true;
  }

  // (ASSIGNMENT expression)?
  private static boolean local_variable_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "local_variable_2")) return false;
    local_variable_2_0(builder, level + 1);
    return true;
  }

  // ASSIGNMENT expression
  private static boolean local_variable_2_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "local_variable_2_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, ASSIGNMENT);
    result = result && expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // local_tuple_declaration | local_variable
  public static boolean local_variable_declaration(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "local_variable_declaration")) return false;
    if (!nextTokenIs(builder, "<local variable declaration>", IDENTIFIER, LPAREN)) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<local variable declaration>");
    result = local_tuple_declaration(builder, level + 1);
    if (!result) result = local_variable(builder, level + 1);
    exit_section_(builder, level, marker, LOCAL_VARIABLE_DECLARATION, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // (KW_VAR | type) local_variable_declaration (COMMA local_variable_declaration)* SEMICOLON
  public static boolean local_variable_declarations(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "local_variable_declarations")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<local variable declarations>");
    result = local_variable_declarations_0(builder, level + 1);
    result = result && local_variable_declaration(builder, level + 1);
    result = result && local_variable_declarations_2(builder, level + 1);
    result = result && consumeToken(builder, SEMICOLON);
    exit_section_(builder, level, marker, LOCAL_VARIABLE_DECLARATIONS, result, false, null);
    return result;
  }

  // KW_VAR | type
  private static boolean local_variable_declarations_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "local_variable_declarations_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_VAR);
    if (!result) result = type(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (COMMA local_variable_declaration)*
  private static boolean local_variable_declarations_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "local_variable_declarations_2")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!local_variable_declarations_2_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "local_variable_declarations_2", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // COMMA local_variable_declaration
  private static boolean local_variable_declarations_2_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "local_variable_declarations_2_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, COMMA);
    result = result && local_variable_declaration(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // KW_LOCK LPAREN expression RPAREN embedded_statement
  public static boolean lock_statement(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "lock_statement")) return false;
    if (!nextTokenIs(builder, KW_LOCK)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, KW_LOCK, LPAREN);
    result = result && expression(builder, level + 1);
    result = result && consumeToken(builder, RPAREN);
    result = result && embedded_statement(builder, level + 1);
    exit_section_(builder, marker, LOCK_STATEMENT, result);
    return result;
  }

  /* ********************************************************** */
  // (GLOBAL_NS IDENTIFIER | IDENTIFIER) type_arguments? (member_access)*
  public static boolean member(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "member")) return false;
    if (!nextTokenIs(builder, "<member>", GLOBAL_NS, IDENTIFIER)) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<member>");
    result = member_0(builder, level + 1);
    result = result && member_1(builder, level + 1);
    result = result && member_2(builder, level + 1);
    exit_section_(builder, level, marker, MEMBER, result, false, null);
    return result;
  }

  // GLOBAL_NS IDENTIFIER | IDENTIFIER
  private static boolean member_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "member_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = parseTokens(builder, 0, GLOBAL_NS, IDENTIFIER);
    if (!result) result = consumeToken(builder, IDENTIFIER);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // type_arguments?
  private static boolean member_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "member_1")) return false;
    type_arguments(builder, level + 1);
    return true;
  }

  // (member_access)*
  private static boolean member_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "member_2")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!member_2_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "member_2", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // (member_access)
  private static boolean member_2_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "member_2_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = member_access(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // DOT IDENTIFIER (type_arguments)?
  public static boolean member_access(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "member_access")) return false;
    if (!nextTokenIs(builder, DOT)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, DOT, IDENTIFIER);
    result = result && member_access_2(builder, level + 1);
    exit_section_(builder, marker, MEMBER_ACCESS, result);
    return result;
  }

  // (type_arguments)?
  private static boolean member_access_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "member_access_2")) return false;
    member_access_2_0(builder, level + 1);
    return true;
  }

  // (type_arguments)
  private static boolean member_access_2_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "member_access_2_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = type_arguments(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // KW_ASYNC | KW_CLASS | KW_EXTERN | KW_INLINE | KW_STATIC
  //                         | KW_ABSTRACT | KW_VIRTUAL | KW_OVERRIDE | KW_NEW
  public static boolean member_declaration_modifier(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "member_declaration_modifier")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<member declaration modifier>");
    result = consumeToken(builder, KW_ASYNC);
    if (!result) result = consumeToken(builder, KW_CLASS);
    if (!result) result = consumeToken(builder, KW_EXTERN);
    if (!result) result = consumeToken(builder, KW_INLINE);
    if (!result) result = consumeToken(builder, KW_STATIC);
    if (!result) result = consumeToken(builder, KW_ABSTRACT);
    if (!result) result = consumeToken(builder, KW_VIRTUAL);
    if (!result) result = consumeToken(builder, KW_OVERRIDE);
    if (!result) result = consumeToken(builder, KW_NEW);
    exit_section_(builder, level, marker, MEMBER_DECLARATION_MODIFIER, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // member_declaration_modifier+
  public static boolean member_declaration_modifiers(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "member_declaration_modifiers")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<member declaration modifiers>");
    result = member_declaration_modifier(builder, level + 1);
    int pos = current_position_(builder);
    while (result) {
      if (!member_declaration_modifier(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "member_declaration_modifiers", pos)) break;
      pos = current_position_(builder);
    }
    exit_section_(builder, level, marker, MEMBER_DECLARATION_MODIFIERS, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // IDENTIFIER ASSIGNMENT expression
  public static boolean member_initializer(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "member_initializer")) return false;
    if (!nextTokenIs(builder, IDENTIFIER)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, IDENTIFIER, ASSIGNMENT);
    result = result && expression(builder, level + 1);
    exit_section_(builder, marker, MEMBER_INITIALIZER, result);
    return result;
  }

  /* ********************************************************** */
  // LPAREN (arguments)? RPAREN (object_initializer)?
  public static boolean method_call(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "method_call")) return false;
    if (!nextTokenIs(builder, LPAREN)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, LPAREN);
    result = result && method_call_1(builder, level + 1);
    result = result && consumeToken(builder, RPAREN);
    result = result && method_call_3(builder, level + 1);
    exit_section_(builder, marker, METHOD_CALL, result);
    return result;
  }

  // (arguments)?
  private static boolean method_call_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "method_call_1")) return false;
    method_call_1_0(builder, level + 1);
    return true;
  }

  // (arguments)
  private static boolean method_call_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "method_call_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = arguments(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (object_initializer)?
  private static boolean method_call_3(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "method_call_3")) return false;
    method_call_3_0(builder, level + 1);
    return true;
  }

  // (object_initializer)
  private static boolean method_call_3_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "method_call_3_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = object_initializer(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // access_modifier? member_declaration_modifiers? type IDENTIFIER
  //             type_parameters? parameters throws_part? requires_decl? ensures_decl?
  //             (SEMICOLON | block)
  public static boolean method_declaration(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "method_declaration")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<method declaration>");
    result = method_declaration_0(builder, level + 1);
    result = result && method_declaration_1(builder, level + 1);
    result = result && type(builder, level + 1);
    result = result && consumeToken(builder, IDENTIFIER);
    result = result && method_declaration_4(builder, level + 1);
    result = result && parameters(builder, level + 1);
    result = result && method_declaration_6(builder, level + 1);
    result = result && method_declaration_7(builder, level + 1);
    result = result && method_declaration_8(builder, level + 1);
    result = result && method_declaration_9(builder, level + 1);
    exit_section_(builder, level, marker, METHOD_DECLARATION, result, false, null);
    return result;
  }

  // access_modifier?
  private static boolean method_declaration_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "method_declaration_0")) return false;
    access_modifier(builder, level + 1);
    return true;
  }

  // member_declaration_modifiers?
  private static boolean method_declaration_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "method_declaration_1")) return false;
    member_declaration_modifiers(builder, level + 1);
    return true;
  }

  // type_parameters?
  private static boolean method_declaration_4(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "method_declaration_4")) return false;
    type_parameters(builder, level + 1);
    return true;
  }

  // throws_part?
  private static boolean method_declaration_6(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "method_declaration_6")) return false;
    throws_part(builder, level + 1);
    return true;
  }

  // requires_decl?
  private static boolean method_declaration_7(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "method_declaration_7")) return false;
    requires_decl(builder, level + 1);
    return true;
  }

  // ensures_decl?
  private static boolean method_declaration_8(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "method_declaration_8")) return false;
    ensures_decl(builder, level + 1);
    return true;
  }

  // SEMICOLON | block
  private static boolean method_declaration_9(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "method_declaration_9")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, SEMICOLON);
    if (!result) result = block(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // unary_expression ( ( MULTIPLY | DIV | MOD ) unary_expression )*
  public static boolean multiplicative_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "multiplicative_expression")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<multiplicative expression>");
    result = unary_expression(builder, level + 1);
    result = result && multiplicative_expression_1(builder, level + 1);
    exit_section_(builder, level, marker, MULTIPLICATIVE_EXPRESSION, result, false, null);
    return result;
  }

  // ( ( MULTIPLY | DIV | MOD ) unary_expression )*
  private static boolean multiplicative_expression_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "multiplicative_expression_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!multiplicative_expression_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "multiplicative_expression_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // ( MULTIPLY | DIV | MOD ) unary_expression
  private static boolean multiplicative_expression_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "multiplicative_expression_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = multiplicative_expression_1_0_0(builder, level + 1);
    result = result && unary_expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // MULTIPLY | DIV | MOD
  private static boolean multiplicative_expression_1_0_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "multiplicative_expression_1_0_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, MULTIPLY);
    if (!result) result = consumeToken(builder, DIV);
    if (!result) result = consumeToken(builder, MOD);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // KW_NAMESPACE symbol LCURL (using_directive)* (namespace_member)* RCURL
  public static boolean namespace_declaration(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "namespace_declaration")) return false;
    if (!nextTokenIs(builder, KW_NAMESPACE)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_NAMESPACE);
    result = result && symbol(builder, level + 1);
    result = result && consumeToken(builder, LCURL);
    result = result && namespace_declaration_3(builder, level + 1);
    result = result && namespace_declaration_4(builder, level + 1);
    result = result && consumeToken(builder, RCURL);
    exit_section_(builder, marker, NAMESPACE_DECLARATION, result);
    return result;
  }

  // (using_directive)*
  private static boolean namespace_declaration_3(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "namespace_declaration_3")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!namespace_declaration_3_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "namespace_declaration_3", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // (using_directive)
  private static boolean namespace_declaration_3_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "namespace_declaration_3_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = using_directive(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (namespace_member)*
  private static boolean namespace_declaration_4(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "namespace_declaration_4")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!namespace_declaration_4_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "namespace_declaration_4", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // (namespace_member)
  private static boolean namespace_declaration_4_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "namespace_declaration_4_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = namespace_member(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // attributes?
  //                      ( namespace_declaration |
  //                        class_declaration |
  //                        interface_declaration |
  //                        struct_declaration |
  //                        enum_declaration |
  //                        errordomain_declaration |
  //                        method_declaration |
  //                        delegate_declaration |
  //                        field_declaration |
  //                        constant_declaration )
  public static boolean namespace_member(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "namespace_member")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<namespace member>");
    result = namespace_member_0(builder, level + 1);
    result = result && namespace_member_1(builder, level + 1);
    exit_section_(builder, level, marker, NAMESPACE_MEMBER, result, false, null);
    return result;
  }

  // attributes?
  private static boolean namespace_member_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "namespace_member_0")) return false;
    attributes(builder, level + 1);
    return true;
  }

  // namespace_declaration |
  //                        class_declaration |
  //                        interface_declaration |
  //                        struct_declaration |
  //                        enum_declaration |
  //                        errordomain_declaration |
  //                        method_declaration |
  //                        delegate_declaration |
  //                        field_declaration |
  //                        constant_declaration
  private static boolean namespace_member_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "namespace_member_1")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = namespace_declaration(builder, level + 1);
    if (!result) result = class_declaration(builder, level + 1);
    if (!result) result = interface_declaration(builder, level + 1);
    if (!result) result = struct_declaration(builder, level + 1);
    if (!result) result = enum_declaration(builder, level + 1);
    if (!result) result = errordomain_declaration(builder, level + 1);
    if (!result) result = method_declaration(builder, level + 1);
    if (!result) result = delegate_declaration(builder, level + 1);
    if (!result) result = field_declaration(builder, level + 1);
    if (!result) result = constant_declaration(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // LPAREN arguments? RPAREN object_initializer?
  public static boolean object_creation_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "object_creation_expression")) return false;
    if (!nextTokenIs(builder, LPAREN)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, LPAREN);
    result = result && object_creation_expression_1(builder, level + 1);
    result = result && consumeToken(builder, RPAREN);
    result = result && object_creation_expression_3(builder, level + 1);
    exit_section_(builder, marker, OBJECT_CREATION_EXPRESSION, result);
    return result;
  }

  // arguments?
  private static boolean object_creation_expression_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "object_creation_expression_1")) return false;
    arguments(builder, level + 1);
    return true;
  }

  // object_initializer?
  private static boolean object_creation_expression_3(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "object_creation_expression_3")) return false;
    object_initializer(builder, level + 1);
    return true;
  }

  /* ********************************************************** */
  // LCURL member_initializer (COMMA member_initializer)? RCURL
  public static boolean object_initializer(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "object_initializer")) return false;
    if (!nextTokenIs(builder, LCURL)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, LCURL);
    result = result && member_initializer(builder, level + 1);
    result = result && object_initializer_2(builder, level + 1);
    result = result && consumeToken(builder, RCURL);
    exit_section_(builder, marker, OBJECT_INITIALIZER, result);
    return result;
  }

  // (COMMA member_initializer)?
  private static boolean object_initializer_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "object_initializer_2")) return false;
    object_initializer_2_0(builder, level + 1);
    return true;
  }

  // COMMA member_initializer
  private static boolean object_initializer_2_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "object_initializer_2_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, COMMA);
    result = result && member_initializer(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // KW_NEW member (object_creation_expression | array_creation_expression)
  public static boolean object_or_array_creation_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "object_or_array_creation_expression")) return false;
    if (!nextTokenIs(builder, KW_NEW)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_NEW);
    result = result && member(builder, level + 1);
    result = result && object_or_array_creation_expression_2(builder, level + 1);
    exit_section_(builder, marker, OBJECT_OR_ARRAY_CREATION_EXPRESSION, result);
    return result;
  }

  // object_creation_expression | array_creation_expression
  private static boolean object_or_array_creation_expression_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "object_or_array_creation_expression_2")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = object_creation_expression(builder, level + 1);
    if (!result) result = array_creation_expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // attributes? (ELIPSIS | (KW_PARAMS? (KW_OWNED | KW_UNOWNED)? (KW_REF | KW_OUT)?
  //             type IDENTIFIER (ASSIGNMENT expression)?))
  public static boolean parameter(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "parameter")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<parameter>");
    result = parameter_0(builder, level + 1);
    result = result && parameter_1(builder, level + 1);
    exit_section_(builder, level, marker, PARAMETER, result, false, null);
    return result;
  }

  // attributes?
  private static boolean parameter_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "parameter_0")) return false;
    attributes(builder, level + 1);
    return true;
  }

  // ELIPSIS | (KW_PARAMS? (KW_OWNED | KW_UNOWNED)? (KW_REF | KW_OUT)?
  //             type IDENTIFIER (ASSIGNMENT expression)?)
  private static boolean parameter_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "parameter_1")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, ELIPSIS);
    if (!result) result = parameter_1_1(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // KW_PARAMS? (KW_OWNED | KW_UNOWNED)? (KW_REF | KW_OUT)?
  //             type IDENTIFIER (ASSIGNMENT expression)?
  private static boolean parameter_1_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "parameter_1_1")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = parameter_1_1_0(builder, level + 1);
    result = result && parameter_1_1_1(builder, level + 1);
    result = result && parameter_1_1_2(builder, level + 1);
    result = result && type(builder, level + 1);
    result = result && consumeToken(builder, IDENTIFIER);
    result = result && parameter_1_1_5(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // KW_PARAMS?
  private static boolean parameter_1_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "parameter_1_1_0")) return false;
    consumeToken(builder, KW_PARAMS);
    return true;
  }

  // (KW_OWNED | KW_UNOWNED)?
  private static boolean parameter_1_1_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "parameter_1_1_1")) return false;
    parameter_1_1_1_0(builder, level + 1);
    return true;
  }

  // KW_OWNED | KW_UNOWNED
  private static boolean parameter_1_1_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "parameter_1_1_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_OWNED);
    if (!result) result = consumeToken(builder, KW_UNOWNED);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (KW_REF | KW_OUT)?
  private static boolean parameter_1_1_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "parameter_1_1_2")) return false;
    parameter_1_1_2_0(builder, level + 1);
    return true;
  }

  // KW_REF | KW_OUT
  private static boolean parameter_1_1_2_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "parameter_1_1_2_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_REF);
    if (!result) result = consumeToken(builder, KW_OUT);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (ASSIGNMENT expression)?
  private static boolean parameter_1_1_5(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "parameter_1_1_5")) return false;
    parameter_1_1_5_0(builder, level + 1);
    return true;
  }

  // ASSIGNMENT expression
  private static boolean parameter_1_1_5_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "parameter_1_1_5_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, ASSIGNMENT);
    result = result && expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // LPAREN parameters_decl? RPAREN
  public static boolean parameters(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "parameters")) return false;
    if (!nextTokenIs(builder, LPAREN)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, LPAREN);
    result = result && parameters_1(builder, level + 1);
    result = result && consumeToken(builder, RPAREN);
    exit_section_(builder, marker, PARAMETERS, result);
    return result;
  }

  // parameters_decl?
  private static boolean parameters_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "parameters_1")) return false;
    parameters_decl(builder, level + 1);
    return true;
  }

  /* ********************************************************** */
  // parameter (COMMA parameter)*
  public static boolean parameters_decl(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "parameters_decl")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<parameters decl>");
    result = parameter(builder, level + 1);
    result = result && parameters_decl_1(builder, level + 1);
    exit_section_(builder, level, marker, PARAMETERS_DECL, result, false, null);
    return result;
  }

  // (COMMA parameter)*
  private static boolean parameters_decl_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "parameters_decl_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!parameters_decl_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "parameters_decl_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // COMMA parameter
  private static boolean parameters_decl_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "parameters_decl_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, COMMA);
    result = result && parameter(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // POINTER_ACCESS IDENTIFIER (type_arguments)?
  public static boolean pointer_member_access(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "pointer_member_access")) return false;
    if (!nextTokenIs(builder, POINTER_ACCESS)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, POINTER_ACCESS, IDENTIFIER);
    result = result && pointer_member_access_2(builder, level + 1);
    exit_section_(builder, marker, POINTER_MEMBER_ACCESS, result);
    return result;
  }

  // (type_arguments)?
  private static boolean pointer_member_access_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "pointer_member_access_2")) return false;
    pointer_member_access_2_0(builder, level + 1);
    return true;
  }

  // (type_arguments)
  private static boolean pointer_member_access_2_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "pointer_member_access_2_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = type_arguments(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // DECREMENT
  public static boolean post_decrement_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "post_decrement_expression")) return false;
    if (!nextTokenIs(builder, DECREMENT)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, DECREMENT);
    exit_section_(builder, marker, POST_DECREMENT_EXPRESSION, result);
    return result;
  }

  /* ********************************************************** */
  // INCREMENT
  public static boolean post_increment_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "post_increment_expression")) return false;
    if (!nextTokenIs(builder, INCREMENT)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, INCREMENT);
    exit_section_(builder, marker, POST_INCREMENT_EXPRESSION, result);
    return result;
  }

  /* ********************************************************** */
  // ( literal | initializer | tuple | template | REGEX_LITERAL | this_access | base_access |
  //                        object_or_array_creation_expression | yield_expression | sizeof_expression | typeof_expression |
  //                        simple_name )
  //                        ( member_access | pointer_member_access | method_call | element_access |
  //                          post_increment_expression | post_decrement_expression )*
  public static boolean primary_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "primary_expression")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<primary expression>");
    result = primary_expression_0(builder, level + 1);
    result = result && primary_expression_1(builder, level + 1);
    exit_section_(builder, level, marker, PRIMARY_EXPRESSION, result, false, null);
    return result;
  }

  // literal | initializer | tuple | template | REGEX_LITERAL | this_access | base_access |
  //                        object_or_array_creation_expression | yield_expression | sizeof_expression | typeof_expression |
  //                        simple_name
  private static boolean primary_expression_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "primary_expression_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = literal(builder, level + 1);
    if (!result) result = initializer(builder, level + 1);
    if (!result) result = tuple(builder, level + 1);
    if (!result) result = template(builder, level + 1);
    if (!result) result = consumeToken(builder, REGEX_LITERAL);
    if (!result) result = this_access(builder, level + 1);
    if (!result) result = base_access(builder, level + 1);
    if (!result) result = object_or_array_creation_expression(builder, level + 1);
    if (!result) result = yield_expression(builder, level + 1);
    if (!result) result = sizeof_expression(builder, level + 1);
    if (!result) result = typeof_expression(builder, level + 1);
    if (!result) result = simple_name(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // ( member_access | pointer_member_access | method_call | element_access |
  //                          post_increment_expression | post_decrement_expression )*
  private static boolean primary_expression_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "primary_expression_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!primary_expression_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "primary_expression_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // member_access | pointer_member_access | method_call | element_access |
  //                          post_increment_expression | post_decrement_expression
  private static boolean primary_expression_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "primary_expression_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = member_access(builder, level + 1);
    if (!result) result = pointer_member_access(builder, level + 1);
    if (!result) result = method_call(builder, level + 1);
    if (!result) result = element_access(builder, level + 1);
    if (!result) result = post_increment_expression(builder, level + 1);
    if (!result) result = post_decrement_expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // attributes? access_modifier? (KW_OWNED | KW_UNOWNED)?
  //             ((property_get_accessor property_set_construct_accessor)
  //             | (property_set_construct_accessor property_get_accessor)
  //             | (property_set_construct_accessor) | (property_get_accessor))
  public static boolean property_accessor(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_accessor")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<property accessor>");
    result = property_accessor_0(builder, level + 1);
    result = result && property_accessor_1(builder, level + 1);
    result = result && property_accessor_2(builder, level + 1);
    result = result && property_accessor_3(builder, level + 1);
    exit_section_(builder, level, marker, PROPERTY_ACCESSOR, result, false, null);
    return result;
  }

  // attributes?
  private static boolean property_accessor_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_accessor_0")) return false;
    attributes(builder, level + 1);
    return true;
  }

  // access_modifier?
  private static boolean property_accessor_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_accessor_1")) return false;
    access_modifier(builder, level + 1);
    return true;
  }

  // (KW_OWNED | KW_UNOWNED)?
  private static boolean property_accessor_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_accessor_2")) return false;
    property_accessor_2_0(builder, level + 1);
    return true;
  }

  // KW_OWNED | KW_UNOWNED
  private static boolean property_accessor_2_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_accessor_2_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_OWNED);
    if (!result) result = consumeToken(builder, KW_UNOWNED);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (property_get_accessor property_set_construct_accessor)
  //             | (property_set_construct_accessor property_get_accessor)
  //             | (property_set_construct_accessor) | (property_get_accessor)
  private static boolean property_accessor_3(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_accessor_3")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = property_accessor_3_0(builder, level + 1);
    if (!result) result = property_accessor_3_1(builder, level + 1);
    if (!result) result = property_accessor_3_2(builder, level + 1);
    if (!result) result = property_accessor_3_3(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // property_get_accessor property_set_construct_accessor
  private static boolean property_accessor_3_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_accessor_3_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = property_get_accessor(builder, level + 1);
    result = result && property_set_construct_accessor(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // property_set_construct_accessor property_get_accessor
  private static boolean property_accessor_3_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_accessor_3_1")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = property_set_construct_accessor(builder, level + 1);
    result = result && property_get_accessor(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (property_set_construct_accessor)
  private static boolean property_accessor_3_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_accessor_3_2")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = property_set_construct_accessor(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (property_get_accessor)
  private static boolean property_accessor_3_3(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_accessor_3_3")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = property_get_accessor(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // access_modifier? property_declaration_modifiers? type IDENTIFIER
  //             LCURL property_declaration_part* RCURL
  public static boolean property_declaration(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_declaration")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<property declaration>");
    result = property_declaration_0(builder, level + 1);
    result = result && property_declaration_1(builder, level + 1);
    result = result && type(builder, level + 1);
    result = result && consumeTokens(builder, 0, IDENTIFIER, LCURL);
    result = result && property_declaration_5(builder, level + 1);
    result = result && consumeToken(builder, RCURL);
    exit_section_(builder, level, marker, PROPERTY_DECLARATION, result, false, null);
    return result;
  }

  // access_modifier?
  private static boolean property_declaration_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_declaration_0")) return false;
    access_modifier(builder, level + 1);
    return true;
  }

  // property_declaration_modifiers?
  private static boolean property_declaration_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_declaration_1")) return false;
    property_declaration_modifiers(builder, level + 1);
    return true;
  }

  // property_declaration_part*
  private static boolean property_declaration_5(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_declaration_5")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!property_declaration_part(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "property_declaration_5", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  /* ********************************************************** */
  // KW_CLASS | KW_STATIC | KW_EXTERN | KW_INLINE | KW_ABSTRACT
  //             | KW_VIRTUAL | KW_OVERRIDE | KW_NEW
  public static boolean property_declaration_modifier(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_declaration_modifier")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<property declaration modifier>");
    result = consumeToken(builder, KW_CLASS);
    if (!result) result = consumeToken(builder, KW_STATIC);
    if (!result) result = consumeToken(builder, KW_EXTERN);
    if (!result) result = consumeToken(builder, KW_INLINE);
    if (!result) result = consumeToken(builder, KW_ABSTRACT);
    if (!result) result = consumeToken(builder, KW_VIRTUAL);
    if (!result) result = consumeToken(builder, KW_OVERRIDE);
    if (!result) result = consumeToken(builder, KW_NEW);
    exit_section_(builder, level, marker, PROPERTY_DECLARATION_MODIFIER, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // property_declaration_modifier+
  public static boolean property_declaration_modifiers(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_declaration_modifiers")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<property declaration modifiers>");
    result = property_declaration_modifier(builder, level + 1);
    int pos = current_position_(builder);
    while (result) {
      if (!property_declaration_modifier(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "property_declaration_modifiers", pos)) break;
      pos = current_position_(builder);
    }
    exit_section_(builder, level, marker, PROPERTY_DECLARATION_MODIFIERS, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // (KW_DEFAULT ASSIGNMENT expression SEMICOLON) | property_accessor
  public static boolean property_declaration_part(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_declaration_part")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<property declaration part>");
    result = property_declaration_part_0(builder, level + 1);
    if (!result) result = property_accessor(builder, level + 1);
    exit_section_(builder, level, marker, PROPERTY_DECLARATION_PART, result, false, null);
    return result;
  }

  // KW_DEFAULT ASSIGNMENT expression SEMICOLON
  private static boolean property_declaration_part_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_declaration_part_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, KW_DEFAULT, ASSIGNMENT);
    result = result && expression(builder, level + 1);
    result = result && consumeToken(builder, SEMICOLON);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // access_modifier? KW_GET (SEMICOLON | block)
  public static boolean property_get_accessor(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_get_accessor")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<property get accessor>");
    result = property_get_accessor_0(builder, level + 1);
    result = result && consumeToken(builder, KW_GET);
    result = result && property_get_accessor_2(builder, level + 1);
    exit_section_(builder, level, marker, PROPERTY_GET_ACCESSOR, result, false, null);
    return result;
  }

  // access_modifier?
  private static boolean property_get_accessor_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_get_accessor_0")) return false;
    access_modifier(builder, level + 1);
    return true;
  }

  // SEMICOLON | block
  private static boolean property_get_accessor_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_get_accessor_2")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, SEMICOLON);
    if (!result) result = block(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // (access_modifier? ((KW_SET KW_CONSTRUCT?) | KW_CONSTRUCT | (KW_CONSTRUCT KW_SET)))
  //             (SEMICOLON | block)
  public static boolean property_set_construct_accessor(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_set_construct_accessor")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<property set construct accessor>");
    result = property_set_construct_accessor_0(builder, level + 1);
    result = result && property_set_construct_accessor_1(builder, level + 1);
    exit_section_(builder, level, marker, PROPERTY_SET_CONSTRUCT_ACCESSOR, result, false, null);
    return result;
  }

  // access_modifier? ((KW_SET KW_CONSTRUCT?) | KW_CONSTRUCT | (KW_CONSTRUCT KW_SET))
  private static boolean property_set_construct_accessor_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_set_construct_accessor_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = property_set_construct_accessor_0_0(builder, level + 1);
    result = result && property_set_construct_accessor_0_1(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // access_modifier?
  private static boolean property_set_construct_accessor_0_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_set_construct_accessor_0_0")) return false;
    access_modifier(builder, level + 1);
    return true;
  }

  // (KW_SET KW_CONSTRUCT?) | KW_CONSTRUCT | (KW_CONSTRUCT KW_SET)
  private static boolean property_set_construct_accessor_0_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_set_construct_accessor_0_1")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = property_set_construct_accessor_0_1_0(builder, level + 1);
    if (!result) result = consumeToken(builder, KW_CONSTRUCT);
    if (!result) result = property_set_construct_accessor_0_1_2(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // KW_SET KW_CONSTRUCT?
  private static boolean property_set_construct_accessor_0_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_set_construct_accessor_0_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_SET);
    result = result && property_set_construct_accessor_0_1_0_1(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // KW_CONSTRUCT?
  private static boolean property_set_construct_accessor_0_1_0_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_set_construct_accessor_0_1_0_1")) return false;
    consumeToken(builder, KW_CONSTRUCT);
    return true;
  }

  // KW_CONSTRUCT KW_SET
  private static boolean property_set_construct_accessor_0_1_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_set_construct_accessor_0_1_2")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, KW_CONSTRUCT, KW_SET);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // SEMICOLON | block
  private static boolean property_set_construct_accessor_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "property_set_construct_accessor_1")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, SEMICOLON);
    if (!result) result = block(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // shift_expression ( (( LT | LTEQ | GT | GTEQ ) shift_expression ) |
  //                                              ( KW_IS type ) | ( KW_AS type ) )*
  public static boolean relational_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "relational_expression")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<relational expression>");
    result = shift_expression(builder, level + 1);
    result = result && relational_expression_1(builder, level + 1);
    exit_section_(builder, level, marker, RELATIONAL_EXPRESSION, result, false, null);
    return result;
  }

  // ( (( LT | LTEQ | GT | GTEQ ) shift_expression ) |
  //                                              ( KW_IS type ) | ( KW_AS type ) )*
  private static boolean relational_expression_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "relational_expression_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!relational_expression_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "relational_expression_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // (( LT | LTEQ | GT | GTEQ ) shift_expression ) |
  //                                              ( KW_IS type ) | ( KW_AS type )
  private static boolean relational_expression_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "relational_expression_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = relational_expression_1_0_0(builder, level + 1);
    if (!result) result = relational_expression_1_0_1(builder, level + 1);
    if (!result) result = relational_expression_1_0_2(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // ( LT | LTEQ | GT | GTEQ ) shift_expression
  private static boolean relational_expression_1_0_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "relational_expression_1_0_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = relational_expression_1_0_0_0(builder, level + 1);
    result = result && shift_expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // LT | LTEQ | GT | GTEQ
  private static boolean relational_expression_1_0_0_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "relational_expression_1_0_0_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, LT);
    if (!result) result = consumeToken(builder, LTEQ);
    if (!result) result = consumeToken(builder, GT);
    if (!result) result = consumeToken(builder, GTEQ);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // KW_IS type
  private static boolean relational_expression_1_0_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "relational_expression_1_0_1")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_IS);
    result = result && type(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // KW_AS type
  private static boolean relational_expression_1_0_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "relational_expression_1_0_2")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_AS);
    result = result && type(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // KW_REQUIRES LPAREN expression RPAREN (requires_decl)?
  public static boolean requires_decl(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "requires_decl")) return false;
    if (!nextTokenIs(builder, KW_REQUIRES)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, KW_REQUIRES, LPAREN);
    result = result && expression(builder, level + 1);
    result = result && consumeToken(builder, RPAREN);
    result = result && requires_decl_4(builder, level + 1);
    exit_section_(builder, marker, REQUIRES_DECL, result);
    return result;
  }

  // (requires_decl)?
  private static boolean requires_decl_4(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "requires_decl_4")) return false;
    requires_decl_4_0(builder, level + 1);
    return true;
  }

  // (requires_decl)
  private static boolean requires_decl_4_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "requires_decl_4_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = requires_decl(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // KW_RETURN expression? SEMICOLON
  public static boolean return_statement(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "return_statement")) return false;
    if (!nextTokenIs(builder, KW_RETURN)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_RETURN);
    result = result && return_statement_1(builder, level + 1);
    result = result && consumeToken(builder, SEMICOLON);
    exit_section_(builder, marker, RETURN_STATEMENT, result);
    return result;
  }

  // expression?
  private static boolean return_statement_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "return_statement_1")) return false;
    expression(builder, level + 1);
    return true;
  }

  /* ********************************************************** */
  // additive_expression ( ( SHIFT_LEFT | SHIFT_RIGHT ) additive_expression )*
  public static boolean shift_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "shift_expression")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<shift expression>");
    result = additive_expression(builder, level + 1);
    result = result && shift_expression_1(builder, level + 1);
    exit_section_(builder, level, marker, SHIFT_EXPRESSION, result, false, null);
    return result;
  }

  // ( ( SHIFT_LEFT | SHIFT_RIGHT ) additive_expression )*
  private static boolean shift_expression_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "shift_expression_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!shift_expression_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "shift_expression_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // ( SHIFT_LEFT | SHIFT_RIGHT ) additive_expression
  private static boolean shift_expression_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "shift_expression_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = shift_expression_1_0_0(builder, level + 1);
    result = result && additive_expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // SHIFT_LEFT | SHIFT_RIGHT
  private static boolean shift_expression_1_0_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "shift_expression_1_0_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, SHIFT_LEFT);
    if (!result) result = consumeToken(builder, SHIFT_RIGHT);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // access_modifier? signal_declaration_modifiers? KW_SIGNAL type IDENTIFIER
  //             parameters (SEMICOLON | block)
  public static boolean signal_declaration(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "signal_declaration")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<signal declaration>");
    result = signal_declaration_0(builder, level + 1);
    result = result && signal_declaration_1(builder, level + 1);
    result = result && consumeToken(builder, KW_SIGNAL);
    result = result && type(builder, level + 1);
    result = result && consumeToken(builder, IDENTIFIER);
    result = result && parameters(builder, level + 1);
    result = result && signal_declaration_6(builder, level + 1);
    exit_section_(builder, level, marker, SIGNAL_DECLARATION, result, false, null);
    return result;
  }

  // access_modifier?
  private static boolean signal_declaration_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "signal_declaration_0")) return false;
    access_modifier(builder, level + 1);
    return true;
  }

  // signal_declaration_modifiers?
  private static boolean signal_declaration_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "signal_declaration_1")) return false;
    signal_declaration_modifiers(builder, level + 1);
    return true;
  }

  // SEMICOLON | block
  private static boolean signal_declaration_6(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "signal_declaration_6")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, SEMICOLON);
    if (!result) result = block(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // KW_ASYNC | KW_EXTERN | KW_INLINE | KW_ABSTRACT | KW_VIRTUAL
  //             | KW_OVERRIDE | KW_NEW
  public static boolean signal_declaration_modifier(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "signal_declaration_modifier")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<signal declaration modifier>");
    result = consumeToken(builder, KW_ASYNC);
    if (!result) result = consumeToken(builder, KW_EXTERN);
    if (!result) result = consumeToken(builder, KW_INLINE);
    if (!result) result = consumeToken(builder, KW_ABSTRACT);
    if (!result) result = consumeToken(builder, KW_VIRTUAL);
    if (!result) result = consumeToken(builder, KW_OVERRIDE);
    if (!result) result = consumeToken(builder, KW_NEW);
    exit_section_(builder, level, marker, SIGNAL_DECLARATION_MODIFIER, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // signal_declaration_modifier+
  public static boolean signal_declaration_modifiers(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "signal_declaration_modifiers")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<signal declaration modifiers>");
    result = signal_declaration_modifier(builder, level + 1);
    int pos = current_position_(builder);
    while (result) {
      if (!signal_declaration_modifier(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "signal_declaration_modifiers", pos)) break;
      pos = current_position_(builder);
    }
    exit_section_(builder, level, marker, SIGNAL_DECLARATION_MODIFIERS, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // (GLOBAL_NS IDENTIFIER | IDENTIFIER) (type_arguments)?
  public static boolean simple_name(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "simple_name")) return false;
    if (!nextTokenIs(builder, "<simple name>", GLOBAL_NS, IDENTIFIER)) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<simple name>");
    result = simple_name_0(builder, level + 1);
    result = result && simple_name_1(builder, level + 1);
    exit_section_(builder, level, marker, SIMPLE_NAME, result, false, null);
    return result;
  }

  // GLOBAL_NS IDENTIFIER | IDENTIFIER
  private static boolean simple_name_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "simple_name_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = parseTokens(builder, 0, GLOBAL_NS, IDENTIFIER);
    if (!result) result = consumeToken(builder, IDENTIFIER);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (type_arguments)?
  private static boolean simple_name_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "simple_name_1")) return false;
    simple_name_1_0(builder, level + 1);
    return true;
  }

  // (type_arguments)
  private static boolean simple_name_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "simple_name_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = type_arguments(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // KW_SIZEOF LPAREN type RPAREN
  public static boolean sizeof_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "sizeof_expression")) return false;
    if (!nextTokenIs(builder, KW_SIZEOF)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, KW_SIZEOF, LPAREN);
    result = result && type(builder, level + 1);
    result = result && consumeToken(builder, RPAREN);
    exit_section_(builder, marker, SIZEOF_EXPRESSION, result);
    return result;
  }

  /* ********************************************************** */
  // COLON expression
  public static boolean slice_array(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "slice_array")) return false;
    if (!nextTokenIs(builder, COLON)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, COLON);
    result = result && expression(builder, level + 1);
    exit_section_(builder, marker, SLICE_ARRAY, result);
    return result;
  }

  /* ********************************************************** */
  // block | SEMICOLON | if_statement | switch_statement | while_statement
  //             | do_statement | for_statement | foreach_statement | break_statement
  //             | continue_statement | return_statement | yield_statement | throw_statement
  //             | try_statement | lock_statement | delete_statement | local_variable_declarations
  //             | expression_statement
  public static boolean statement(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "statement")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<statement>");
    result = block(builder, level + 1);
    if (!result) result = consumeToken(builder, SEMICOLON);
    if (!result) result = if_statement(builder, level + 1);
    if (!result) result = switch_statement(builder, level + 1);
    if (!result) result = while_statement(builder, level + 1);
    if (!result) result = do_statement(builder, level + 1);
    if (!result) result = for_statement(builder, level + 1);
    if (!result) result = foreach_statement(builder, level + 1);
    if (!result) result = break_statement(builder, level + 1);
    if (!result) result = continue_statement(builder, level + 1);
    if (!result) result = return_statement(builder, level + 1);
    if (!result) result = yield_statement(builder, level + 1);
    if (!result) result = throw_statement(builder, level + 1);
    if (!result) result = try_statement(builder, level + 1);
    if (!result) result = lock_statement(builder, level + 1);
    if (!result) result = delete_statement(builder, level + 1);
    if (!result) result = local_variable_declarations(builder, level + 1);
    if (!result) result = expression_statement(builder, level + 1);
    exit_section_(builder, level, marker, STATEMENT, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // expression
  public static boolean statement_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "statement_expression")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<statement expression>");
    result = expression(builder, level + 1);
    exit_section_(builder, level, marker, STATEMENT_EXPRESSION, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // access_modifier? type_declaration_modifiers? KW_STRUCT symbol (COLON base_types)?
  //             LCURL (struct_member)* RCURL
  public static boolean struct_declaration(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "struct_declaration")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<struct declaration>");
    result = struct_declaration_0(builder, level + 1);
    result = result && struct_declaration_1(builder, level + 1);
    result = result && consumeToken(builder, KW_STRUCT);
    result = result && symbol(builder, level + 1);
    result = result && struct_declaration_4(builder, level + 1);
    result = result && consumeToken(builder, LCURL);
    result = result && struct_declaration_6(builder, level + 1);
    result = result && consumeToken(builder, RCURL);
    exit_section_(builder, level, marker, STRUCT_DECLARATION, result, false, null);
    return result;
  }

  // access_modifier?
  private static boolean struct_declaration_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "struct_declaration_0")) return false;
    access_modifier(builder, level + 1);
    return true;
  }

  // type_declaration_modifiers?
  private static boolean struct_declaration_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "struct_declaration_1")) return false;
    type_declaration_modifiers(builder, level + 1);
    return true;
  }

  // (COLON base_types)?
  private static boolean struct_declaration_4(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "struct_declaration_4")) return false;
    struct_declaration_4_0(builder, level + 1);
    return true;
  }

  // COLON base_types
  private static boolean struct_declaration_4_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "struct_declaration_4_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, COLON);
    result = result && base_types(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (struct_member)*
  private static boolean struct_declaration_6(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "struct_declaration_6")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!struct_declaration_6_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "struct_declaration_6", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // (struct_member)
  private static boolean struct_declaration_6_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "struct_declaration_6_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = struct_member(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // attributes? (method_declaration | field_declaration | constant_declaration
  //             | property_declaration | creation_method_declaration)
  public static boolean struct_member(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "struct_member")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<struct member>");
    result = struct_member_0(builder, level + 1);
    result = result && struct_member_1(builder, level + 1);
    exit_section_(builder, level, marker, STRUCT_MEMBER, result, false, null);
    return result;
  }

  // attributes?
  private static boolean struct_member_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "struct_member_0")) return false;
    attributes(builder, level + 1);
    return true;
  }

  // method_declaration | field_declaration | constant_declaration
  //             | property_declaration | creation_method_declaration
  private static boolean struct_member_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "struct_member_1")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = method_declaration(builder, level + 1);
    if (!result) result = field_declaration(builder, level + 1);
    if (!result) result = constant_declaration(builder, level + 1);
    if (!result) result = property_declaration(builder, level + 1);
    if (!result) result = creation_method_declaration(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // ((KW_CASE expression) | KW_DEFAULT) COLON statement* KW_BREAK?
  public static boolean switch_section(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "switch_section")) return false;
    if (!nextTokenIs(builder, "<switch section>", KW_CASE, KW_DEFAULT)) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<switch section>");
    result = switch_section_0(builder, level + 1);
    result = result && consumeToken(builder, COLON);
    result = result && switch_section_2(builder, level + 1);
    result = result && switch_section_3(builder, level + 1);
    exit_section_(builder, level, marker, SWITCH_SECTION, result, false, null);
    return result;
  }

  // (KW_CASE expression) | KW_DEFAULT
  private static boolean switch_section_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "switch_section_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = switch_section_0_0(builder, level + 1);
    if (!result) result = consumeToken(builder, KW_DEFAULT);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // KW_CASE expression
  private static boolean switch_section_0_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "switch_section_0_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_CASE);
    result = result && expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // statement*
  private static boolean switch_section_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "switch_section_2")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!statement(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "switch_section_2", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // KW_BREAK?
  private static boolean switch_section_3(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "switch_section_3")) return false;
    consumeToken(builder, KW_BREAK);
    return true;
  }

  /* ********************************************************** */
  // KW_SWITCH LPAREN expression RPAREN LCURL (switch_section)* RCURL
  public static boolean switch_statement(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "switch_statement")) return false;
    if (!nextTokenIs(builder, KW_SWITCH)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, KW_SWITCH, LPAREN);
    result = result && expression(builder, level + 1);
    result = result && consumeTokens(builder, 0, RPAREN, LCURL);
    result = result && switch_statement_5(builder, level + 1);
    result = result && consumeToken(builder, RCURL);
    exit_section_(builder, marker, SWITCH_STATEMENT, result);
    return result;
  }

  // (switch_section)*
  private static boolean switch_statement_5(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "switch_statement_5")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!switch_statement_5_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "switch_statement_5", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // (switch_section)
  private static boolean switch_statement_5_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "switch_statement_5_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = switch_section(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // symbol_part (DOT symbol_part)*
  public static boolean symbol(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "symbol")) return false;
    if (!nextTokenIs(builder, "<symbol>", GLOBAL_NS, IDENTIFIER)) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<symbol>");
    result = symbol_part(builder, level + 1);
    result = result && symbol_1(builder, level + 1);
    exit_section_(builder, level, marker, SYMBOL, result, false, null);
    return result;
  }

  // (DOT symbol_part)*
  private static boolean symbol_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "symbol_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!symbol_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "symbol_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // DOT symbol_part
  private static boolean symbol_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "symbol_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, DOT);
    result = result && symbol_part(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // ( GLOBAL_NS IDENTIFIER ) | IDENTIFIER
  public static boolean symbol_part(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "symbol_part")) return false;
    if (!nextTokenIs(builder, "<symbol part>", GLOBAL_NS, IDENTIFIER)) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<symbol part>");
    result = symbol_part_0(builder, level + 1);
    if (!result) result = consumeToken(builder, IDENTIFIER);
    exit_section_(builder, level, marker, SYMBOL_PART, result, false, null);
    return result;
  }

  // GLOBAL_NS IDENTIFIER
  private static boolean symbol_part_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "symbol_part_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, GLOBAL_NS, IDENTIFIER);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // TEMPLATE_START (expression COMMA)* QUOT
  public static boolean template(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "template")) return false;
    if (!nextTokenIs(builder, TEMPLATE_START)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, TEMPLATE_START);
    result = result && template_1(builder, level + 1);
    result = result && consumeToken(builder, QUOT);
    exit_section_(builder, marker, TEMPLATE, result);
    return result;
  }

  // (expression COMMA)*
  private static boolean template_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "template_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!template_1_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "template_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // expression COMMA
  private static boolean template_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "template_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = expression(builder, level + 1);
    result = result && consumeToken(builder, COMMA);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // KW_THIS
  public static boolean this_access(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "this_access")) return false;
    if (!nextTokenIs(builder, KW_THIS)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_THIS);
    exit_section_(builder, marker, THIS_ACCESS, result);
    return result;
  }

  /* ********************************************************** */
  // KW_THROW expression SEMICOLON
  public static boolean throw_statement(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "throw_statement")) return false;
    if (!nextTokenIs(builder, KW_THROW)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_THROW);
    result = result && expression(builder, level + 1);
    result = result && consumeToken(builder, SEMICOLON);
    exit_section_(builder, marker, THROW_STATEMENT, result);
    return result;
  }

  /* ********************************************************** */
  // KW_THROWS type (COMMA type)*
  public static boolean throws_part(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "throws_part")) return false;
    if (!nextTokenIs(builder, KW_THROWS)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_THROWS);
    result = result && type(builder, level + 1);
    result = result && throws_part_2(builder, level + 1);
    exit_section_(builder, marker, THROWS_PART, result);
    return result;
  }

  // (COMMA type)*
  private static boolean throws_part_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "throws_part_2")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!throws_part_2_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "throws_part_2", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // COMMA type
  private static boolean throws_part_2_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "throws_part_2_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, COMMA);
    result = result && type(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // KW_TRY block catch_clause* finally_clause?
  public static boolean try_statement(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "try_statement")) return false;
    if (!nextTokenIs(builder, KW_TRY)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_TRY);
    result = result && block(builder, level + 1);
    result = result && try_statement_2(builder, level + 1);
    result = result && try_statement_3(builder, level + 1);
    exit_section_(builder, marker, TRY_STATEMENT, result);
    return result;
  }

  // catch_clause*
  private static boolean try_statement_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "try_statement_2")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!catch_clause(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "try_statement_2", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // finally_clause?
  private static boolean try_statement_3(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "try_statement_3")) return false;
    finally_clause(builder, level + 1);
    return true;
  }

  /* ********************************************************** */
  // LPAREN  expression (COMMA expression)* RPAREN
  public static boolean tuple(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "tuple")) return false;
    if (!nextTokenIs(builder, LPAREN)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, LPAREN);
    result = result && expression(builder, level + 1);
    result = result && tuple_2(builder, level + 1);
    result = result && consumeToken(builder, RPAREN);
    exit_section_(builder, marker, TUPLE, result);
    return result;
  }

  // (COMMA expression)*
  private static boolean tuple_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "tuple_2")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!tuple_2_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "tuple_2", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // COMMA expression
  private static boolean tuple_2_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "tuple_2_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, COMMA);
    result = result && expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // ( KW_VOID (MULTIPLY)* ) |
  //         ( (KW_DYNAMIC)? (KW_OWNED | KW_UNOWNED | KW_WEAK)? symbol
  //             (type_arguments)? (MULTIPLY)* (QUESTION)? (array_type)* )
  public static boolean type(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "type")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<type>");
    result = type_0(builder, level + 1);
    if (!result) result = type_1(builder, level + 1);
    exit_section_(builder, level, marker, TYPE, result, false, null);
    return result;
  }

  // KW_VOID (MULTIPLY)*
  private static boolean type_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "type_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_VOID);
    result = result && type_0_1(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (MULTIPLY)*
  private static boolean type_0_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "type_0_1")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!consumeToken(builder, MULTIPLY)) break;
      if (!empty_element_parsed_guard_(builder, "type_0_1", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // (KW_DYNAMIC)? (KW_OWNED | KW_UNOWNED | KW_WEAK)? symbol
  //             (type_arguments)? (MULTIPLY)* (QUESTION)? (array_type)*
  private static boolean type_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "type_1")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = type_1_0(builder, level + 1);
    result = result && type_1_1(builder, level + 1);
    result = result && symbol(builder, level + 1);
    result = result && type_1_3(builder, level + 1);
    result = result && type_1_4(builder, level + 1);
    result = result && type_1_5(builder, level + 1);
    result = result && type_1_6(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (KW_DYNAMIC)?
  private static boolean type_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "type_1_0")) return false;
    consumeToken(builder, KW_DYNAMIC);
    return true;
  }

  // (KW_OWNED | KW_UNOWNED | KW_WEAK)?
  private static boolean type_1_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "type_1_1")) return false;
    type_1_1_0(builder, level + 1);
    return true;
  }

  // KW_OWNED | KW_UNOWNED | KW_WEAK
  private static boolean type_1_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "type_1_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_OWNED);
    if (!result) result = consumeToken(builder, KW_UNOWNED);
    if (!result) result = consumeToken(builder, KW_WEAK);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (type_arguments)?
  private static boolean type_1_3(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "type_1_3")) return false;
    type_1_3_0(builder, level + 1);
    return true;
  }

  // (type_arguments)
  private static boolean type_1_3_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "type_1_3_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = type_arguments(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // (MULTIPLY)*
  private static boolean type_1_4(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "type_1_4")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!consumeToken(builder, MULTIPLY)) break;
      if (!empty_element_parsed_guard_(builder, "type_1_4", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // (QUESTION)?
  private static boolean type_1_5(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "type_1_5")) return false;
    consumeToken(builder, QUESTION);
    return true;
  }

  // (array_type)*
  private static boolean type_1_6(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "type_1_6")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!type_1_6_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "type_1_6", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // (array_type)
  private static boolean type_1_6_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "type_1_6_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = array_type(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // LT type (COMMA type)* GT
  public static boolean type_arguments(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "type_arguments")) return false;
    if (!nextTokenIs(builder, LT)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, LT);
    result = result && type(builder, level + 1);
    result = result && type_arguments_2(builder, level + 1);
    result = result && consumeToken(builder, GT);
    exit_section_(builder, marker, TYPE_ARGUMENTS, result);
    return result;
  }

  // (COMMA type)*
  private static boolean type_arguments_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "type_arguments_2")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!type_arguments_2_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "type_arguments_2", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // COMMA type
  private static boolean type_arguments_2_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "type_arguments_2_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, COMMA);
    result = result && type(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // KW_ABSTRACT | KW_EXTERN | KW_STATIC
  public static boolean type_declaration_modifier(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "type_declaration_modifier")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<type declaration modifier>");
    result = consumeToken(builder, KW_ABSTRACT);
    if (!result) result = consumeToken(builder, KW_EXTERN);
    if (!result) result = consumeToken(builder, KW_STATIC);
    exit_section_(builder, level, marker, TYPE_DECLARATION_MODIFIER, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // type_declaration_modifier+
  public static boolean type_declaration_modifiers(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "type_declaration_modifiers")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<type declaration modifiers>");
    result = type_declaration_modifier(builder, level + 1);
    int pos = current_position_(builder);
    while (result) {
      if (!type_declaration_modifier(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "type_declaration_modifiers", pos)) break;
      pos = current_position_(builder);
    }
    exit_section_(builder, level, marker, TYPE_DECLARATION_MODIFIERS, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // LT IDENTIFIER (COMMA IDENTIFIER)* GT
  public static boolean type_parameters(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "type_parameters")) return false;
    if (!nextTokenIs(builder, LT)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, LT, IDENTIFIER);
    result = result && type_parameters_2(builder, level + 1);
    result = result && consumeToken(builder, GT);
    exit_section_(builder, marker, TYPE_PARAMETERS, result);
    return result;
  }

  // (COMMA IDENTIFIER)*
  private static boolean type_parameters_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "type_parameters_2")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!type_parameters_2_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "type_parameters_2", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // COMMA IDENTIFIER
  private static boolean type_parameters_2_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "type_parameters_2_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, COMMA, IDENTIFIER);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // KW_TYPEOF LPAREN type RPAREN
  public static boolean typeof_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "typeof_expression")) return false;
    if (!nextTokenIs(builder, KW_TYPEOF)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, KW_TYPEOF, LPAREN);
    result = result && type(builder, level + 1);
    result = result && consumeToken(builder, RPAREN);
    exit_section_(builder, marker, TYPEOF_EXPRESSION, result);
    return result;
  }

  /* ********************************************************** */
  // ( unary_operator unary_expression ) | primary_expression
  public static boolean unary_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "unary_expression")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<unary expression>");
    result = unary_expression_0(builder, level + 1);
    if (!result) result = primary_expression(builder, level + 1);
    exit_section_(builder, level, marker, UNARY_EXPRESSION, result, false, null);
    return result;
  }

  // unary_operator unary_expression
  private static boolean unary_expression_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "unary_expression_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = unary_operator(builder, level + 1);
    result = result && unary_expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // PLUS | MINUS | NOT | BITWISE_NOT | INCREMENT | DECREMENT | MULTIPLY | AND |
  //                     cast_operator | LPAREN NOT RPAREN
  public static boolean unary_operator(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "unary_operator")) return false;
    boolean result;
    Marker marker = enter_section_(builder, level, _NONE_, "<unary operator>");
    result = consumeToken(builder, PLUS);
    if (!result) result = consumeToken(builder, MINUS);
    if (!result) result = consumeToken(builder, NOT);
    if (!result) result = consumeToken(builder, BITWISE_NOT);
    if (!result) result = consumeToken(builder, INCREMENT);
    if (!result) result = consumeToken(builder, DECREMENT);
    if (!result) result = consumeToken(builder, MULTIPLY);
    if (!result) result = consumeToken(builder, AND);
    if (!result) result = cast_operator(builder, level + 1);
    if (!result) result = parseTokens(builder, 0, LPAREN, NOT, RPAREN);
    exit_section_(builder, level, marker, UNARY_OPERATOR, result, false, null);
    return result;
  }

  /* ********************************************************** */
  // KW_USING symbol (COMMA symbol)* SEMICOLON
  public static boolean using_directive(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "using_directive")) return false;
    if (!nextTokenIs(builder, KW_USING)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_USING);
    result = result && symbol(builder, level + 1);
    result = result && using_directive_2(builder, level + 1);
    result = result && consumeToken(builder, SEMICOLON);
    exit_section_(builder, marker, USING_DIRECTIVE, result);
    return result;
  }

  // (COMMA symbol)*
  private static boolean using_directive_2(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "using_directive_2")) return false;
    int pos = current_position_(builder);
    while (true) {
      if (!using_directive_2_0(builder, level + 1)) break;
      if (!empty_element_parsed_guard_(builder, "using_directive_2", pos)) break;
      pos = current_position_(builder);
    }
    return true;
  }

  // COMMA symbol
  private static boolean using_directive_2_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "using_directive_2_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, COMMA);
    result = result && symbol(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // KW_WHILE LPAREN expression RPAREN embedded_statement
  public static boolean while_statement(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "while_statement")) return false;
    if (!nextTokenIs(builder, KW_WHILE)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeTokens(builder, 0, KW_WHILE, LPAREN);
    result = result && expression(builder, level + 1);
    result = result && consumeToken(builder, RPAREN);
    result = result && embedded_statement(builder, level + 1);
    exit_section_(builder, marker, WHILE_STATEMENT, result);
    return result;
  }

  /* ********************************************************** */
  // KW_YIELD (base_access DOT)? member method_call
  public static boolean yield_expression(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "yield_expression")) return false;
    if (!nextTokenIs(builder, KW_YIELD)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_YIELD);
    result = result && yield_expression_1(builder, level + 1);
    result = result && member(builder, level + 1);
    result = result && method_call(builder, level + 1);
    exit_section_(builder, marker, YIELD_EXPRESSION, result);
    return result;
  }

  // (base_access DOT)?
  private static boolean yield_expression_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "yield_expression_1")) return false;
    yield_expression_1_0(builder, level + 1);
    return true;
  }

  // base_access DOT
  private static boolean yield_expression_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "yield_expression_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = base_access(builder, level + 1);
    result = result && consumeToken(builder, DOT);
    exit_section_(builder, marker, null, result);
    return result;
  }

  /* ********************************************************** */
  // KW_YIELD (expression_statement | KW_RETURN expression)? SEMICOLON
  public static boolean yield_statement(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "yield_statement")) return false;
    if (!nextTokenIs(builder, KW_YIELD)) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_YIELD);
    result = result && yield_statement_1(builder, level + 1);
    result = result && consumeToken(builder, SEMICOLON);
    exit_section_(builder, marker, YIELD_STATEMENT, result);
    return result;
  }

  // (expression_statement | KW_RETURN expression)?
  private static boolean yield_statement_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "yield_statement_1")) return false;
    yield_statement_1_0(builder, level + 1);
    return true;
  }

  // expression_statement | KW_RETURN expression
  private static boolean yield_statement_1_0(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "yield_statement_1_0")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = expression_statement(builder, level + 1);
    if (!result) result = yield_statement_1_0_1(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

  // KW_RETURN expression
  private static boolean yield_statement_1_0_1(PsiBuilder builder, int level) {
    if (!recursion_guard_(builder, level, "yield_statement_1_0_1")) return false;
    boolean result;
    Marker marker = enter_section_(builder);
    result = consumeToken(builder, KW_RETURN);
    result = result && expression(builder, level + 1);
    exit_section_(builder, marker, null, result);
    return result;
  }

}
