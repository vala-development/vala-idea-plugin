// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface BnfEmbeddedStatement extends ValaCompositeElement {

  @Nullable
  BnfBlock getBlock();

  @Nullable
  BnfEmbeddedStatementWithoutBlock getEmbeddedStatementWithoutBlock();

}
