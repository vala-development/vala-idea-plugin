// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface BnfStatement extends ValaCompositeElement {

  @Nullable
  BnfBlock getBlock();

  @Nullable
  BnfBreakStatement getBreakStatement();

  @Nullable
  BnfContinueStatement getContinueStatement();

  @Nullable
  BnfDeleteStatement getDeleteStatement();

  @Nullable
  BnfDoStatement getDoStatement();

  @Nullable
  BnfExpressionStatement getExpressionStatement();

  @Nullable
  BnfForStatement getForStatement();

  @Nullable
  BnfForeachStatement getForeachStatement();

  @Nullable
  BnfIfStatement getIfStatement();

  @Nullable
  BnfLocalVariableDeclarations getLocalVariableDeclarations();

  @Nullable
  BnfLockStatement getLockStatement();

  @Nullable
  BnfReturnStatement getReturnStatement();

  @Nullable
  BnfSwitchStatement getSwitchStatement();

  @Nullable
  BnfThrowStatement getThrowStatement();

  @Nullable
  BnfTryStatement getTryStatement();

  @Nullable
  BnfWhileStatement getWhileStatement();

  @Nullable
  BnfYieldStatement getYieldStatement();

}
