// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface BnfStructDeclaration extends ValaCompositeElement {

  @Nullable
  BnfAccessModifier getAccessModifier();

  @Nullable
  BnfBaseTypes getBaseTypes();

  @NotNull
  List<BnfStructMember> getStructMemberList();

  @NotNull
  BnfSymbol getSymbol();

  @Nullable
  BnfTypeDeclarationModifiers getTypeDeclarationModifiers();

}
