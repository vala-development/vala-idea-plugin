// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface BnfPrimaryExpression extends ValaCompositeElement {

  @Nullable
  BnfBaseAccess getBaseAccess();

  @NotNull
  List<BnfElementAccess> getElementAccessList();

  @Nullable
  BnfInitializer getInitializer();

  @Nullable
  BnfLiteral getLiteral();

  @NotNull
  List<BnfMemberAccess> getMemberAccessList();

  @NotNull
  List<BnfMethodCall> getMethodCallList();

  @Nullable
  BnfObjectOrArrayCreationExpression getObjectOrArrayCreationExpression();

  @NotNull
  List<BnfPointerMemberAccess> getPointerMemberAccessList();

  @NotNull
  List<BnfPostDecrementExpression> getPostDecrementExpressionList();

  @NotNull
  List<BnfPostIncrementExpression> getPostIncrementExpressionList();

  @Nullable
  BnfSimpleName getSimpleName();

  @Nullable
  BnfSizeofExpression getSizeofExpression();

  @Nullable
  BnfTemplate getTemplate();

  @Nullable
  BnfThisAccess getThisAccess();

  @Nullable
  BnfTuple getTuple();

  @Nullable
  BnfTypeofExpression getTypeofExpression();

  @Nullable
  BnfYieldExpression getYieldExpression();

  @Nullable
  PsiElement getRegexLiteral();

}
