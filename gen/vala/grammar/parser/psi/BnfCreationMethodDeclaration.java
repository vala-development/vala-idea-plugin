// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface BnfCreationMethodDeclaration extends ValaCompositeElement {

  @Nullable
  BnfAccessModifier getAccessModifier();

  @Nullable
  BnfBlock getBlock();

  @Nullable
  BnfConstructorDeclarationModifiers getConstructorDeclarationModifiers();

  @Nullable
  BnfEnsuresDecl getEnsuresDecl();

  @NotNull
  BnfParameters getParameters();

  @Nullable
  BnfRequiresDecl getRequiresDecl();

  @NotNull
  BnfSymbol getSymbol();

  @Nullable
  BnfThrowsPart getThrowsPart();

}
