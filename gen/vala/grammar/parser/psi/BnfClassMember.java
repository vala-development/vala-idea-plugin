// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface BnfClassMember extends ValaCompositeElement {

  @Nullable
  BnfAttributes getAttributes();

  @Nullable
  BnfClassDeclaration getClassDeclaration();

  @Nullable
  BnfConstantDeclaration getConstantDeclaration();

  @Nullable
  BnfConstructorDeclaration getConstructorDeclaration();

  @Nullable
  BnfCreationMethodDeclaration getCreationMethodDeclaration();

  @Nullable
  BnfDelegateDeclaration getDelegateDeclaration();

  @Nullable
  BnfDestructorDeclaration getDestructorDeclaration();

  @Nullable
  BnfEnumDeclaration getEnumDeclaration();

  @Nullable
  BnfFieldDeclaration getFieldDeclaration();

  @Nullable
  BnfMethodDeclaration getMethodDeclaration();

  @Nullable
  BnfPropertyDeclaration getPropertyDeclaration();

  @Nullable
  BnfSignalDeclaration getSignalDeclaration();

  @Nullable
  BnfStructDeclaration getStructDeclaration();

}
