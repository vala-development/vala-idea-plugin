// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface BnfNamespaceDeclaration extends ValaCompositeElement {

  @NotNull
  List<BnfNamespaceMember> getNamespaceMemberList();

  @NotNull
  BnfSymbol getSymbol();

  @NotNull
  List<BnfUsingDirective> getUsingDirectiveList();

}
