// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;

public class BnfVisitor extends PsiElementVisitor {

  public void visitAccessModifier(@NotNull BnfAccessModifier o) {
    visitValaCompositeElement(o);
  }

  public void visitAdditiveExpression(@NotNull BnfAdditiveExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitAndExpression(@NotNull BnfAndExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitArgument(@NotNull BnfArgument o) {
    visitValaCompositeElement(o);
  }

  public void visitArguments(@NotNull BnfArguments o) {
    visitValaCompositeElement(o);
  }

  public void visitArrayCreationExpression(@NotNull BnfArrayCreationExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitArraySize(@NotNull BnfArraySize o) {
    visitValaCompositeElement(o);
  }

  public void visitArrayType(@NotNull BnfArrayType o) {
    visitValaCompositeElement(o);
  }

  public void visitAssignmentOperator(@NotNull BnfAssignmentOperator o) {
    visitValaCompositeElement(o);
  }

  public void visitAttribute(@NotNull BnfAttribute o) {
    visitValaCompositeElement(o);
  }

  public void visitAttributeArgument(@NotNull BnfAttributeArgument o) {
    visitValaCompositeElement(o);
  }

  public void visitAttributeArguments(@NotNull BnfAttributeArguments o) {
    visitValaCompositeElement(o);
  }

  public void visitAttributes(@NotNull BnfAttributes o) {
    visitValaCompositeElement(o);
  }

  public void visitBaseAccess(@NotNull BnfBaseAccess o) {
    visitValaCompositeElement(o);
  }

  public void visitBaseTypes(@NotNull BnfBaseTypes o) {
    visitValaCompositeElement(o);
  }

  public void visitBlock(@NotNull BnfBlock o) {
    visitValaCompositeElement(o);
  }

  public void visitBreakStatement(@NotNull BnfBreakStatement o) {
    visitValaCompositeElement(o);
  }

  public void visitCastOperator(@NotNull BnfCastOperator o) {
    visitValaCompositeElement(o);
  }

  public void visitCatchClause(@NotNull BnfCatchClause o) {
    visitValaCompositeElement(o);
  }

  public void visitClassDeclaration(@NotNull BnfClassDeclaration o) {
    visitValaCompositeElement(o);
  }

  public void visitClassMember(@NotNull BnfClassMember o) {
    visitValaCompositeElement(o);
  }

  public void visitCoalescingExpression(@NotNull BnfCoalescingExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitConditionalAndExpression(@NotNull BnfConditionalAndExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitConditionalExpression(@NotNull BnfConditionalExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitConditionalOrExpression(@NotNull BnfConditionalOrExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitConstantDeclaration(@NotNull BnfConstantDeclaration o) {
    visitValaCompositeElement(o);
  }

  public void visitConstructorDeclaration(@NotNull BnfConstructorDeclaration o) {
    visitValaCompositeElement(o);
  }

  public void visitConstructorDeclarationModifier(@NotNull BnfConstructorDeclarationModifier o) {
    visitValaCompositeElement(o);
  }

  public void visitConstructorDeclarationModifiers(@NotNull BnfConstructorDeclarationModifiers o) {
    visitValaCompositeElement(o);
  }

  public void visitContinueStatement(@NotNull BnfContinueStatement o) {
    visitValaCompositeElement(o);
  }

  public void visitCreationMethodDeclaration(@NotNull BnfCreationMethodDeclaration o) {
    visitValaCompositeElement(o);
  }

  public void visitDelegateDeclaration(@NotNull BnfDelegateDeclaration o) {
    visitValaCompositeElement(o);
  }

  public void visitDelegateDeclarationModifier(@NotNull BnfDelegateDeclarationModifier o) {
    visitValaCompositeElement(o);
  }

  public void visitDelegateDeclarationModifiers(@NotNull BnfDelegateDeclarationModifiers o) {
    visitValaCompositeElement(o);
  }

  public void visitDeleteStatement(@NotNull BnfDeleteStatement o) {
    visitValaCompositeElement(o);
  }

  public void visitDestructorDeclaration(@NotNull BnfDestructorDeclaration o) {
    visitValaCompositeElement(o);
  }

  public void visitDoStatement(@NotNull BnfDoStatement o) {
    visitValaCompositeElement(o);
  }

  public void visitElementAccess(@NotNull BnfElementAccess o) {
    visitValaCompositeElement(o);
  }

  public void visitEmbeddedStatement(@NotNull BnfEmbeddedStatement o) {
    visitValaCompositeElement(o);
  }

  public void visitEmbeddedStatementWithoutBlock(@NotNull BnfEmbeddedStatementWithoutBlock o) {
    visitValaCompositeElement(o);
  }

  public void visitEnsuresDecl(@NotNull BnfEnsuresDecl o) {
    visitValaCompositeElement(o);
  }

  public void visitEnumDeclaration(@NotNull BnfEnumDeclaration o) {
    visitValaCompositeElement(o);
  }

  public void visitEnumMember(@NotNull BnfEnumMember o) {
    visitValaCompositeElement(o);
  }

  public void visitEnumValue(@NotNull BnfEnumValue o) {
    visitValaCompositeElement(o);
  }

  public void visitEnumValues(@NotNull BnfEnumValues o) {
    visitValaCompositeElement(o);
  }

  public void visitEqualityExpression(@NotNull BnfEqualityExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitErrorcode(@NotNull BnfErrorcode o) {
    visitValaCompositeElement(o);
  }

  public void visitErrorcodes(@NotNull BnfErrorcodes o) {
    visitValaCompositeElement(o);
  }

  public void visitErrordomainDeclaration(@NotNull BnfErrordomainDeclaration o) {
    visitValaCompositeElement(o);
  }

  public void visitExclusiveOrExpression(@NotNull BnfExclusiveOrExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitExpression(@NotNull BnfExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitExpressionStatement(@NotNull BnfExpressionStatement o) {
    visitValaCompositeElement(o);
  }

  public void visitFieldDeclaration(@NotNull BnfFieldDeclaration o) {
    visitValaCompositeElement(o);
  }

  public void visitFinallyClause(@NotNull BnfFinallyClause o) {
    visitValaCompositeElement(o);
  }

  public void visitForInitializer(@NotNull BnfForInitializer o) {
    visitValaCompositeElement(o);
  }

  public void visitForIterator(@NotNull BnfForIterator o) {
    visitValaCompositeElement(o);
  }

  public void visitForStatement(@NotNull BnfForStatement o) {
    visitValaCompositeElement(o);
  }

  public void visitForeachStatement(@NotNull BnfForeachStatement o) {
    visitValaCompositeElement(o);
  }

  public void visitIfStatement(@NotNull BnfIfStatement o) {
    visitValaCompositeElement(o);
  }

  public void visitInExpression(@NotNull BnfInExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitInclusiveOrExpression(@NotNull BnfInclusiveOrExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitInitializer(@NotNull BnfInitializer o) {
    visitValaCompositeElement(o);
  }

  public void visitInlineArrayType(@NotNull BnfInlineArrayType o) {
    visitValaCompositeElement(o);
  }

  public void visitInterfaceDeclaration(@NotNull BnfInterfaceDeclaration o) {
    visitValaCompositeElement(o);
  }

  public void visitInterfaceMember(@NotNull BnfInterfaceMember o) {
    visitValaCompositeElement(o);
  }

  public void visitLambdaExpression(@NotNull BnfLambdaExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitLambdaExpressionBody(@NotNull BnfLambdaExpressionBody o) {
    visitValaCompositeElement(o);
  }

  public void visitLambdaExpressionParams(@NotNull BnfLambdaExpressionParams o) {
    visitValaCompositeElement(o);
  }

  public void visitLiteral(@NotNull BnfLiteral o) {
    visitValaCompositeElement(o);
  }

  public void visitLocalTupleDeclaration(@NotNull BnfLocalTupleDeclaration o) {
    visitValaCompositeElement(o);
  }

  public void visitLocalVariable(@NotNull BnfLocalVariable o) {
    visitValaCompositeElement(o);
  }

  public void visitLocalVariableDeclaration(@NotNull BnfLocalVariableDeclaration o) {
    visitValaCompositeElement(o);
  }

  public void visitLocalVariableDeclarations(@NotNull BnfLocalVariableDeclarations o) {
    visitValaCompositeElement(o);
  }

  public void visitLockStatement(@NotNull BnfLockStatement o) {
    visitValaCompositeElement(o);
  }

  public void visitMember(@NotNull BnfMember o) {
    visitValaCompositeElement(o);
  }

  public void visitMemberAccess(@NotNull BnfMemberAccess o) {
    visitValaCompositeElement(o);
  }

  public void visitMemberDeclarationModifier(@NotNull BnfMemberDeclarationModifier o) {
    visitValaCompositeElement(o);
  }

  public void visitMemberDeclarationModifiers(@NotNull BnfMemberDeclarationModifiers o) {
    visitValaCompositeElement(o);
  }

  public void visitMemberInitializer(@NotNull BnfMemberInitializer o) {
    visitValaCompositeElement(o);
  }

  public void visitMethodCall(@NotNull BnfMethodCall o) {
    visitValaCompositeElement(o);
  }

  public void visitMethodDeclaration(@NotNull BnfMethodDeclaration o) {
    visitValaCompositeElement(o);
  }

  public void visitMultiplicativeExpression(@NotNull BnfMultiplicativeExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitNamespaceDeclaration(@NotNull BnfNamespaceDeclaration o) {
    visitValaCompositeElement(o);
  }

  public void visitNamespaceMember(@NotNull BnfNamespaceMember o) {
    visitValaCompositeElement(o);
  }

  public void visitObjectCreationExpression(@NotNull BnfObjectCreationExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitObjectInitializer(@NotNull BnfObjectInitializer o) {
    visitValaCompositeElement(o);
  }

  public void visitObjectOrArrayCreationExpression(@NotNull BnfObjectOrArrayCreationExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitParameter(@NotNull BnfParameter o) {
    visitValaCompositeElement(o);
  }

  public void visitParameters(@NotNull BnfParameters o) {
    visitValaCompositeElement(o);
  }

  public void visitParametersDecl(@NotNull BnfParametersDecl o) {
    visitValaCompositeElement(o);
  }

  public void visitPointerMemberAccess(@NotNull BnfPointerMemberAccess o) {
    visitValaCompositeElement(o);
  }

  public void visitPostDecrementExpression(@NotNull BnfPostDecrementExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitPostIncrementExpression(@NotNull BnfPostIncrementExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitPrimaryExpression(@NotNull BnfPrimaryExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitPropertyAccessor(@NotNull BnfPropertyAccessor o) {
    visitValaCompositeElement(o);
  }

  public void visitPropertyDeclaration(@NotNull BnfPropertyDeclaration o) {
    visitValaCompositeElement(o);
  }

  public void visitPropertyDeclarationModifier(@NotNull BnfPropertyDeclarationModifier o) {
    visitValaCompositeElement(o);
  }

  public void visitPropertyDeclarationModifiers(@NotNull BnfPropertyDeclarationModifiers o) {
    visitValaCompositeElement(o);
  }

  public void visitPropertyDeclarationPart(@NotNull BnfPropertyDeclarationPart o) {
    visitValaCompositeElement(o);
  }

  public void visitPropertyGetAccessor(@NotNull BnfPropertyGetAccessor o) {
    visitValaCompositeElement(o);
  }

  public void visitPropertySetConstructAccessor(@NotNull BnfPropertySetConstructAccessor o) {
    visitValaCompositeElement(o);
  }

  public void visitRelationalExpression(@NotNull BnfRelationalExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitRequiresDecl(@NotNull BnfRequiresDecl o) {
    visitValaCompositeElement(o);
  }

  public void visitReturnStatement(@NotNull BnfReturnStatement o) {
    visitValaCompositeElement(o);
  }

  public void visitShiftExpression(@NotNull BnfShiftExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitSignalDeclaration(@NotNull BnfSignalDeclaration o) {
    visitValaCompositeElement(o);
  }

  public void visitSignalDeclarationModifier(@NotNull BnfSignalDeclarationModifier o) {
    visitValaCompositeElement(o);
  }

  public void visitSignalDeclarationModifiers(@NotNull BnfSignalDeclarationModifiers o) {
    visitValaCompositeElement(o);
  }

  public void visitSimpleName(@NotNull BnfSimpleName o) {
    visitValaCompositeElement(o);
  }

  public void visitSizeofExpression(@NotNull BnfSizeofExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitSliceArray(@NotNull BnfSliceArray o) {
    visitValaCompositeElement(o);
  }

  public void visitStatement(@NotNull BnfStatement o) {
    visitValaCompositeElement(o);
  }

  public void visitStatementExpression(@NotNull BnfStatementExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitStructDeclaration(@NotNull BnfStructDeclaration o) {
    visitValaCompositeElement(o);
  }

  public void visitStructMember(@NotNull BnfStructMember o) {
    visitValaCompositeElement(o);
  }

  public void visitSwitchSection(@NotNull BnfSwitchSection o) {
    visitValaCompositeElement(o);
  }

  public void visitSwitchStatement(@NotNull BnfSwitchStatement o) {
    visitValaCompositeElement(o);
  }

  public void visitSymbol(@NotNull BnfSymbol o) {
    visitValaCompositeElement(o);
  }

  public void visitSymbolPart(@NotNull BnfSymbolPart o) {
    visitValaCompositeElement(o);
  }

  public void visitTemplate(@NotNull BnfTemplate o) {
    visitValaCompositeElement(o);
  }

  public void visitThisAccess(@NotNull BnfThisAccess o) {
    visitValaCompositeElement(o);
  }

  public void visitThrowStatement(@NotNull BnfThrowStatement o) {
    visitValaCompositeElement(o);
  }

  public void visitThrowsPart(@NotNull BnfThrowsPart o) {
    visitValaCompositeElement(o);
  }

  public void visitTryStatement(@NotNull BnfTryStatement o) {
    visitValaCompositeElement(o);
  }

  public void visitTuple(@NotNull BnfTuple o) {
    visitValaCompositeElement(o);
  }

  public void visitType(@NotNull BnfType o) {
    visitValaCompositeElement(o);
  }

  public void visitTypeArguments(@NotNull BnfTypeArguments o) {
    visitValaCompositeElement(o);
  }

  public void visitTypeDeclarationModifier(@NotNull BnfTypeDeclarationModifier o) {
    visitValaCompositeElement(o);
  }

  public void visitTypeDeclarationModifiers(@NotNull BnfTypeDeclarationModifiers o) {
    visitValaCompositeElement(o);
  }

  public void visitTypeParameters(@NotNull BnfTypeParameters o) {
    visitValaCompositeElement(o);
  }

  public void visitTypeofExpression(@NotNull BnfTypeofExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitUnaryExpression(@NotNull BnfUnaryExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitUnaryOperator(@NotNull BnfUnaryOperator o) {
    visitValaCompositeElement(o);
  }

  public void visitUsingDirective(@NotNull BnfUsingDirective o) {
    visitValaCompositeElement(o);
  }

  public void visitWhileStatement(@NotNull BnfWhileStatement o) {
    visitValaCompositeElement(o);
  }

  public void visitYieldExpression(@NotNull BnfYieldExpression o) {
    visitValaCompositeElement(o);
  }

  public void visitYieldStatement(@NotNull BnfYieldStatement o) {
    visitValaCompositeElement(o);
  }

  public void visitValaCompositeElement(@NotNull ValaCompositeElement o) {
    visitElement(o);
  }

}
