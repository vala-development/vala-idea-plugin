// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface BnfTuple extends ValaCompositeElement {

  @NotNull
  List<BnfExpression> getExpressionList();

}
