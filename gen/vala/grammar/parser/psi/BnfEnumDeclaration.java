// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface BnfEnumDeclaration extends ValaCompositeElement {

  @Nullable
  BnfAccessModifier getAccessModifier();

  @NotNull
  List<BnfEnumMember> getEnumMemberList();

  @NotNull
  BnfSymbol getSymbol();

  @Nullable
  BnfTypeDeclarationModifiers getTypeDeclarationModifiers();

}
