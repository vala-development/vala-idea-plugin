// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static vala.grammar.parser.psi.ValaTypes.*;
import vala.grammar.parser.psi.*;

public class BnfInterfaceDeclarationImpl extends ValaCompositeElementImpl implements BnfInterfaceDeclaration {

  public BnfInterfaceDeclarationImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof BnfVisitor) ((BnfVisitor)visitor).visitInterfaceDeclaration(this);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public BnfAccessModifier getAccessModifier() {
    return findChildByClass(BnfAccessModifier.class);
  }

  @Override
  @Nullable
  public BnfBaseTypes getBaseTypes() {
    return findChildByClass(BnfBaseTypes.class);
  }

  @Override
  @NotNull
  public List<BnfInterfaceMember> getInterfaceMemberList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, BnfInterfaceMember.class);
  }

  @Override
  @NotNull
  public BnfSymbol getSymbol() {
    return findNotNullChildByClass(BnfSymbol.class);
  }

  @Override
  @Nullable
  public BnfTypeDeclarationModifiers getTypeDeclarationModifiers() {
    return findChildByClass(BnfTypeDeclarationModifiers.class);
  }

  @Override
  @Nullable
  public BnfTypeParameters getTypeParameters() {
    return findChildByClass(BnfTypeParameters.class);
  }

}
