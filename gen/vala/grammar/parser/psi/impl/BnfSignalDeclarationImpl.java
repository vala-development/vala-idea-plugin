// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static vala.grammar.parser.psi.ValaTypes.*;
import vala.grammar.parser.psi.*;

public class BnfSignalDeclarationImpl extends ValaCompositeElementImpl implements BnfSignalDeclaration {

  public BnfSignalDeclarationImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof BnfVisitor) ((BnfVisitor)visitor).visitSignalDeclaration(this);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public BnfAccessModifier getAccessModifier() {
    return findChildByClass(BnfAccessModifier.class);
  }

  @Override
  @Nullable
  public BnfBlock getBlock() {
    return findChildByClass(BnfBlock.class);
  }

  @Override
  @NotNull
  public BnfParameters getParameters() {
    return findNotNullChildByClass(BnfParameters.class);
  }

  @Override
  @Nullable
  public BnfSignalDeclarationModifiers getSignalDeclarationModifiers() {
    return findChildByClass(BnfSignalDeclarationModifiers.class);
  }

  @Override
  @NotNull
  public BnfType getType() {
    return findNotNullChildByClass(BnfType.class);
  }

  @Override
  @NotNull
  public PsiElement getIdentifier() {
    return findNotNullChildByType(IDENTIFIER);
  }

}
