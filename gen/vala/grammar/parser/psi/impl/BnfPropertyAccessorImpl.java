// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static vala.grammar.parser.psi.ValaTypes.*;
import vala.grammar.parser.psi.*;

public class BnfPropertyAccessorImpl extends ValaCompositeElementImpl implements BnfPropertyAccessor {

  public BnfPropertyAccessorImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof BnfVisitor) ((BnfVisitor)visitor).visitPropertyAccessor(this);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public BnfAccessModifier getAccessModifier() {
    return findChildByClass(BnfAccessModifier.class);
  }

  @Override
  @Nullable
  public BnfAttributes getAttributes() {
    return findChildByClass(BnfAttributes.class);
  }

  @Override
  @Nullable
  public BnfPropertyGetAccessor getPropertyGetAccessor() {
    return findChildByClass(BnfPropertyGetAccessor.class);
  }

  @Override
  @Nullable
  public BnfPropertySetConstructAccessor getPropertySetConstructAccessor() {
    return findChildByClass(BnfPropertySetConstructAccessor.class);
  }

}
