// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static vala.grammar.parser.psi.ValaTypes.*;
import vala.grammar.parser.psi.*;

public class BnfNamespaceMemberImpl extends ValaCompositeElementImpl implements BnfNamespaceMember {

  public BnfNamespaceMemberImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof BnfVisitor) ((BnfVisitor)visitor).visitNamespaceMember(this);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public BnfAttributes getAttributes() {
    return findChildByClass(BnfAttributes.class);
  }

  @Override
  @Nullable
  public BnfClassDeclaration getClassDeclaration() {
    return findChildByClass(BnfClassDeclaration.class);
  }

  @Override
  @Nullable
  public BnfConstantDeclaration getConstantDeclaration() {
    return findChildByClass(BnfConstantDeclaration.class);
  }

  @Override
  @Nullable
  public BnfDelegateDeclaration getDelegateDeclaration() {
    return findChildByClass(BnfDelegateDeclaration.class);
  }

  @Override
  @Nullable
  public BnfEnumDeclaration getEnumDeclaration() {
    return findChildByClass(BnfEnumDeclaration.class);
  }

  @Override
  @Nullable
  public BnfErrordomainDeclaration getErrordomainDeclaration() {
    return findChildByClass(BnfErrordomainDeclaration.class);
  }

  @Override
  @Nullable
  public BnfFieldDeclaration getFieldDeclaration() {
    return findChildByClass(BnfFieldDeclaration.class);
  }

  @Override
  @Nullable
  public BnfInterfaceDeclaration getInterfaceDeclaration() {
    return findChildByClass(BnfInterfaceDeclaration.class);
  }

  @Override
  @Nullable
  public BnfMethodDeclaration getMethodDeclaration() {
    return findChildByClass(BnfMethodDeclaration.class);
  }

  @Override
  @Nullable
  public BnfNamespaceDeclaration getNamespaceDeclaration() {
    return findChildByClass(BnfNamespaceDeclaration.class);
  }

  @Override
  @Nullable
  public BnfStructDeclaration getStructDeclaration() {
    return findChildByClass(BnfStructDeclaration.class);
  }

}
