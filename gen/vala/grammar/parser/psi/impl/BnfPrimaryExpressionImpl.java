// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static vala.grammar.parser.psi.ValaTypes.*;
import vala.grammar.parser.psi.*;

public class BnfPrimaryExpressionImpl extends ValaCompositeElementImpl implements BnfPrimaryExpression {

  public BnfPrimaryExpressionImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof BnfVisitor) ((BnfVisitor)visitor).visitPrimaryExpression(this);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public BnfBaseAccess getBaseAccess() {
    return findChildByClass(BnfBaseAccess.class);
  }

  @Override
  @NotNull
  public List<BnfElementAccess> getElementAccessList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, BnfElementAccess.class);
  }

  @Override
  @Nullable
  public BnfInitializer getInitializer() {
    return findChildByClass(BnfInitializer.class);
  }

  @Override
  @Nullable
  public BnfLiteral getLiteral() {
    return findChildByClass(BnfLiteral.class);
  }

  @Override
  @NotNull
  public List<BnfMemberAccess> getMemberAccessList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, BnfMemberAccess.class);
  }

  @Override
  @NotNull
  public List<BnfMethodCall> getMethodCallList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, BnfMethodCall.class);
  }

  @Override
  @Nullable
  public BnfObjectOrArrayCreationExpression getObjectOrArrayCreationExpression() {
    return findChildByClass(BnfObjectOrArrayCreationExpression.class);
  }

  @Override
  @NotNull
  public List<BnfPointerMemberAccess> getPointerMemberAccessList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, BnfPointerMemberAccess.class);
  }

  @Override
  @NotNull
  public List<BnfPostDecrementExpression> getPostDecrementExpressionList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, BnfPostDecrementExpression.class);
  }

  @Override
  @NotNull
  public List<BnfPostIncrementExpression> getPostIncrementExpressionList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, BnfPostIncrementExpression.class);
  }

  @Override
  @Nullable
  public BnfSimpleName getSimpleName() {
    return findChildByClass(BnfSimpleName.class);
  }

  @Override
  @Nullable
  public BnfSizeofExpression getSizeofExpression() {
    return findChildByClass(BnfSizeofExpression.class);
  }

  @Override
  @Nullable
  public BnfTemplate getTemplate() {
    return findChildByClass(BnfTemplate.class);
  }

  @Override
  @Nullable
  public BnfThisAccess getThisAccess() {
    return findChildByClass(BnfThisAccess.class);
  }

  @Override
  @Nullable
  public BnfTuple getTuple() {
    return findChildByClass(BnfTuple.class);
  }

  @Override
  @Nullable
  public BnfTypeofExpression getTypeofExpression() {
    return findChildByClass(BnfTypeofExpression.class);
  }

  @Override
  @Nullable
  public BnfYieldExpression getYieldExpression() {
    return findChildByClass(BnfYieldExpression.class);
  }

  @Override
  @Nullable
  public PsiElement getRegexLiteral() {
    return findChildByType(REGEX_LITERAL);
  }

}
