// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static vala.grammar.parser.psi.ValaTypes.*;
import vala.grammar.parser.psi.*;

public class BnfConstantDeclarationImpl extends ValaCompositeElementImpl implements BnfConstantDeclaration {

  public BnfConstantDeclarationImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof BnfVisitor) ((BnfVisitor)visitor).visitConstantDeclaration(this);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public BnfAccessModifier getAccessModifier() {
    return findChildByClass(BnfAccessModifier.class);
  }

  @Override
  @Nullable
  public BnfExpression getExpression() {
    return findChildByClass(BnfExpression.class);
  }

  @Override
  @Nullable
  public BnfInlineArrayType getInlineArrayType() {
    return findChildByClass(BnfInlineArrayType.class);
  }

  @Override
  @Nullable
  public BnfMemberDeclarationModifiers getMemberDeclarationModifiers() {
    return findChildByClass(BnfMemberDeclarationModifiers.class);
  }

  @Override
  @NotNull
  public BnfType getType() {
    return findNotNullChildByClass(BnfType.class);
  }

  @Override
  @NotNull
  public PsiElement getIdentifier() {
    return findNotNullChildByType(IDENTIFIER);
  }

}
