// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static vala.grammar.parser.psi.ValaTypes.*;
import vala.grammar.parser.psi.*;

public class BnfExpressionImpl extends ValaCompositeElementImpl implements BnfExpression {

  public BnfExpressionImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof BnfVisitor) ((BnfVisitor)visitor).visitExpression(this);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public BnfAssignmentOperator getAssignmentOperator() {
    return findChildByClass(BnfAssignmentOperator.class);
  }

  @Override
  @Nullable
  public BnfConditionalExpression getConditionalExpression() {
    return findChildByClass(BnfConditionalExpression.class);
  }

  @Override
  @Nullable
  public BnfExpression getExpression() {
    return findChildByClass(BnfExpression.class);
  }

  @Override
  @Nullable
  public BnfLambdaExpression getLambdaExpression() {
    return findChildByClass(BnfLambdaExpression.class);
  }

}
