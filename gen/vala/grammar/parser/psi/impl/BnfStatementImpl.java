// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static vala.grammar.parser.psi.ValaTypes.*;
import vala.grammar.parser.psi.*;

public class BnfStatementImpl extends ValaCompositeElementImpl implements BnfStatement {

  public BnfStatementImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof BnfVisitor) ((BnfVisitor)visitor).visitStatement(this);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public BnfBlock getBlock() {
    return findChildByClass(BnfBlock.class);
  }

  @Override
  @Nullable
  public BnfBreakStatement getBreakStatement() {
    return findChildByClass(BnfBreakStatement.class);
  }

  @Override
  @Nullable
  public BnfContinueStatement getContinueStatement() {
    return findChildByClass(BnfContinueStatement.class);
  }

  @Override
  @Nullable
  public BnfDeleteStatement getDeleteStatement() {
    return findChildByClass(BnfDeleteStatement.class);
  }

  @Override
  @Nullable
  public BnfDoStatement getDoStatement() {
    return findChildByClass(BnfDoStatement.class);
  }

  @Override
  @Nullable
  public BnfExpressionStatement getExpressionStatement() {
    return findChildByClass(BnfExpressionStatement.class);
  }

  @Override
  @Nullable
  public BnfForStatement getForStatement() {
    return findChildByClass(BnfForStatement.class);
  }

  @Override
  @Nullable
  public BnfForeachStatement getForeachStatement() {
    return findChildByClass(BnfForeachStatement.class);
  }

  @Override
  @Nullable
  public BnfIfStatement getIfStatement() {
    return findChildByClass(BnfIfStatement.class);
  }

  @Override
  @Nullable
  public BnfLocalVariableDeclarations getLocalVariableDeclarations() {
    return findChildByClass(BnfLocalVariableDeclarations.class);
  }

  @Override
  @Nullable
  public BnfLockStatement getLockStatement() {
    return findChildByClass(BnfLockStatement.class);
  }

  @Override
  @Nullable
  public BnfReturnStatement getReturnStatement() {
    return findChildByClass(BnfReturnStatement.class);
  }

  @Override
  @Nullable
  public BnfSwitchStatement getSwitchStatement() {
    return findChildByClass(BnfSwitchStatement.class);
  }

  @Override
  @Nullable
  public BnfThrowStatement getThrowStatement() {
    return findChildByClass(BnfThrowStatement.class);
  }

  @Override
  @Nullable
  public BnfTryStatement getTryStatement() {
    return findChildByClass(BnfTryStatement.class);
  }

  @Override
  @Nullable
  public BnfWhileStatement getWhileStatement() {
    return findChildByClass(BnfWhileStatement.class);
  }

  @Override
  @Nullable
  public BnfYieldStatement getYieldStatement() {
    return findChildByClass(BnfYieldStatement.class);
  }

}
