// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static vala.grammar.parser.psi.ValaTypes.*;
import vala.grammar.parser.psi.*;

public class BnfPropertyDeclarationImpl extends ValaCompositeElementImpl implements BnfPropertyDeclaration {

  public BnfPropertyDeclarationImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof BnfVisitor) ((BnfVisitor)visitor).visitPropertyDeclaration(this);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public BnfAccessModifier getAccessModifier() {
    return findChildByClass(BnfAccessModifier.class);
  }

  @Override
  @Nullable
  public BnfPropertyDeclarationModifiers getPropertyDeclarationModifiers() {
    return findChildByClass(BnfPropertyDeclarationModifiers.class);
  }

  @Override
  @NotNull
  public List<BnfPropertyDeclarationPart> getPropertyDeclarationPartList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, BnfPropertyDeclarationPart.class);
  }

  @Override
  @NotNull
  public BnfType getType() {
    return findNotNullChildByClass(BnfType.class);
  }

  @Override
  @NotNull
  public PsiElement getIdentifier() {
    return findNotNullChildByType(IDENTIFIER);
  }

}
