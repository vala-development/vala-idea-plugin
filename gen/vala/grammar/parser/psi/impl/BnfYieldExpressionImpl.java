// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static vala.grammar.parser.psi.ValaTypes.*;
import vala.grammar.parser.psi.*;

public class BnfYieldExpressionImpl extends ValaCompositeElementImpl implements BnfYieldExpression {

  public BnfYieldExpressionImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof BnfVisitor) ((BnfVisitor)visitor).visitYieldExpression(this);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public BnfBaseAccess getBaseAccess() {
    return findChildByClass(BnfBaseAccess.class);
  }

  @Override
  @NotNull
  public BnfMember getMember() {
    return findNotNullChildByClass(BnfMember.class);
  }

  @Override
  @NotNull
  public BnfMethodCall getMethodCall() {
    return findNotNullChildByClass(BnfMethodCall.class);
  }

}
