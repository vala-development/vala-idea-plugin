// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static vala.grammar.parser.psi.ValaTypes.*;
import vala.grammar.parser.psi.*;

public class BnfLambdaExpressionBodyImpl extends ValaCompositeElementImpl implements BnfLambdaExpressionBody {

  public BnfLambdaExpressionBodyImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof BnfVisitor) ((BnfVisitor)visitor).visitLambdaExpressionBody(this);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public BnfBlock getBlock() {
    return findChildByClass(BnfBlock.class);
  }

  @Override
  @Nullable
  public BnfExpression getExpression() {
    return findChildByClass(BnfExpression.class);
  }

}
