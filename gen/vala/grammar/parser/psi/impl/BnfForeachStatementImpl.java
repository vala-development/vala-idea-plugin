// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static vala.grammar.parser.psi.ValaTypes.*;
import vala.grammar.parser.psi.*;

public class BnfForeachStatementImpl extends ValaCompositeElementImpl implements BnfForeachStatement {

  public BnfForeachStatementImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof BnfVisitor) ((BnfVisitor)visitor).visitForeachStatement(this);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public BnfEmbeddedStatement getEmbeddedStatement() {
    return findNotNullChildByClass(BnfEmbeddedStatement.class);
  }

  @Override
  @NotNull
  public BnfExpression getExpression() {
    return findNotNullChildByClass(BnfExpression.class);
  }

  @Override
  @Nullable
  public BnfType getType() {
    return findChildByClass(BnfType.class);
  }

  @Override
  @NotNull
  public PsiElement getIdentifier() {
    return findNotNullChildByType(IDENTIFIER);
  }

}
