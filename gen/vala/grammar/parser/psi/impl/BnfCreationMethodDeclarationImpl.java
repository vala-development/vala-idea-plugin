// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static vala.grammar.parser.psi.ValaTypes.*;
import vala.grammar.parser.psi.*;

public class BnfCreationMethodDeclarationImpl extends ValaCompositeElementImpl implements BnfCreationMethodDeclaration {

  public BnfCreationMethodDeclarationImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof BnfVisitor) ((BnfVisitor)visitor).visitCreationMethodDeclaration(this);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public BnfAccessModifier getAccessModifier() {
    return findChildByClass(BnfAccessModifier.class);
  }

  @Override
  @Nullable
  public BnfBlock getBlock() {
    return findChildByClass(BnfBlock.class);
  }

  @Override
  @Nullable
  public BnfConstructorDeclarationModifiers getConstructorDeclarationModifiers() {
    return findChildByClass(BnfConstructorDeclarationModifiers.class);
  }

  @Override
  @Nullable
  public BnfEnsuresDecl getEnsuresDecl() {
    return findChildByClass(BnfEnsuresDecl.class);
  }

  @Override
  @NotNull
  public BnfParameters getParameters() {
    return findNotNullChildByClass(BnfParameters.class);
  }

  @Override
  @Nullable
  public BnfRequiresDecl getRequiresDecl() {
    return findChildByClass(BnfRequiresDecl.class);
  }

  @Override
  @NotNull
  public BnfSymbol getSymbol() {
    return findNotNullChildByClass(BnfSymbol.class);
  }

  @Override
  @Nullable
  public BnfThrowsPart getThrowsPart() {
    return findChildByClass(BnfThrowsPart.class);
  }

}
