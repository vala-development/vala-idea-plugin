// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static vala.grammar.parser.psi.ValaTypes.*;
import vala.grammar.parser.psi.*;

public class BnfConditionalAndExpressionImpl extends ValaCompositeElementImpl implements BnfConditionalAndExpression {

  public BnfConditionalAndExpressionImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof BnfVisitor) ((BnfVisitor)visitor).visitConditionalAndExpression(this);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<BnfInExpression> getInExpressionList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, BnfInExpression.class);
  }

}
