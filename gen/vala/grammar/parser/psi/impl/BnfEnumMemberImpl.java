// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static vala.grammar.parser.psi.ValaTypes.*;
import vala.grammar.parser.psi.*;

public class BnfEnumMemberImpl extends ValaCompositeElementImpl implements BnfEnumMember {

  public BnfEnumMemberImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof BnfVisitor) ((BnfVisitor)visitor).visitEnumMember(this);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public BnfAttributes getAttributes() {
    return findChildByClass(BnfAttributes.class);
  }

  @Override
  @Nullable
  public BnfConstantDeclaration getConstantDeclaration() {
    return findChildByClass(BnfConstantDeclaration.class);
  }

  @Override
  @Nullable
  public BnfEnumValues getEnumValues() {
    return findChildByClass(BnfEnumValues.class);
  }

  @Override
  @Nullable
  public BnfMethodDeclaration getMethodDeclaration() {
    return findChildByClass(BnfMethodDeclaration.class);
  }

}
