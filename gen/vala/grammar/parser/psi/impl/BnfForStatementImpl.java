// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static vala.grammar.parser.psi.ValaTypes.*;
import vala.grammar.parser.psi.*;

public class BnfForStatementImpl extends ValaCompositeElementImpl implements BnfForStatement {

  public BnfForStatementImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof BnfVisitor) ((BnfVisitor)visitor).visitForStatement(this);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public BnfEmbeddedStatement getEmbeddedStatement() {
    return findNotNullChildByClass(BnfEmbeddedStatement.class);
  }

  @Override
  @Nullable
  public BnfExpression getExpression() {
    return findChildByClass(BnfExpression.class);
  }

  @Override
  @Nullable
  public BnfForInitializer getForInitializer() {
    return findChildByClass(BnfForInitializer.class);
  }

  @Override
  @Nullable
  public BnfForIterator getForIterator() {
    return findChildByClass(BnfForIterator.class);
  }

}
