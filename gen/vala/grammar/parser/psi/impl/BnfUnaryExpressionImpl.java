// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static vala.grammar.parser.psi.ValaTypes.*;
import vala.grammar.parser.psi.*;

public class BnfUnaryExpressionImpl extends ValaCompositeElementImpl implements BnfUnaryExpression {

  public BnfUnaryExpressionImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof BnfVisitor) ((BnfVisitor)visitor).visitUnaryExpression(this);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public BnfPrimaryExpression getPrimaryExpression() {
    return findChildByClass(BnfPrimaryExpression.class);
  }

  @Override
  @Nullable
  public BnfUnaryExpression getUnaryExpression() {
    return findChildByClass(BnfUnaryExpression.class);
  }

  @Override
  @Nullable
  public BnfUnaryOperator getUnaryOperator() {
    return findChildByClass(BnfUnaryOperator.class);
  }

}
