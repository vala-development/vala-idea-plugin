// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static vala.grammar.parser.psi.ValaTypes.*;
import vala.grammar.parser.psi.*;

public class BnfSwitchSectionImpl extends ValaCompositeElementImpl implements BnfSwitchSection {

  public BnfSwitchSectionImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof BnfVisitor) ((BnfVisitor)visitor).visitSwitchSection(this);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public BnfExpression getExpression() {
    return findChildByClass(BnfExpression.class);
  }

  @Override
  @NotNull
  public List<BnfStatement> getStatementList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, BnfStatement.class);
  }

}
