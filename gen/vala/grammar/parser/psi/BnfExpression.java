// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface BnfExpression extends ValaCompositeElement {

  @Nullable
  BnfAssignmentOperator getAssignmentOperator();

  @Nullable
  BnfConditionalExpression getConditionalExpression();

  @Nullable
  BnfExpression getExpression();

  @Nullable
  BnfLambdaExpression getLambdaExpression();

}
