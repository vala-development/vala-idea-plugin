// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface BnfMethodDeclaration extends ValaCompositeElement {

  @Nullable
  BnfAccessModifier getAccessModifier();

  @Nullable
  BnfBlock getBlock();

  @Nullable
  BnfEnsuresDecl getEnsuresDecl();

  @Nullable
  BnfMemberDeclarationModifiers getMemberDeclarationModifiers();

  @NotNull
  BnfParameters getParameters();

  @Nullable
  BnfRequiresDecl getRequiresDecl();

  @Nullable
  BnfThrowsPart getThrowsPart();

  @NotNull
  BnfType getType();

  @Nullable
  BnfTypeParameters getTypeParameters();

  @NotNull
  PsiElement getIdentifier();

}
