// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface BnfDestructorDeclaration extends ValaCompositeElement {

  @NotNull
  BnfBlock getBlock();

  @Nullable
  BnfConstructorDeclarationModifiers getConstructorDeclarationModifiers();

  @NotNull
  PsiElement getIdentifier();

}
