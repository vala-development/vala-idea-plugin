// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface BnfConstantDeclaration extends ValaCompositeElement {

  @Nullable
  BnfAccessModifier getAccessModifier();

  @Nullable
  BnfExpression getExpression();

  @Nullable
  BnfInlineArrayType getInlineArrayType();

  @Nullable
  BnfMemberDeclarationModifiers getMemberDeclarationModifiers();

  @NotNull
  BnfType getType();

  @NotNull
  PsiElement getIdentifier();

}
