// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface BnfLiteral extends ValaCompositeElement {

  @Nullable
  PsiElement getChar();

  @Nullable
  PsiElement getHex();

  @Nullable
  PsiElement getInteger();

  @Nullable
  PsiElement getReal();

  @Nullable
  PsiElement getRegexLiteral();

  @Nullable
  PsiElement getString();

}
