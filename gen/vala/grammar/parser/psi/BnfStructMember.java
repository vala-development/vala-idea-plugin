// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface BnfStructMember extends ValaCompositeElement {

  @Nullable
  BnfAttributes getAttributes();

  @Nullable
  BnfConstantDeclaration getConstantDeclaration();

  @Nullable
  BnfCreationMethodDeclaration getCreationMethodDeclaration();

  @Nullable
  BnfFieldDeclaration getFieldDeclaration();

  @Nullable
  BnfMethodDeclaration getMethodDeclaration();

  @Nullable
  BnfPropertyDeclaration getPropertyDeclaration();

}
