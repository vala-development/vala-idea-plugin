// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.PsiElement;
import com.intellij.lang.ASTNode;
import vala.grammar.parser.psi.impl.*;

public interface ValaTypes {

  IElementType ACCESS_MODIFIER = new ValaElementType("ACCESS_MODIFIER");
  IElementType ADDITIVE_EXPRESSION = new ValaElementType("ADDITIVE_EXPRESSION");
  IElementType AND_EXPRESSION = new ValaElementType("AND_EXPRESSION");
  IElementType ARGUMENT = new ValaElementType("ARGUMENT");
  IElementType ARGUMENTS = new ValaElementType("ARGUMENTS");
  IElementType ARRAY_CREATION_EXPRESSION = new ValaElementType("ARRAY_CREATION_EXPRESSION");
  IElementType ARRAY_SIZE = new ValaElementType("ARRAY_SIZE");
  IElementType ARRAY_TYPE = new ValaElementType("ARRAY_TYPE");
  IElementType ASSIGNMENT_OPERATOR = new ValaElementType("ASSIGNMENT_OPERATOR");
  IElementType ATTRIBUTE = new ValaElementType("ATTRIBUTE");
  IElementType ATTRIBUTES = new ValaElementType("ATTRIBUTES");
  IElementType ATTRIBUTE_ARGUMENT = new ValaElementType("ATTRIBUTE_ARGUMENT");
  IElementType ATTRIBUTE_ARGUMENTS = new ValaElementType("ATTRIBUTE_ARGUMENTS");
  IElementType BASE_ACCESS = new ValaElementType("BASE_ACCESS");
  IElementType BASE_TYPES = new ValaElementType("BASE_TYPES");
  IElementType BLOCK = new ValaElementType("BLOCK");
  IElementType BREAK_STATEMENT = new ValaElementType("BREAK_STATEMENT");
  IElementType CAST_OPERATOR = new ValaElementType("CAST_OPERATOR");
  IElementType CATCH_CLAUSE = new ValaElementType("CATCH_CLAUSE");
  IElementType CLASS_DECLARATION = new ValaElementType("CLASS_DECLARATION");
  IElementType CLASS_MEMBER = new ValaElementType("CLASS_MEMBER");
  IElementType COALESCING_EXPRESSION = new ValaElementType("COALESCING_EXPRESSION");
  IElementType CONDITIONAL_AND_EXPRESSION = new ValaElementType("CONDITIONAL_AND_EXPRESSION");
  IElementType CONDITIONAL_EXPRESSION = new ValaElementType("CONDITIONAL_EXPRESSION");
  IElementType CONDITIONAL_OR_EXPRESSION = new ValaElementType("CONDITIONAL_OR_EXPRESSION");
  IElementType CONSTANT_DECLARATION = new ValaElementType("CONSTANT_DECLARATION");
  IElementType CONSTRUCTOR_DECLARATION = new ValaElementType("CONSTRUCTOR_DECLARATION");
  IElementType CONSTRUCTOR_DECLARATION_MODIFIER = new ValaElementType("CONSTRUCTOR_DECLARATION_MODIFIER");
  IElementType CONSTRUCTOR_DECLARATION_MODIFIERS = new ValaElementType("CONSTRUCTOR_DECLARATION_MODIFIERS");
  IElementType CONTINUE_STATEMENT = new ValaElementType("CONTINUE_STATEMENT");
  IElementType CREATION_METHOD_DECLARATION = new ValaElementType("CREATION_METHOD_DECLARATION");
  IElementType DELEGATE_DECLARATION = new ValaElementType("DELEGATE_DECLARATION");
  IElementType DELEGATE_DECLARATION_MODIFIER = new ValaElementType("DELEGATE_DECLARATION_MODIFIER");
  IElementType DELEGATE_DECLARATION_MODIFIERS = new ValaElementType("DELEGATE_DECLARATION_MODIFIERS");
  IElementType DELETE_STATEMENT = new ValaElementType("DELETE_STATEMENT");
  IElementType DESTRUCTOR_DECLARATION = new ValaElementType("DESTRUCTOR_DECLARATION");
  IElementType DO_STATEMENT = new ValaElementType("DO_STATEMENT");
  IElementType ELEMENT_ACCESS = new ValaElementType("ELEMENT_ACCESS");
  IElementType EMBEDDED_STATEMENT = new ValaElementType("EMBEDDED_STATEMENT");
  IElementType EMBEDDED_STATEMENT_WITHOUT_BLOCK = new ValaElementType("EMBEDDED_STATEMENT_WITHOUT_BLOCK");
  IElementType ENSURES_DECL = new ValaElementType("ENSURES_DECL");
  IElementType ENUM_DECLARATION = new ValaElementType("ENUM_DECLARATION");
  IElementType ENUM_MEMBER = new ValaElementType("ENUM_MEMBER");
  IElementType ENUM_VALUE = new ValaElementType("ENUM_VALUE");
  IElementType ENUM_VALUES = new ValaElementType("ENUM_VALUES");
  IElementType EQUALITY_EXPRESSION = new ValaElementType("EQUALITY_EXPRESSION");
  IElementType ERRORCODE = new ValaElementType("ERRORCODE");
  IElementType ERRORCODES = new ValaElementType("ERRORCODES");
  IElementType ERRORDOMAIN_DECLARATION = new ValaElementType("ERRORDOMAIN_DECLARATION");
  IElementType EXCLUSIVE_OR_EXPRESSION = new ValaElementType("EXCLUSIVE_OR_EXPRESSION");
  IElementType EXPRESSION = new ValaElementType("EXPRESSION");
  IElementType EXPRESSION_STATEMENT = new ValaElementType("EXPRESSION_STATEMENT");
  IElementType FIELD_DECLARATION = new ValaElementType("FIELD_DECLARATION");
  IElementType FINALLY_CLAUSE = new ValaElementType("FINALLY_CLAUSE");
  IElementType FOREACH_STATEMENT = new ValaElementType("FOREACH_STATEMENT");
  IElementType FOR_INITIALIZER = new ValaElementType("FOR_INITIALIZER");
  IElementType FOR_ITERATOR = new ValaElementType("FOR_ITERATOR");
  IElementType FOR_STATEMENT = new ValaElementType("FOR_STATEMENT");
  IElementType IF_STATEMENT = new ValaElementType("IF_STATEMENT");
  IElementType INCLUSIVE_OR_EXPRESSION = new ValaElementType("INCLUSIVE_OR_EXPRESSION");
  IElementType INITIALIZER = new ValaElementType("INITIALIZER");
  IElementType INLINE_ARRAY_TYPE = new ValaElementType("INLINE_ARRAY_TYPE");
  IElementType INTERFACE_DECLARATION = new ValaElementType("INTERFACE_DECLARATION");
  IElementType INTERFACE_MEMBER = new ValaElementType("INTERFACE_MEMBER");
  IElementType IN_EXPRESSION = new ValaElementType("IN_EXPRESSION");
  IElementType LAMBDA_EXPRESSION = new ValaElementType("LAMBDA_EXPRESSION");
  IElementType LAMBDA_EXPRESSION_BODY = new ValaElementType("LAMBDA_EXPRESSION_BODY");
  IElementType LAMBDA_EXPRESSION_PARAMS = new ValaElementType("LAMBDA_EXPRESSION_PARAMS");
  IElementType LITERAL = new ValaElementType("LITERAL");
  IElementType LOCAL_TUPLE_DECLARATION = new ValaElementType("LOCAL_TUPLE_DECLARATION");
  IElementType LOCAL_VARIABLE = new ValaElementType("LOCAL_VARIABLE");
  IElementType LOCAL_VARIABLE_DECLARATION = new ValaElementType("LOCAL_VARIABLE_DECLARATION");
  IElementType LOCAL_VARIABLE_DECLARATIONS = new ValaElementType("LOCAL_VARIABLE_DECLARATIONS");
  IElementType LOCK_STATEMENT = new ValaElementType("LOCK_STATEMENT");
  IElementType MEMBER = new ValaElementType("MEMBER");
  IElementType MEMBER_ACCESS = new ValaElementType("MEMBER_ACCESS");
  IElementType MEMBER_DECLARATION_MODIFIER = new ValaElementType("MEMBER_DECLARATION_MODIFIER");
  IElementType MEMBER_DECLARATION_MODIFIERS = new ValaElementType("MEMBER_DECLARATION_MODIFIERS");
  IElementType MEMBER_INITIALIZER = new ValaElementType("MEMBER_INITIALIZER");
  IElementType METHOD_CALL = new ValaElementType("METHOD_CALL");
  IElementType METHOD_DECLARATION = new ValaElementType("METHOD_DECLARATION");
  IElementType MULTIPLICATIVE_EXPRESSION = new ValaElementType("MULTIPLICATIVE_EXPRESSION");
  IElementType NAMESPACE_DECLARATION = new ValaElementType("NAMESPACE_DECLARATION");
  IElementType NAMESPACE_MEMBER = new ValaElementType("NAMESPACE_MEMBER");
  IElementType OBJECT_CREATION_EXPRESSION = new ValaElementType("OBJECT_CREATION_EXPRESSION");
  IElementType OBJECT_INITIALIZER = new ValaElementType("OBJECT_INITIALIZER");
  IElementType OBJECT_OR_ARRAY_CREATION_EXPRESSION = new ValaElementType("OBJECT_OR_ARRAY_CREATION_EXPRESSION");
  IElementType PARAMETER = new ValaElementType("PARAMETER");
  IElementType PARAMETERS = new ValaElementType("PARAMETERS");
  IElementType PARAMETERS_DECL = new ValaElementType("PARAMETERS_DECL");
  IElementType POINTER_MEMBER_ACCESS = new ValaElementType("POINTER_MEMBER_ACCESS");
  IElementType POST_DECREMENT_EXPRESSION = new ValaElementType("POST_DECREMENT_EXPRESSION");
  IElementType POST_INCREMENT_EXPRESSION = new ValaElementType("POST_INCREMENT_EXPRESSION");
  IElementType PRIMARY_EXPRESSION = new ValaElementType("PRIMARY_EXPRESSION");
  IElementType PROPERTY_ACCESSOR = new ValaElementType("PROPERTY_ACCESSOR");
  IElementType PROPERTY_DECLARATION = new ValaElementType("PROPERTY_DECLARATION");
  IElementType PROPERTY_DECLARATION_MODIFIER = new ValaElementType("PROPERTY_DECLARATION_MODIFIER");
  IElementType PROPERTY_DECLARATION_MODIFIERS = new ValaElementType("PROPERTY_DECLARATION_MODIFIERS");
  IElementType PROPERTY_DECLARATION_PART = new ValaElementType("PROPERTY_DECLARATION_PART");
  IElementType PROPERTY_GET_ACCESSOR = new ValaElementType("PROPERTY_GET_ACCESSOR");
  IElementType PROPERTY_SET_CONSTRUCT_ACCESSOR = new ValaElementType("PROPERTY_SET_CONSTRUCT_ACCESSOR");
  IElementType RELATIONAL_EXPRESSION = new ValaElementType("RELATIONAL_EXPRESSION");
  IElementType REQUIRES_DECL = new ValaElementType("REQUIRES_DECL");
  IElementType RETURN_STATEMENT = new ValaElementType("RETURN_STATEMENT");
  IElementType SHIFT_EXPRESSION = new ValaElementType("SHIFT_EXPRESSION");
  IElementType SIGNAL_DECLARATION = new ValaElementType("SIGNAL_DECLARATION");
  IElementType SIGNAL_DECLARATION_MODIFIER = new ValaElementType("SIGNAL_DECLARATION_MODIFIER");
  IElementType SIGNAL_DECLARATION_MODIFIERS = new ValaElementType("SIGNAL_DECLARATION_MODIFIERS");
  IElementType SIMPLE_NAME = new ValaElementType("SIMPLE_NAME");
  IElementType SIZEOF_EXPRESSION = new ValaElementType("SIZEOF_EXPRESSION");
  IElementType SLICE_ARRAY = new ValaElementType("SLICE_ARRAY");
  IElementType STATEMENT = new ValaElementType("STATEMENT");
  IElementType STATEMENT_EXPRESSION = new ValaElementType("STATEMENT_EXPRESSION");
  IElementType STRUCT_DECLARATION = new ValaElementType("STRUCT_DECLARATION");
  IElementType STRUCT_MEMBER = new ValaElementType("STRUCT_MEMBER");
  IElementType SWITCH_SECTION = new ValaElementType("SWITCH_SECTION");
  IElementType SWITCH_STATEMENT = new ValaElementType("SWITCH_STATEMENT");
  IElementType SYMBOL = new ValaElementType("SYMBOL");
  IElementType SYMBOL_PART = new ValaElementType("SYMBOL_PART");
  IElementType TEMPLATE = new ValaElementType("TEMPLATE");
  IElementType THIS_ACCESS = new ValaElementType("THIS_ACCESS");
  IElementType THROWS_PART = new ValaElementType("THROWS_PART");
  IElementType THROW_STATEMENT = new ValaElementType("THROW_STATEMENT");
  IElementType TRY_STATEMENT = new ValaElementType("TRY_STATEMENT");
  IElementType TUPLE = new ValaElementType("TUPLE");
  IElementType TYPE = new ValaElementType("TYPE");
  IElementType TYPEOF_EXPRESSION = new ValaElementType("TYPEOF_EXPRESSION");
  IElementType TYPE_ARGUMENTS = new ValaElementType("TYPE_ARGUMENTS");
  IElementType TYPE_DECLARATION_MODIFIER = new ValaElementType("TYPE_DECLARATION_MODIFIER");
  IElementType TYPE_DECLARATION_MODIFIERS = new ValaElementType("TYPE_DECLARATION_MODIFIERS");
  IElementType TYPE_PARAMETERS = new ValaElementType("TYPE_PARAMETERS");
  IElementType UNARY_EXPRESSION = new ValaElementType("UNARY_EXPRESSION");
  IElementType UNARY_OPERATOR = new ValaElementType("UNARY_OPERATOR");
  IElementType USING_DIRECTIVE = new ValaElementType("USING_DIRECTIVE");
  IElementType WHILE_STATEMENT = new ValaElementType("WHILE_STATEMENT");
  IElementType YIELD_EXPRESSION = new ValaElementType("YIELD_EXPRESSION");
  IElementType YIELD_STATEMENT = new ValaElementType("YIELD_STATEMENT");

  IElementType AND = new ValaTokenType("&");
  IElementType AND_ASSIGN = new ValaTokenType("&=");
  IElementType ASSIGNMENT = new ValaTokenType("=");
  IElementType BITWISE_NOT = new ValaTokenType("~");
  IElementType BLOCK_COMMENT = new ValaTokenType("BLOCK_COMMENT");
  IElementType BOOL_AND = new ValaTokenType("&&");
  IElementType BOOL_OR = new ValaTokenType("||");
  IElementType CHAR = new ValaTokenType("CHAR");
  IElementType COALESCE = new ValaTokenType("??");
  IElementType COLON = new ValaTokenType(":");
  IElementType COMMA = new ValaTokenType(",");
  IElementType DECREMENT = new ValaTokenType("--");
  IElementType DECR_ASSIGN = new ValaTokenType("-=");
  IElementType DIV = new ValaTokenType("/");
  IElementType DIV_ASSIGN = new ValaTokenType("/=");
  IElementType DOT = new ValaTokenType(".");
  IElementType ELIPSIS = new ValaTokenType("...");
  IElementType EQUAL = new ValaTokenType("==");
  IElementType GLOBAL_NS = new ValaTokenType("global::");
  IElementType GT = new ValaTokenType(">");
  IElementType GTEQ = new ValaTokenType(">=");
  IElementType HEX = new ValaTokenType("HEX");
  IElementType IDENTIFIER = new ValaTokenType("IDENTIFIER");
  IElementType INCREMENT = new ValaTokenType("++");
  IElementType INCR_ASSIGN = new ValaTokenType("+=");
  IElementType INTEGER = new ValaTokenType("INTEGER");
  IElementType KW_ABSTRACT = new ValaTokenType("abstract");
  IElementType KW_AS = new ValaTokenType("as");
  IElementType KW_ASYNC = new ValaTokenType("async");
  IElementType KW_BASE = new ValaTokenType("base");
  IElementType KW_BREAK = new ValaTokenType("break");
  IElementType KW_CASE = new ValaTokenType("case");
  IElementType KW_CATCH = new ValaTokenType("catch");
  IElementType KW_CLASS = new ValaTokenType("class");
  IElementType KW_CONST = new ValaTokenType("const");
  IElementType KW_CONSTRUCT = new ValaTokenType("construct");
  IElementType KW_CONTINUE = new ValaTokenType("continue");
  IElementType KW_DEFAULT = new ValaTokenType("default");
  IElementType KW_DELEGATE = new ValaTokenType("delegate");
  IElementType KW_DELETE = new ValaTokenType("delete");
  IElementType KW_DO = new ValaTokenType("do");
  IElementType KW_DYNAMIC = new ValaTokenType("dynamic");
  IElementType KW_ELSE = new ValaTokenType("else");
  IElementType KW_ENSURES = new ValaTokenType("ensures");
  IElementType KW_ENUM = new ValaTokenType("enum");
  IElementType KW_ERRORDOMAIN = new ValaTokenType("errordomain");
  IElementType KW_EXTERN = new ValaTokenType("extern");
  IElementType KW_FALSE = new ValaTokenType("false");
  IElementType KW_FINALLY = new ValaTokenType("finally");
  IElementType KW_FOR = new ValaTokenType("for");
  IElementType KW_FOREACH = new ValaTokenType("foreach");
  IElementType KW_GET = new ValaTokenType("get");
  IElementType KW_IF = new ValaTokenType("if");
  IElementType KW_IN = new ValaTokenType("in");
  IElementType KW_INLINE = new ValaTokenType("inline");
  IElementType KW_INTERFACE = new ValaTokenType("interface");
  IElementType KW_INTERNAL = new ValaTokenType("internal");
  IElementType KW_IS = new ValaTokenType("is");
  IElementType KW_LOCK = new ValaTokenType("lock");
  IElementType KW_NAMESPACE = new ValaTokenType("namespace");
  IElementType KW_NEW = new ValaTokenType("new");
  IElementType KW_NULL = new ValaTokenType("null");
  IElementType KW_OUT = new ValaTokenType("out");
  IElementType KW_OVERRIDE = new ValaTokenType("override");
  IElementType KW_OWNED = new ValaTokenType("owned");
  IElementType KW_PARAMS = new ValaTokenType("params");
  IElementType KW_PRIVATE = new ValaTokenType("private");
  IElementType KW_PROTECTED = new ValaTokenType("protected");
  IElementType KW_PUBLIC = new ValaTokenType("public");
  IElementType KW_REF = new ValaTokenType("ref");
  IElementType KW_REQUIRES = new ValaTokenType("requires");
  IElementType KW_RETURN = new ValaTokenType("return");
  IElementType KW_SET = new ValaTokenType("set");
  IElementType KW_SIGNAL = new ValaTokenType("signal");
  IElementType KW_SIZEOF = new ValaTokenType("sizeof");
  IElementType KW_STATIC = new ValaTokenType("static");
  IElementType KW_STRUCT = new ValaTokenType("struct");
  IElementType KW_SWITCH = new ValaTokenType("switch");
  IElementType KW_THIS = new ValaTokenType("this");
  IElementType KW_THROW = new ValaTokenType("throw");
  IElementType KW_THROWS = new ValaTokenType("throws");
  IElementType KW_TRUE = new ValaTokenType("true");
  IElementType KW_TRY = new ValaTokenType("try");
  IElementType KW_TYPEOF = new ValaTokenType("typeof");
  IElementType KW_UNOWNED = new ValaTokenType("KW_UNOWNED");
  IElementType KW_USING = new ValaTokenType("using");
  IElementType KW_VAR = new ValaTokenType("var");
  IElementType KW_VIRTUAL = new ValaTokenType("virtual");
  IElementType KW_VOID = new ValaTokenType("void");
  IElementType KW_WEAK = new ValaTokenType("KW_WEAK");
  IElementType KW_WHILE = new ValaTokenType("while");
  IElementType KW_YIELD = new ValaTokenType("yield");
  IElementType LAMBDA = new ValaTokenType("=>");
  IElementType LBRACKET = new ValaTokenType("[");
  IElementType LCURL = new ValaTokenType("{");
  IElementType LINE_COMMENT = new ValaTokenType("LINE_COMMENT");
  IElementType LPAREN = new ValaTokenType("(");
  IElementType LT = new ValaTokenType("<");
  IElementType LTEQ = new ValaTokenType("<=");
  IElementType MINUS = new ValaTokenType("-");
  IElementType MOD = new ValaTokenType("%");
  IElementType MOD_ASSIGN = new ValaTokenType("%=");
  IElementType MULTIPLY = new ValaTokenType("*");
  IElementType MUL_ASSIGN = new ValaTokenType("*=");
  IElementType NOT = new ValaTokenType("!");
  IElementType NOT_EQUAL = new ValaTokenType("!=");
  IElementType OR = new ValaTokenType("|");
  IElementType OR_ASSIGN = new ValaTokenType("|=");
  IElementType PLUS = new ValaTokenType("+");
  IElementType POINTER_ACCESS = new ValaTokenType("->");
  IElementType QUESTION = new ValaTokenType("?");
  IElementType QUOT = new ValaTokenType("\"");
  IElementType RBRACKET = new ValaTokenType("]");
  IElementType RCURL = new ValaTokenType("}");
  IElementType REAL = new ValaTokenType("REAL");
  IElementType REGEX_LITERAL = new ValaTokenType("REGEX_LITERAL");
  IElementType RPAREN = new ValaTokenType(")");
  IElementType SEMICOLON = new ValaTokenType(";");
  IElementType SHIFT_LEFT = new ValaTokenType("<<");
  IElementType SHIFT_RIGHT = new ValaTokenType(">>");
  IElementType SHL_ASSIGN = new ValaTokenType("<<=");
  IElementType SHR_ASSIGN = new ValaTokenType(">>=");
  IElementType STRING = new ValaTokenType("STRING");
  IElementType TEMPLATE_LITERAL = new ValaTokenType("TEMPLATE_LITERAL");
  IElementType TEMPLATE_START = new ValaTokenType("@\"");
  IElementType VERBATIM_LITERAL = new ValaTokenType("");
  IElementType XOR = new ValaTokenType("^");
  IElementType XOR_ASSIGN = new ValaTokenType("^=");

  class Factory {
    public static PsiElement createElement(ASTNode node) {
      IElementType type = node.getElementType();
       if (type == ACCESS_MODIFIER) {
        return new BnfAccessModifierImpl(node);
      }
      else if (type == ADDITIVE_EXPRESSION) {
        return new BnfAdditiveExpressionImpl(node);
      }
      else if (type == AND_EXPRESSION) {
        return new BnfAndExpressionImpl(node);
      }
      else if (type == ARGUMENT) {
        return new BnfArgumentImpl(node);
      }
      else if (type == ARGUMENTS) {
        return new BnfArgumentsImpl(node);
      }
      else if (type == ARRAY_CREATION_EXPRESSION) {
        return new BnfArrayCreationExpressionImpl(node);
      }
      else if (type == ARRAY_SIZE) {
        return new BnfArraySizeImpl(node);
      }
      else if (type == ARRAY_TYPE) {
        return new BnfArrayTypeImpl(node);
      }
      else if (type == ASSIGNMENT_OPERATOR) {
        return new BnfAssignmentOperatorImpl(node);
      }
      else if (type == ATTRIBUTE) {
        return new BnfAttributeImpl(node);
      }
      else if (type == ATTRIBUTES) {
        return new BnfAttributesImpl(node);
      }
      else if (type == ATTRIBUTE_ARGUMENT) {
        return new BnfAttributeArgumentImpl(node);
      }
      else if (type == ATTRIBUTE_ARGUMENTS) {
        return new BnfAttributeArgumentsImpl(node);
      }
      else if (type == BASE_ACCESS) {
        return new BnfBaseAccessImpl(node);
      }
      else if (type == BASE_TYPES) {
        return new BnfBaseTypesImpl(node);
      }
      else if (type == BLOCK) {
        return new BnfBlockImpl(node);
      }
      else if (type == BREAK_STATEMENT) {
        return new BnfBreakStatementImpl(node);
      }
      else if (type == CAST_OPERATOR) {
        return new BnfCastOperatorImpl(node);
      }
      else if (type == CATCH_CLAUSE) {
        return new BnfCatchClauseImpl(node);
      }
      else if (type == CLASS_DECLARATION) {
        return new BnfClassDeclarationImpl(node);
      }
      else if (type == CLASS_MEMBER) {
        return new BnfClassMemberImpl(node);
      }
      else if (type == COALESCING_EXPRESSION) {
        return new BnfCoalescingExpressionImpl(node);
      }
      else if (type == CONDITIONAL_AND_EXPRESSION) {
        return new BnfConditionalAndExpressionImpl(node);
      }
      else if (type == CONDITIONAL_EXPRESSION) {
        return new BnfConditionalExpressionImpl(node);
      }
      else if (type == CONDITIONAL_OR_EXPRESSION) {
        return new BnfConditionalOrExpressionImpl(node);
      }
      else if (type == CONSTANT_DECLARATION) {
        return new BnfConstantDeclarationImpl(node);
      }
      else if (type == CONSTRUCTOR_DECLARATION) {
        return new BnfConstructorDeclarationImpl(node);
      }
      else if (type == CONSTRUCTOR_DECLARATION_MODIFIER) {
        return new BnfConstructorDeclarationModifierImpl(node);
      }
      else if (type == CONSTRUCTOR_DECLARATION_MODIFIERS) {
        return new BnfConstructorDeclarationModifiersImpl(node);
      }
      else if (type == CONTINUE_STATEMENT) {
        return new BnfContinueStatementImpl(node);
      }
      else if (type == CREATION_METHOD_DECLARATION) {
        return new BnfCreationMethodDeclarationImpl(node);
      }
      else if (type == DELEGATE_DECLARATION) {
        return new BnfDelegateDeclarationImpl(node);
      }
      else if (type == DELEGATE_DECLARATION_MODIFIER) {
        return new BnfDelegateDeclarationModifierImpl(node);
      }
      else if (type == DELEGATE_DECLARATION_MODIFIERS) {
        return new BnfDelegateDeclarationModifiersImpl(node);
      }
      else if (type == DELETE_STATEMENT) {
        return new BnfDeleteStatementImpl(node);
      }
      else if (type == DESTRUCTOR_DECLARATION) {
        return new BnfDestructorDeclarationImpl(node);
      }
      else if (type == DO_STATEMENT) {
        return new BnfDoStatementImpl(node);
      }
      else if (type == ELEMENT_ACCESS) {
        return new BnfElementAccessImpl(node);
      }
      else if (type == EMBEDDED_STATEMENT) {
        return new BnfEmbeddedStatementImpl(node);
      }
      else if (type == EMBEDDED_STATEMENT_WITHOUT_BLOCK) {
        return new BnfEmbeddedStatementWithoutBlockImpl(node);
      }
      else if (type == ENSURES_DECL) {
        return new BnfEnsuresDeclImpl(node);
      }
      else if (type == ENUM_DECLARATION) {
        return new BnfEnumDeclarationImpl(node);
      }
      else if (type == ENUM_MEMBER) {
        return new BnfEnumMemberImpl(node);
      }
      else if (type == ENUM_VALUE) {
        return new BnfEnumValueImpl(node);
      }
      else if (type == ENUM_VALUES) {
        return new BnfEnumValuesImpl(node);
      }
      else if (type == EQUALITY_EXPRESSION) {
        return new BnfEqualityExpressionImpl(node);
      }
      else if (type == ERRORCODE) {
        return new BnfErrorcodeImpl(node);
      }
      else if (type == ERRORCODES) {
        return new BnfErrorcodesImpl(node);
      }
      else if (type == ERRORDOMAIN_DECLARATION) {
        return new BnfErrordomainDeclarationImpl(node);
      }
      else if (type == EXCLUSIVE_OR_EXPRESSION) {
        return new BnfExclusiveOrExpressionImpl(node);
      }
      else if (type == EXPRESSION) {
        return new BnfExpressionImpl(node);
      }
      else if (type == EXPRESSION_STATEMENT) {
        return new BnfExpressionStatementImpl(node);
      }
      else if (type == FIELD_DECLARATION) {
        return new BnfFieldDeclarationImpl(node);
      }
      else if (type == FINALLY_CLAUSE) {
        return new BnfFinallyClauseImpl(node);
      }
      else if (type == FOREACH_STATEMENT) {
        return new BnfForeachStatementImpl(node);
      }
      else if (type == FOR_INITIALIZER) {
        return new BnfForInitializerImpl(node);
      }
      else if (type == FOR_ITERATOR) {
        return new BnfForIteratorImpl(node);
      }
      else if (type == FOR_STATEMENT) {
        return new BnfForStatementImpl(node);
      }
      else if (type == IF_STATEMENT) {
        return new BnfIfStatementImpl(node);
      }
      else if (type == INCLUSIVE_OR_EXPRESSION) {
        return new BnfInclusiveOrExpressionImpl(node);
      }
      else if (type == INITIALIZER) {
        return new BnfInitializerImpl(node);
      }
      else if (type == INLINE_ARRAY_TYPE) {
        return new BnfInlineArrayTypeImpl(node);
      }
      else if (type == INTERFACE_DECLARATION) {
        return new BnfInterfaceDeclarationImpl(node);
      }
      else if (type == INTERFACE_MEMBER) {
        return new BnfInterfaceMemberImpl(node);
      }
      else if (type == IN_EXPRESSION) {
        return new BnfInExpressionImpl(node);
      }
      else if (type == LAMBDA_EXPRESSION) {
        return new BnfLambdaExpressionImpl(node);
      }
      else if (type == LAMBDA_EXPRESSION_BODY) {
        return new BnfLambdaExpressionBodyImpl(node);
      }
      else if (type == LAMBDA_EXPRESSION_PARAMS) {
        return new BnfLambdaExpressionParamsImpl(node);
      }
      else if (type == LITERAL) {
        return new BnfLiteralImpl(node);
      }
      else if (type == LOCAL_TUPLE_DECLARATION) {
        return new BnfLocalTupleDeclarationImpl(node);
      }
      else if (type == LOCAL_VARIABLE) {
        return new BnfLocalVariableImpl(node);
      }
      else if (type == LOCAL_VARIABLE_DECLARATION) {
        return new BnfLocalVariableDeclarationImpl(node);
      }
      else if (type == LOCAL_VARIABLE_DECLARATIONS) {
        return new BnfLocalVariableDeclarationsImpl(node);
      }
      else if (type == LOCK_STATEMENT) {
        return new BnfLockStatementImpl(node);
      }
      else if (type == MEMBER) {
        return new BnfMemberImpl(node);
      }
      else if (type == MEMBER_ACCESS) {
        return new BnfMemberAccessImpl(node);
      }
      else if (type == MEMBER_DECLARATION_MODIFIER) {
        return new BnfMemberDeclarationModifierImpl(node);
      }
      else if (type == MEMBER_DECLARATION_MODIFIERS) {
        return new BnfMemberDeclarationModifiersImpl(node);
      }
      else if (type == MEMBER_INITIALIZER) {
        return new BnfMemberInitializerImpl(node);
      }
      else if (type == METHOD_CALL) {
        return new BnfMethodCallImpl(node);
      }
      else if (type == METHOD_DECLARATION) {
        return new BnfMethodDeclarationImpl(node);
      }
      else if (type == MULTIPLICATIVE_EXPRESSION) {
        return new BnfMultiplicativeExpressionImpl(node);
      }
      else if (type == NAMESPACE_DECLARATION) {
        return new BnfNamespaceDeclarationImpl(node);
      }
      else if (type == NAMESPACE_MEMBER) {
        return new BnfNamespaceMemberImpl(node);
      }
      else if (type == OBJECT_CREATION_EXPRESSION) {
        return new BnfObjectCreationExpressionImpl(node);
      }
      else if (type == OBJECT_INITIALIZER) {
        return new BnfObjectInitializerImpl(node);
      }
      else if (type == OBJECT_OR_ARRAY_CREATION_EXPRESSION) {
        return new BnfObjectOrArrayCreationExpressionImpl(node);
      }
      else if (type == PARAMETER) {
        return new BnfParameterImpl(node);
      }
      else if (type == PARAMETERS) {
        return new BnfParametersImpl(node);
      }
      else if (type == PARAMETERS_DECL) {
        return new BnfParametersDeclImpl(node);
      }
      else if (type == POINTER_MEMBER_ACCESS) {
        return new BnfPointerMemberAccessImpl(node);
      }
      else if (type == POST_DECREMENT_EXPRESSION) {
        return new BnfPostDecrementExpressionImpl(node);
      }
      else if (type == POST_INCREMENT_EXPRESSION) {
        return new BnfPostIncrementExpressionImpl(node);
      }
      else if (type == PRIMARY_EXPRESSION) {
        return new BnfPrimaryExpressionImpl(node);
      }
      else if (type == PROPERTY_ACCESSOR) {
        return new BnfPropertyAccessorImpl(node);
      }
      else if (type == PROPERTY_DECLARATION) {
        return new BnfPropertyDeclarationImpl(node);
      }
      else if (type == PROPERTY_DECLARATION_MODIFIER) {
        return new BnfPropertyDeclarationModifierImpl(node);
      }
      else if (type == PROPERTY_DECLARATION_MODIFIERS) {
        return new BnfPropertyDeclarationModifiersImpl(node);
      }
      else if (type == PROPERTY_DECLARATION_PART) {
        return new BnfPropertyDeclarationPartImpl(node);
      }
      else if (type == PROPERTY_GET_ACCESSOR) {
        return new BnfPropertyGetAccessorImpl(node);
      }
      else if (type == PROPERTY_SET_CONSTRUCT_ACCESSOR) {
        return new BnfPropertySetConstructAccessorImpl(node);
      }
      else if (type == RELATIONAL_EXPRESSION) {
        return new BnfRelationalExpressionImpl(node);
      }
      else if (type == REQUIRES_DECL) {
        return new BnfRequiresDeclImpl(node);
      }
      else if (type == RETURN_STATEMENT) {
        return new BnfReturnStatementImpl(node);
      }
      else if (type == SHIFT_EXPRESSION) {
        return new BnfShiftExpressionImpl(node);
      }
      else if (type == SIGNAL_DECLARATION) {
        return new BnfSignalDeclarationImpl(node);
      }
      else if (type == SIGNAL_DECLARATION_MODIFIER) {
        return new BnfSignalDeclarationModifierImpl(node);
      }
      else if (type == SIGNAL_DECLARATION_MODIFIERS) {
        return new BnfSignalDeclarationModifiersImpl(node);
      }
      else if (type == SIMPLE_NAME) {
        return new BnfSimpleNameImpl(node);
      }
      else if (type == SIZEOF_EXPRESSION) {
        return new BnfSizeofExpressionImpl(node);
      }
      else if (type == SLICE_ARRAY) {
        return new BnfSliceArrayImpl(node);
      }
      else if (type == STATEMENT) {
        return new BnfStatementImpl(node);
      }
      else if (type == STATEMENT_EXPRESSION) {
        return new BnfStatementExpressionImpl(node);
      }
      else if (type == STRUCT_DECLARATION) {
        return new BnfStructDeclarationImpl(node);
      }
      else if (type == STRUCT_MEMBER) {
        return new BnfStructMemberImpl(node);
      }
      else if (type == SWITCH_SECTION) {
        return new BnfSwitchSectionImpl(node);
      }
      else if (type == SWITCH_STATEMENT) {
        return new BnfSwitchStatementImpl(node);
      }
      else if (type == SYMBOL) {
        return new BnfSymbolImpl(node);
      }
      else if (type == SYMBOL_PART) {
        return new BnfSymbolPartImpl(node);
      }
      else if (type == TEMPLATE) {
        return new BnfTemplateImpl(node);
      }
      else if (type == THIS_ACCESS) {
        return new BnfThisAccessImpl(node);
      }
      else if (type == THROWS_PART) {
        return new BnfThrowsPartImpl(node);
      }
      else if (type == THROW_STATEMENT) {
        return new BnfThrowStatementImpl(node);
      }
      else if (type == TRY_STATEMENT) {
        return new BnfTryStatementImpl(node);
      }
      else if (type == TUPLE) {
        return new BnfTupleImpl(node);
      }
      else if (type == TYPE) {
        return new BnfTypeImpl(node);
      }
      else if (type == TYPEOF_EXPRESSION) {
        return new BnfTypeofExpressionImpl(node);
      }
      else if (type == TYPE_ARGUMENTS) {
        return new BnfTypeArgumentsImpl(node);
      }
      else if (type == TYPE_DECLARATION_MODIFIER) {
        return new BnfTypeDeclarationModifierImpl(node);
      }
      else if (type == TYPE_DECLARATION_MODIFIERS) {
        return new BnfTypeDeclarationModifiersImpl(node);
      }
      else if (type == TYPE_PARAMETERS) {
        return new BnfTypeParametersImpl(node);
      }
      else if (type == UNARY_EXPRESSION) {
        return new BnfUnaryExpressionImpl(node);
      }
      else if (type == UNARY_OPERATOR) {
        return new BnfUnaryOperatorImpl(node);
      }
      else if (type == USING_DIRECTIVE) {
        return new BnfUsingDirectiveImpl(node);
      }
      else if (type == WHILE_STATEMENT) {
        return new BnfWhileStatementImpl(node);
      }
      else if (type == YIELD_EXPRESSION) {
        return new BnfYieldExpressionImpl(node);
      }
      else if (type == YIELD_STATEMENT) {
        return new BnfYieldStatementImpl(node);
      }
      throw new AssertionError("Unknown element type: " + type);
    }
  }
}
