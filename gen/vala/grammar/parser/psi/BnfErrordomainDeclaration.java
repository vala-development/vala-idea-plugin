// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface BnfErrordomainDeclaration extends ValaCompositeElement {

  @Nullable
  BnfAccessModifier getAccessModifier();

  @NotNull
  BnfErrorcodes getErrorcodes();

  @NotNull
  List<BnfMethodDeclaration> getMethodDeclarationList();

  @NotNull
  BnfSymbol getSymbol();

  @Nullable
  BnfTypeDeclarationModifiers getTypeDeclarationModifiers();

}
