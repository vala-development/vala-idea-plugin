// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface BnfSignalDeclaration extends ValaCompositeElement {

  @Nullable
  BnfAccessModifier getAccessModifier();

  @Nullable
  BnfBlock getBlock();

  @NotNull
  BnfParameters getParameters();

  @Nullable
  BnfSignalDeclarationModifiers getSignalDeclarationModifiers();

  @NotNull
  BnfType getType();

  @NotNull
  PsiElement getIdentifier();

}
