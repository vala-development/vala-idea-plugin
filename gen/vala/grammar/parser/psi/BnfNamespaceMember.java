// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface BnfNamespaceMember extends ValaCompositeElement {

  @Nullable
  BnfAttributes getAttributes();

  @Nullable
  BnfClassDeclaration getClassDeclaration();

  @Nullable
  BnfConstantDeclaration getConstantDeclaration();

  @Nullable
  BnfDelegateDeclaration getDelegateDeclaration();

  @Nullable
  BnfEnumDeclaration getEnumDeclaration();

  @Nullable
  BnfErrordomainDeclaration getErrordomainDeclaration();

  @Nullable
  BnfFieldDeclaration getFieldDeclaration();

  @Nullable
  BnfInterfaceDeclaration getInterfaceDeclaration();

  @Nullable
  BnfMethodDeclaration getMethodDeclaration();

  @Nullable
  BnfNamespaceDeclaration getNamespaceDeclaration();

  @Nullable
  BnfStructDeclaration getStructDeclaration();

}
