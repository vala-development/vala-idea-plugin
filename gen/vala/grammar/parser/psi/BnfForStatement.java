// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface BnfForStatement extends ValaCompositeElement {

  @NotNull
  BnfEmbeddedStatement getEmbeddedStatement();

  @Nullable
  BnfExpression getExpression();

  @Nullable
  BnfForInitializer getForInitializer();

  @Nullable
  BnfForIterator getForIterator();

}
