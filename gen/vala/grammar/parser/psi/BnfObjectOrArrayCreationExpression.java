// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface BnfObjectOrArrayCreationExpression extends ValaCompositeElement {

  @Nullable
  BnfArrayCreationExpression getArrayCreationExpression();

  @NotNull
  BnfMember getMember();

  @Nullable
  BnfObjectCreationExpression getObjectCreationExpression();

}
