// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface BnfDelegateDeclaration extends ValaCompositeElement {

  @Nullable
  BnfAccessModifier getAccessModifier();

  @Nullable
  BnfDelegateDeclarationModifiers getDelegateDeclarationModifiers();

  @NotNull
  BnfParameters getParameters();

  @NotNull
  BnfSymbol getSymbol();

  @Nullable
  BnfThrowsPart getThrowsPart();

  @NotNull
  BnfType getType();

  @Nullable
  BnfTypeParameters getTypeParameters();

}
