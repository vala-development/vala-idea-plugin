// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface BnfEmbeddedStatementWithoutBlock extends ValaCompositeElement {

  @Nullable
  BnfBreakStatement getBreakStatement();

  @Nullable
  BnfContinueStatement getContinueStatement();

  @Nullable
  BnfDeleteStatement getDeleteStatement();

  @Nullable
  BnfDoStatement getDoStatement();

  @Nullable
  BnfExpressionStatement getExpressionStatement();

  @Nullable
  BnfForStatement getForStatement();

  @Nullable
  BnfForeachStatement getForeachStatement();

  @Nullable
  BnfIfStatement getIfStatement();

  @Nullable
  BnfLockStatement getLockStatement();

  @Nullable
  BnfReturnStatement getReturnStatement();

  @Nullable
  BnfSwitchStatement getSwitchStatement();

  @Nullable
  BnfThrowStatement getThrowStatement();

  @Nullable
  BnfTryStatement getTryStatement();

  @Nullable
  BnfWhileStatement getWhileStatement();

  @Nullable
  BnfYieldStatement getYieldStatement();

}
