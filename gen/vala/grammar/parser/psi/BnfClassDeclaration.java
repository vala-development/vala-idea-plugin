// This is a generated file. Not intended for manual editing.
package vala.grammar.parser.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface BnfClassDeclaration extends ValaCompositeElement {

  @Nullable
  BnfAccessModifier getAccessModifier();

  @Nullable
  BnfBaseTypes getBaseTypes();

  @NotNull
  List<BnfClassMember> getClassMemberList();

  @NotNull
  BnfSymbol getSymbol();

  @Nullable
  BnfTypeArguments getTypeArguments();

  @Nullable
  BnfTypeDeclarationModifiers getTypeDeclarationModifiers();

}
