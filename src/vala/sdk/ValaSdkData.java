package vala.sdk;

import com.intellij.openapi.projectRoots.SdkAdditionalData;

import java.util.ArrayList;

public class ValaSdkData implements SdkAdditionalData {

    public ArrayList<ValaSdkPlatformData> platforms = new ArrayList<>();

    public String SDK_PATH = "";
    public String VALA_BIN_PATH = "";
    public String VALA_VERSION = "";

    public ValaSdkData() {
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
