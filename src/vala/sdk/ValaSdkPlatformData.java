package vala.sdk;

public class ValaSdkPlatformData {
    public String PLATFORM = "";
    public String ARCH = "";
    public String GCC_BIN_PATH = "";
    public String GCC_VERSION = "";
}
