package vala.sdk;

import com.intellij.openapi.projectRoots.*;
import com.intellij.util.xmlb.XmlSerializer;
import org.jdom.Element;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import vala.ValaFileType;
import vala.ValaIcons;
import vala.compiler.ValaCommandLineUtils;

import javax.swing.*;

public class ValaSdkType extends SdkType {
    private ValaSdkData _sdkData;

    public static ValaSdkType getInstance() {
        return SdkType.findInstance(ValaSdkType.class);
    }

    public ValaSdkType() {
        super("ValaSdk");
    }

    @Nullable
    @Override
    public String suggestHomePath() {
        return null;
    }

    @Override
    public boolean isValidSdkHome(String path) {
        ValaSdkData data = ValaCommandLineUtils.getSdkData(path);
        if (data != null) {
            return true;
        }

        return false;
    }

    @Override
    public String suggestSdkName(String currentSdkName, String sdkHome) {
        return "Vala SDK";
    }

    @Override
    public String getVersionString(@NotNull Sdk sdk) {
        return getVersionString(sdk.getHomePath());
    }

    @Nullable
    @Override
    public String getVersionString(String sdkHome) {
        ValaSdkData data = ValaCommandLineUtils.getSdkData(sdkHome);
        if (data != null) {
            return data.VALA_VERSION;
        }
        return "";
    }

    @Override
    public String getPresentableName() {
        return "Vala SDK";
    }

    @Nullable
    @Override
    public AdditionalDataConfigurable createAdditionalDataConfigurable(SdkModel sdkModel, SdkModificator sdkModificator) {
        return null;
    }

    @Override
    public void setupSdkPaths(@NotNull Sdk sdk) {
    }

    @Override
    public SdkAdditionalData loadAdditionalData(Element additional) {
        return null;
    }

    @Override
    public void saveAdditionalData(@NotNull SdkAdditionalData additionalData, @NotNull Element additional) {
    }

    @Override
    public Icon getIcon() {
        return ValaIcons.VALA_ICON_16;
    }

    @Override
    public Icon getIconForAddAction() {
        return getIcon();
    }
}
