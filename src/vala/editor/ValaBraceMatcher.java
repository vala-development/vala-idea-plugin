package vala.editor;

import com.intellij.lang.BracePair;
import com.intellij.lang.PairedBraceMatcher;
import com.intellij.psi.PsiFile;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import vala.grammar.parser.psi.ValaTypes;

public class ValaBraceMatcher implements PairedBraceMatcher {
    private static BracePair[] ourBracePairs =
            {
                    new BracePair(ValaTypes.LCURL, ValaTypes.RCURL, true),
                    new BracePair(ValaTypes.LBRACKET, ValaTypes.RBRACKET, false),
                    new BracePair(ValaTypes.LPAREN, ValaTypes.RPAREN, false)
            };

    @Override
    public BracePair[] getPairs() {
        return ourBracePairs;
    }

    @Override
    public boolean isPairedBracesAllowedBeforeType(@NotNull IElementType lbraceType, @Nullable IElementType contextType) {
        return true;
    }

    @Override
    public int getCodeConstructStart(PsiFile file, int openingBraceOffset) {
        return openingBraceOffset;
    }
}
