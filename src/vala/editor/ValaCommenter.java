package vala.editor;

import com.intellij.lang.CodeDocumentationAwareCommenter;
import com.intellij.psi.PsiComment;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.Nullable;
import vala.grammar.parser.psi.ValaTypes;

public class ValaCommenter implements CodeDocumentationAwareCommenter {
    public String getLineCommentPrefix() {
        return "//";
    }

    public String getBlockCommentPrefix() {
        return "/*";
    }

    public String getBlockCommentSuffix() {
        return "*/";
    }

    public String getCommentedBlockCommentPrefix() {
        return null;
    }

    public String getCommentedBlockCommentSuffix() {
        return null;
    }

    @Nullable
    public IElementType getLineCommentTokenType() {
        return ValaTypes.LINE_COMMENT;
    }

    @Nullable
    public IElementType getBlockCommentTokenType() {
        return ValaTypes.BLOCK_COMMENT;
    }

    public String getDocumentationCommentPrefix() {
        return "/**";
    }

    public String getDocumentationCommentLinePrefix() {
        return "*";
    }

    public String getDocumentationCommentSuffix() {
        return "*/";
    }

    public boolean isDocumentationComment(final PsiComment element) {
        return false;

        // TODO: add doc comments support
        /*return element.getTokenType() == ValaTypes.LINE_DOC_COMMENT ||
                element.getTokenType() == ValaTypes.BLOCK_DOC_COMMENT;*/
    }

    @Nullable
    public IElementType getDocumentationCommentTokenType() {
        return null;

        // TODO: add doc comments support
        //return ValaTypes.LINE_DOC_COMMENT;
    }
}
