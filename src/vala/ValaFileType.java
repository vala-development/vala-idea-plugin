package vala;

import com.intellij.openapi.fileTypes.LanguageFileType;
import com.intellij.openapi.util.IconLoader;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class ValaFileType extends LanguageFileType {
    public static final ValaFileType INSTANCE = new ValaFileType();

    private ValaFileType() {
        super(ValaLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public String getName() {
        return "Vala file";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Vala language file";
    }

    @NotNull
    @Override
    public String getDefaultExtension() {
        return "vala";
    }

    @Nullable
    @Override
    public Icon getIcon() {
        return ValaIcons.VALA_ICON_16;
    }
}
