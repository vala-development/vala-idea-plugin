package vala.module;

import com.intellij.openapi.module.ModuleType;
import com.intellij.openapi.module.ModuleTypeManager;
import org.jetbrains.annotations.NotNull;
import vala.ValaIcons;

import javax.swing.*;

public class ValaModuleType extends ModuleType<ValaModuleBuilder> {
    public static final String MODULE_TYPE_ID = "VALA_MODULE";

    public static ModuleType getInstance() {
        return (ValaModuleType) ModuleTypeManager.getInstance().findByID(MODULE_TYPE_ID);
    }

    public ValaModuleType() {
        super(MODULE_TYPE_ID);
    }

    @NotNull
    @Override
    public ValaModuleBuilder createModuleBuilder() {
        return new ValaModuleBuilder();
    }

    @NotNull
    @Override
    public String getName() {
        return "Vala Module";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "A module supporting Vala development.";
    }

    @Override
    public Icon getBigIcon() {
        return ValaIcons.VALA_ICON_24;
    }

    @Override
    public Icon getNodeIcon(@Deprecated boolean b) {
        return ValaIcons.VALA_ICON_16;
    }
}
