package vala.module;

import com.intellij.ide.util.projectWizard.JavaModuleBuilder;
import com.intellij.openapi.module.ModuleType;
import com.intellij.openapi.projectRoots.SdkTypeId;
import vala.sdk.ValaSdkType;

public class ValaModuleBuilder extends JavaModuleBuilder {
    public ValaModuleBuilder() {
    }

    @Override
    public ModuleType getModuleType() {
        return ValaModuleType.getInstance();
    }

    @Override
    public boolean isSuitableSdkType(SdkTypeId sdkTypeId) {
        return sdkTypeId instanceof ValaSdkType;
    }
}
