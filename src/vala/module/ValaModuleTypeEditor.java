package vala.module;

import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleConfigurationEditor;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.roots.ui.configuration.ModuleConfigurationState;
import com.intellij.openapi.ui.ComboBox;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.*;

public class ValaModuleTypeEditor implements ModuleConfigurationEditor {
    private final Module module;
    private final ValaModuleComponent component;

    private ComboBox comboModuleType;

    public ValaModuleTypeEditor(ModuleConfigurationState state) {
        module = state.getRootModel().getModule();
        component = ValaModuleComponent.getInstance(module);
    }

    @Nls
    @Override
    public String getDisplayName() {
        return "Module Type";
    }

    @Nullable
    @Override
    public String getHelpTopic() {
        return null;
    }

    @Nullable
    @Override
    public JComponent createComponent() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        panel.add(new JLabel("Module type:"));

        comboModuleType = new ComboBox(new String[] { "Application", "Console application", "Library" });
        comboModuleType.setSelectedIndex(moduleTypeToComboIndex(component.getModuleType()));
        panel.add(comboModuleType);

        return panel;
    }

    @Override
    public boolean isModified() {
        return !component.getModuleType().equals(comboIndexToModuleType(comboModuleType.getSelectedIndex()));
    }

    @Override
    public void apply() throws ConfigurationException {
        component.setModuleType(comboIndexToModuleType(comboModuleType.getSelectedIndex()));
    }

    @Override
    public void reset() {
        comboModuleType.setSelectedIndex(moduleTypeToComboIndex(component.getModuleType()));
    }

    @Override
    public void disposeUIResources() {

    }

    @Override
    public void saveData() {

    }

    @Override
    public void moduleStateChanged() {

    }

    private int moduleTypeToComboIndex(ValaModuleComponent.ModuleType type) {
        switch (type) {
            case APPLICATION: return 0;
            case CONSOLE_APPLICATION: return 1;
            case LIBRARY: return 2;
            default: return 0;
        }
    }

    private ValaModuleComponent.ModuleType comboIndexToModuleType(int index) {
        switch (index) {
            case 0: return ValaModuleComponent.ModuleType.APPLICATION;
            case 1: return ValaModuleComponent.ModuleType.CONSOLE_APPLICATION;
            case 2: return ValaModuleComponent.ModuleType.LIBRARY;
            default: return ValaModuleComponent.ModuleType.APPLICATION;
        }
    }
}
