package vala.module;

import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleComponent;
import com.intellij.openapi.util.InvalidDataException;
import com.intellij.openapi.util.JDOMExternalizable;
import com.intellij.openapi.util.WriteExternalException;
import org.jdom.Element;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ValaModuleComponent implements ModuleComponent, JDOMExternalizable {
    public enum ModuleType {
        APPLICATION,
        CONSOLE_APPLICATION,
        LIBRARY
    }

    private static final String MODULE_TYPE_ATTR = "module_type";

    private final Module module;

    private ModuleType moduleType = ModuleType.APPLICATION;

    @Nullable
    public static ValaModuleComponent getInstance(Module module) {
        return module.getComponent(ValaModuleComponent.class);
    }

    public ValaModuleComponent(Module module) {
        this.module = module;
    }

    public void initComponent() {
    }

    public void disposeComponent() {
    }

    @NotNull
    public String getComponentName() {
        return "ValaModuleComponent";
    }

    public void projectOpened() {
        // called when project is opened
    }

    public void projectClosed() {
        // called when project is being closed
    }

    public void moduleAdded() {
        // Invoked when the module corresponding to this component instance has been completely
        // loaded and added to the project.
    }

    public void readExternal(Element element) throws InvalidDataException {
        moduleType = ModuleType.valueOf(element.getAttributeValue(MODULE_TYPE_ATTR));
    }

    public void writeExternal(Element element) throws WriteExternalException {
        element.setAttribute(MODULE_TYPE_ATTR, moduleType.name());
    }

    public ModuleType getModuleType() {
        return moduleType;
    }

    public void setModuleType(ModuleType moduleType) {
        this.moduleType = moduleType;
    }
}
