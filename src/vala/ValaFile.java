package vala;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import org.jetbrains.annotations.NotNull;

public class ValaFile extends PsiFileBase {
    public ValaFile(final FileViewProvider fileViewProvider) {
        super(fileViewProvider, ValaLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public FileType getFileType() {
        return ValaFileType.INSTANCE;
    }

    @Override
    public String toString() {
        return "ValaFile:" + getName();
    }
}
