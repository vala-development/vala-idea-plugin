package vala.compiler;

import com.intellij.openapi.compiler.CompileContext;
import com.intellij.openapi.compiler.CompileScope;
import com.intellij.openapi.compiler.CompileTask;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleType;
import vala.module.ValaModuleType;

import java.util.ArrayList;
import java.util.List;

public class ValaCompileTask implements CompileTask {

    @Override
    public boolean execute(CompileContext context) {
        List<Module> modules = getModulesToCompile(context.getCompileScope());
        for (Module module : modules) {
            if (!ValaMakeModuleUtils.makeModule(context, module)) {
                return false;
            }
        }

        return true;
    }

    private static List<Module> getModulesToCompile(CompileScope scope) {
        final List<Module> result = new ArrayList<Module>();
        for (final Module module : scope.getAffectedModules()) {
            if (ModuleType.get(module) != ValaModuleType.getInstance()) continue;
            result.add(module);
        }
        return result;
    }
}
