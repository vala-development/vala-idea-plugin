package vala.compiler;

import com.intellij.compiler.options.CompileStepBeforeRun;
import com.intellij.execution.RunManager;
import com.intellij.execution.RunnerAndConfigurationSettings;
import com.intellij.execution.configurations.RunConfiguration;
import com.intellij.openapi.actionSystem.ActionManager;
import com.intellij.openapi.compiler.CompileContext;
import com.intellij.openapi.compiler.CompilerMessageCategory;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.projectRoots.Sdk;
import com.intellij.openapi.roots.CompilerModuleExtension;
import com.intellij.openapi.roots.ModuleRootManager;
import vala.configuration.ValaRunConfiguration;
import vala.module.ValaModuleComponent;
import vala.sdk.ValaSdkData;

import java.io.File;

public class ValaMakeModuleUtils {
    public static boolean makeModule(CompileContext context, Module module) {
        ValaModuleComponent moduleComponent = ValaModuleComponent.getInstance(module);
        ValaModuleComponent.ModuleType moduleType = moduleComponent.getModuleType();

        ModuleRootManager moduleRootManager = ModuleRootManager.getInstance(module);
        Sdk sdk = moduleRootManager.getSdk();
        if (sdk == null) {
            context.addMessage(CompilerMessageCategory.ERROR, "Vala SDK must be selected.", null, -1, -1);
            return false;
        }

        RunnerAndConfigurationSettings selectedConfiguration = RunManager.getInstance(module.getProject()).getSelectedConfiguration();
        if (selectedConfiguration == null) {
            context.addMessage(CompilerMessageCategory.ERROR, "Vala run configuration must be selected.", null, -1, -1);
            return false;
        }
        RunConfiguration runConfiguration = selectedConfiguration.getConfiguration();
        if (runConfiguration == null) {
            context.addMessage(CompilerMessageCategory.ERROR, "Vala run configuration must be selected.", null, -1, -1);
            return false;
        }
        if (!(runConfiguration instanceof ValaRunConfiguration)) {
            context.addMessage(CompilerMessageCategory.ERROR, "Selected run configuration must be Vala run configuration.", null, -1, -1);
            return false;
        }
        ValaRunConfiguration valaRunConfiguration = (ValaRunConfiguration)runConfiguration;

        return makeModule(context, module, moduleType, module.getModuleFilePath(), sdk, valaRunConfiguration);
    }

    private static boolean makeModule(CompileContext context,
                                      Module module,
                                      ValaModuleComponent.ModuleType moduleType,
                                      String modulePath,
                                      Sdk sdk,
                                      ValaRunConfiguration runConfiguration)
    {
        String sourceBaseDir = getSourceBaseDirFromModulePath(modulePath);

        return true;
    }

    private static String getSourceBaseDirFromModulePath(String modulePath) {
        File moduleFile = new File(modulePath);
        File parentFile = new File(moduleFile.getParentFile(), "src");
        return parentFile.toString();
    }
}
