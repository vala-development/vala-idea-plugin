package vala.compiler;

import com.intellij.execution.configurations.GeneralCommandLine;
import com.intellij.execution.process.CapturingProcessHandler;
import com.intellij.execution.process.ProcessOutput;

import java.io.File;
import java.nio.charset.Charset;

public class CommandLineUtils {
    public static ProcessOutput runCommand(String workDir, String command, String params) throws Exception {
        GeneralCommandLine cmd = new GeneralCommandLine();
        cmd.setWorkDirectory(workDir);
        cmd.setExePath(new File(workDir, command).getAbsolutePath());
        cmd.addParameters(params);

        ProcessOutput output = new CapturingProcessHandler(cmd.createProcess(),
                Charset.defaultCharset(), cmd.getCommandLineString()).runProcess();

        return output;
    }
}
