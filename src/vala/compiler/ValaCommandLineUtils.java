package vala.compiler;

import com.intellij.execution.process.ProcessOutput;
import vala.sdk.ValaSdkData;
import vala.sdk.ValaSdkPlatformData;

import java.io.File;
import java.util.List;

public class ValaCommandLineUtils {
    public static ValaSdkData getSdkData(String folder) {
        final ValaSdkData sdkData = new ValaSdkData();
        File fileSDK = new File(folder);
        sdkData.SDK_PATH = fileSDK.getAbsolutePath();
        sdkData.VALA_BIN_PATH = new File(fileSDK, "common/vala/bin").getAbsolutePath();
        sdkData.VALA_VERSION = getValaVersion(sdkData.VALA_BIN_PATH);
        if (sdkData.VALA_VERSION == null) {
            return null;
        }

        for (File f : fileSDK.listFiles()) {
            ValaSdkPlatformData platformData = getPlatformData(f);
            if (platformData != null) {
                sdkData.platforms.add(platformData);
            }
        }

        return sdkData;
    }

    private static String getValaVersion(String valaBinPath) {
        try {
            ProcessOutput out = CommandLineUtils.runCommand(valaBinPath, "valac", "--version");
            List<String> linesOut = out.getStdoutLines();
            if (linesOut != null && linesOut.size() > 0) {
                String line = linesOut.get(0);
                if (line.startsWith("Vala ")) {
                    return line.substring("Vala ".length());
                }
            }
        } catch (Exception ex) {
            return null;
        }

        return null;
    }

    private static ValaSdkPlatformData getPlatformData(File path) {
        if (!path.isDirectory()) {
            return null;
        }

        String parts[] = path.getName().split("_");
        if (parts == null || parts.length != 2) {
            return null;
        }

        ValaSdkPlatformData data = new ValaSdkPlatformData();
        data.PLATFORM = parts[0];
        data.ARCH = parts[1];
        data.GCC_BIN_PATH = new File(path, "mingw/bin").getAbsolutePath();
        data.GCC_VERSION = getGccVersion(data.GCC_BIN_PATH);
        if (data.GCC_VERSION == null) {
            return null;
        }
        
        return data;
    }

    private static String getGccVersion(String binPath) {
        try {
            ProcessOutput out = CommandLineUtils.runCommand(binPath, "gcc", "--version");
            List<String> linesOut = out.getStdoutLines();
            if (linesOut != null && linesOut.size() > 0) {
                String line = linesOut.get(0);
                if (line.startsWith("gcc ")) {
                    return line.substring("gcc ".length());
                }
            }
        } catch (Exception ex) {
            return null;
        }

        return null;
    }
}
