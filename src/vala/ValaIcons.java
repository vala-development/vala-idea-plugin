package vala;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class ValaIcons {

    public static final Icon VALA_ICON_16 = IconLoader.getIcon("/vala/icons/vala-16.png");
    public static final Icon VALA_ICON_24 = IconLoader.getIcon("/vala/icons/vala-24.png");

}
