package vala.grammar.lexer;

import com.intellij.lexer.*;
import com.intellij.psi.tree.IElementType;
import static vala.grammar.parser.psi.ValaTypes.*;

%%

%{
  public ValaLexer() {
    this((java.io.Reader)null);
  }
%}

%public
%class ValaLexer
%implements FlexLexer
%function advance
%type IElementType
%unicode

EOL="\r"|"\n"|"\r\n"
LINE_WS=[\ \t\f]
WHITE_SPACE=({LINE_WS}|{EOL})+

COMMENT = {BLOCK_COMMENT} | {LINE_COMMENT}
BLOCK_COMMENT = "/*" [^*] ~"*/" | "/*" "*"+ "/"
LINE_COMMENT = "//" [^\r\n]* {EOL}?

HEX=0x[0-9a-fA-F]+
IDENTIFIER=(@[a-zA-Z0-9_]+)|([a-zA-Z_][a-zA-Z0-9_]*)
INTEGER=[0-9]+
REAL=([0-9]+)(\.[0-9]?)[dDfF]?
CHAR='[a-zA-Z0-9]'
//REGEX_LITERAL="/"[^/]*"/"[gmi]*
STRING=\"[^\"]*\"

%%
<YYINITIAL> {
  {WHITE_SPACE}         { return com.intellij.psi.TokenType.WHITE_SPACE; }

  {BLOCK_COMMENT}       { return BLOCK_COMMENT; }
  {LINE_COMMENT}        { return LINE_COMMENT; }

  {STRING}              { return STRING; }

  "."                   { return DOT; }
  ","                   { return COMMA; }
  ":"                   { return COLON; }
  ";"                   { return SEMICOLON; }
  "<<"                  { return SHIFT_LEFT; }
  ">>"                  { return SHIFT_RIGHT; }
  "["                   { return LBRACKET; }
  "]"                   { return RBRACKET; }
  "("                   { return LPAREN; }
  ")"                   { return RPAREN; }
  "{"                   { return LCURL; }
  "}"                   { return RCURL; }
  "="                   { return ASSIGNMENT; }
  "+="                  { return INCR_ASSIGN; }
  "-="                  { return DECR_ASSIGN; }
  "|="                  { return OR_ASSIGN; }
  "&="                  { return AND_ASSIGN; }
  "^="                  { return XOR_ASSIGN; }
  "/="                  { return DIV_ASSIGN; }
  "*="                  { return MUL_ASSIGN; }
  "%="                  { return MOD_ASSIGN; }
  "<<="                 { return SHL_ASSIGN; }
  ">>="                 { return SHR_ASSIGN; }
  ">="                  { return GTEQ; }
  ">"                   { return GT; }
  "<="                  { return LTEQ; }
  "<"                   { return LT; }
  "=="                  { return EQUAL; }
  "?"                   { return QUESTION; }
  "??"                  { return COALESCE; }
  "||"                  { return BOOL_OR; }
  "&&"                  { return BOOL_AND; }
  "|"                   { return OR; }
  "&"                   { return AND; }
  "^"                   { return XOR; }
  "!="                  { return NOT_EQUAL; }
  "*"                   { return MULTIPLY; }
  "+"                   { return PLUS; }
  "-"                   { return MINUS; }
  "/"                   { return DIV; }
  "%"                   { return MOD; }
  "!"                   { return NOT; }
  "~"                   { return BITWISE_NOT; }
  "++"                  { return INCREMENT; }
  "--"                  { return DECREMENT; }
  "->"                  { return POINTER_ACCESS; }
  "@\""                 { return TEMPLATE_START; }
  "\""                  { return QUOT; }
  "=>"                  { return LAMBDA; }
  "..."                 { return ELIPSIS; }
  "global::"            { return GLOBAL_NS; }
  "abstract"            { return KW_ABSTRACT; }
  "as"                  { return KW_AS; }
  "async"               { return KW_ASYNC; }
  "base"                { return KW_BASE; }
  "break"               { return KW_BREAK; }
  "case"                { return KW_CASE; }
  "catch"               { return KW_CATCH; }
  "class"               { return KW_CLASS; }
  "const"               { return KW_CONST; }
  "construct"           { return KW_CONSTRUCT; }
  "continue"            { return KW_CONTINUE; }
  "default"             { return KW_DEFAULT; }
  "delegate"            { return KW_DELEGATE; }
  "delete"              { return KW_DELETE; }
  "do"                  { return KW_DO; }
  "dynamic"             { return KW_DYNAMIC; }
  "else"                { return KW_ELSE; }
  "ensures"             { return KW_ENSURES; }
  "enum"                { return KW_ENUM; }
  "errordomain"         { return KW_ERRORDOMAIN; }
  "extern"              { return KW_EXTERN; }
  "false"               { return KW_FALSE; }
  "finally"             { return KW_FINALLY; }
  "for"                 { return KW_FOR; }
  "foreach"             { return KW_FOREACH; }
  "get"                 { return KW_GET; }
  "if"                  { return KW_IF; }
  "in"                  { return KW_IN; }
  "inline"              { return KW_INLINE; }
  "interface"           { return KW_INTERFACE; }
  "internal"            { return KW_INTERNAL; }
  "is"                  { return KW_IS; }
  "lock"                { return KW_LOCK; }
  "namespace"           { return KW_NAMESPACE; }
  "new"                 { return KW_NEW; }
  "null"                { return KW_NULL; }
  "out"                 { return KW_OUT; }
  "override"            { return KW_OVERRIDE; }
  "owned"               { return KW_OWNED; }
  "params"              { return KW_PARAMS; }
  "private"             { return KW_PRIVATE; }
  "protected"           { return KW_PROTECTED; }
  "public"              { return KW_PUBLIC; }
  "ref"                 { return KW_REF; }
  "requires"            { return KW_REQUIRES; }
  "return"              { return KW_RETURN; }
  "set"                 { return KW_SET; }
  "signal"              { return KW_SIGNAL; }
  "sizeof"              { return KW_SIZEOF; }
  "static"              { return KW_STATIC; }
  "struct"              { return KW_STRUCT; }
  "switch"              { return KW_SWITCH; }
  "this"                { return KW_THIS; }
  "throw"               { return KW_THROW; }
  "throws"              { return KW_THROWS; }
  "true"                { return KW_TRUE; }
  "try"                 { return KW_TRY; }
  "typeof"              { return KW_TYPEOF; }
  "using"               { return KW_USING; }
  "var"                 { return KW_VAR; }
  "virtual"             { return KW_VIRTUAL; }
  "void"                { return KW_VOID; }
  "while"               { return KW_WHILE; }
  "yield"               { return KW_YIELD; }
  ""                    { return VERBATIM_LITERAL; }
  "KW_UNOWNED"          { return KW_UNOWNED; }
  "KW_WEAK"             { return KW_WEAK; }
  "TEMPLATE_LITERAL"    { return TEMPLATE_LITERAL; }

  {HEX}                 { return HEX; }
  {IDENTIFIER}          { return IDENTIFIER; }
  {INTEGER}             { return INTEGER; }
  {REAL}                { return REAL; }
  {CHAR}                { return CHAR; }
//  {REGEX_LITERAL}       { return REGEX_LITERAL; }

  [^] { return com.intellij.psi.TokenType.BAD_CHARACTER; }
}
