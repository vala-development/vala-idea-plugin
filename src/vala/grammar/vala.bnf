{
    generate=[names="long"]

    parserClass="vala.grammar.parser.ValaGrammarParser"
    parserUtilClass="vala.grammar.parser.ValaParserUtil"

    implements="vala.grammar.parser.psi.ValaCompositeElement"
    extends="vala.grammar.parser.psi.impl.ValaCompositeElementImpl"

    psiClassPrefix="Bnf"
    psiImplClassSuffix="Impl"
    psiPackage="vala.grammar.parser.psi"
    psiImplPackage="vala.grammar.parser.psi.impl"
    psiImplUtilClass="vala.grammar.parser.psi.impl.GrammarPsiImplUtil"

    elementTypeHolderClass="vala.grammar.parser.psi.ValaTypes"
    elementTypePrefix=""
    elementTypeClass="vala.grammar.parser.psi.ValaElementType"
    tokenTypeClass="vala.grammar.parser.psi.ValaTokenType"

    tokens = [
        DOT = "."
        COMMA = ","
        COLON = ":"
        SEMICOLON = ";"
        SHIFT_LEFT = "<<"
        SHIFT_RIGHT = ">>"
        LBRACKET = "["
        RBRACKET = "]"
        LPAREN = "("
        RPAREN = ")"
        LCURL = "{"
        RCURL = "}"
        ASSIGNMENT = "="
        INCR_ASSIGN = "+="
        DECR_ASSIGN = "-="
        OR_ASSIGN = "|="
        AND_ASSIGN = "&="
        XOR_ASSIGN = "^="
        DIV_ASSIGN = "/="
        MUL_ASSIGN = "*="
        MOD_ASSIGN = "%="
        SHL_ASSIGN = "<<="
        SHR_ASSIGN = ">>="
        GTEQ = ">="
        GT = ">"
        LTEQ = "<="
        LT = "<"
        EQUAL = "=="
        QUESTION = "?"
        COALESCE = "??"
        BOOL_OR = "||"
        BOOL_AND = "&&"
        OR = "|"
        AND = "&"
        XOR = "^"
        NOT_EQUAL = "!="
        MULTIPLY = "*"
        PLUS = "+"
        MINUS = "-"
        DIV = "/"
        MOD = "%"
        NOT = "!"
        BITWISE_NOT = "~"
        INCREMENT = "++"
        DECREMENT = "--"
        POINTER_ACCESS = "->"
        TEMPLATE_START = '@"'
        QUOT = '"'
        LAMBDA = "=>"
        ELIPSIS = "..."

        GLOBAL_NS = "global::"

        KW_ABSTRACT = "abstract"
        KW_AS = "as"
        KW_ASYNC = "async"
        KW_BASE = "base"
        KW_BREAK = "break"
        KW_CASE = "case"
        KW_CATCH = "catch"
        KW_CLASS = "class"
        KW_CONST = "const"
        KW_CONSTRUCT = "construct"
        KW_CONTINUE = "continue"
        KW_DEFAULT = "default"
        KW_DELEGATE = "delegate"
        KW_DELETE = "delete"
        KW_DO = "do"
        KW_DYNAMIC = "dynamic"
        KW_ELSE = "else"
        KW_ENSURES = "ensures"
        KW_ENUM = "enum"
        KW_ERRORDOMAIN = "errordomain"
        KW_EXTERN = "extern"
        KW_FALSE = "false"
        KW_FINALLY = "finally"
        KW_FOR = "for"
        KW_FOREACH = "foreach"
        KW_GET = "get"
        KW_IF = "if"
        KW_IN = "in"
        KW_INLINE = "inline"
        KW_INTERFACE = "interface"
        KW_INTERNAL = "internal"
        KW_IS = "is"
        KW_LOCK = "lock"
        KW_NAMESPACE = "namespace"
        KW_NEW = "new"
        KW_NULL = "null"
        KW_OUT = "out"
        KW_OVERRIDE = "override"
        KW_OWNED = "owned"
        KW_PARAMS = "params"
        KW_PRIVATE = "private"
        KW_PROTECTED = "protected"
        KW_PUBLIC = "public"
        KW_REF = "ref"
        KW_REQUIRES = "requires"
        KW_RETURN = "return"
        KW_SET = "set"
        KW_SIGNAL = "signal"
        KW_SIZEOF = "sizeof"
        KW_STATIC = "static"
        KW_STRUCT = "struct"
        KW_SWITCH = "switch"
        KW_THIS = "this"
        KW_THROW = "throw"
        KW_THROWS = "throws"
        KW_TRUE = "true"
        KW_TRY = "try"
        KW_TYPEOF = "typeof"
        KW_USING = "using"
        KW_VAR = "var"
        KW_VIRTUAL = "virtual"
        KW_VOID = "void"
        KW_WHILE = "while"
        KW_YIELD = "yield"

        HEX = "regexp:0x[0-9a-fA-F]+"
        IDENTIFIER = 'regexp:(@[a-zA-Z0-9_]+)|([a-zA-Z_][a-zA-Z0-9_]*)'
        INTEGER = 'regexp:[0-9]+'
        REAL = 'regexp:([0-9]+)(\.[0-9]?)[dDfF]?'
        CHAR = "regexp:\'[a-zA-Z0-9]\'"
        REGEX_LITERAL = "regexp:/[^/]*/[gmi]*"
        STRING = 'regexp:\"[^\"]*\"'
        TEMPLATE_LITERAL = ""
        VERBATIM_LITERAL = ""
        LINE_COMMENT="regexp://.*"
        BLOCK_COMMENT="regexp:/\*(.|\n)*\*/"
    ]
}

input ::= using_directive* namespace_member*

using_directive ::= KW_USING symbol (COMMA symbol)* SEMICOLON

symbol ::= symbol_part (DOT symbol_part)*

symbol_part ::= ( GLOBAL_NS IDENTIFIER ) | IDENTIFIER

namespace_member ::= attributes?
                     ( namespace_declaration |
                       class_declaration |
                       interface_declaration |
                       struct_declaration |
                       enum_declaration |
                       errordomain_declaration |
                       method_declaration |
                       delegate_declaration |
                       field_declaration |
                       constant_declaration )

attributes ::= attribute*

attribute ::= LBRACKET IDENTIFIER attribute_arguments? RBRACKET

attribute_arguments ::= LPAREN (attribute_argument (COMMA attribute_argument)*)? RPAREN

attribute_argument ::= IDENTIFIER ASSIGNMENT expression

expression ::= lambda_expression | ( conditional_expression (assignment_operator expression)? )

assignment_operator ::= ASSIGNMENT | INCR_ASSIGN | DECR_ASSIGN | OR_ASSIGN | AND_ASSIGN | XOR_ASSIGN |
                        DIV_ASSIGN | MUL_ASSIGN | MOD_ASSIGN | SHL_ASSIGN | SHR_ASSIGN

conditional_expression ::= coalescing_expression (QUESTION expression COLON expression)?

coalescing_expression ::= conditional_or_expression (COALESCE coalescing_expression)?

conditional_or_expression ::= conditional_and_expression (BOOL_OR conditional_and_expression)*

conditional_and_expression ::= in_expression (BOOL_AND in_expression)*

in_expression ::= inclusive_or_expression (KW_IN inclusive_or_expression)?

inclusive_or_expression ::= exclusive_or_expression (OR exclusive_or_expression)*

exclusive_or_expression ::= and_expression (XOR and_expression)?

and_expression ::= equality_expression (AND equality_expression)*

equality_expression ::= relational_expression (( EQUAL | NOT_EQUAL ) relational_expression)*

relational_expression ::= shift_expression ( (( LT | LTEQ | GT | GTEQ ) shift_expression ) |
                                             ( KW_IS type ) | ( KW_AS type ) )*

type ::= ( KW_VOID (MULTIPLY)* ) |
        ( (KW_DYNAMIC)? (KW_OWNED | KW_UNOWNED | KW_WEAK)? symbol
            (type_arguments)? (MULTIPLY)* (QUESTION)? (array_type)* )

array_type ::= LBRACKET (array_size)? RBRACKET (QUESTION)?

shift_expression ::= additive_expression ( ( SHIFT_LEFT | SHIFT_RIGHT ) additive_expression )*

additive_expression ::= multiplicative_expression ( ( PLUS | MINUS ) multiplicative_expression )*

multiplicative_expression ::= unary_expression ( ( MULTIPLY | DIV | MOD ) unary_expression )*

unary_expression ::= ( unary_operator unary_expression ) | primary_expression

unary_operator ::= PLUS | MINUS | NOT | BITWISE_NOT | INCREMENT | DECREMENT | MULTIPLY | AND |
                    cast_operator | LPAREN NOT RPAREN

cast_operator ::= LPAREN type RPAREN | LPAREN KW_UNOWNED RPAREN | LPAREN KW_OWNED RPAREN | LPAREN KW_WEAK RPAREN

primary_expression ::= ( literal | initializer | tuple | template | REGEX_LITERAL | this_access | base_access |
                       object_or_array_creation_expression | yield_expression | sizeof_expression | typeof_expression |
                       simple_name )
                       ( member_access | pointer_member_access | method_call | element_access |
                         post_increment_expression | post_decrement_expression )*

member_access ::= DOT IDENTIFIER (type_arguments)?

post_increment_expression ::= INCREMENT;

post_decrement_expression ::= DECREMENT;

element_access ::= LBRACKET expression (slice_array)? RBRACKET;

slice_array ::= COLON expression;

pointer_member_access ::= POINTER_ACCESS IDENTIFIER (type_arguments)?

literal ::= KW_TRUE | KW_FALSE | KW_NULL | INTEGER | REAL | HEX | CHAR | REGEX_LITERAL |
            STRING | TEMPLATE_LITERAL | VERBATIM_LITERAL

initializer ::= LCURL (argument (COMMA argument)*)? RCURL

arguments ::= argument (COMMA argument)*

argument ::= (IDENTIFIER ":")? (KW_REF | KW_OUT)? expression

tuple ::= LPAREN  expression (COMMA expression)* RPAREN

template ::= TEMPLATE_START (expression COMMA)* QUOT

this_access ::= KW_THIS

base_access ::= KW_BASE

object_or_array_creation_expression ::= KW_NEW member (object_creation_expression | array_creation_expression)

object_creation_expression ::= LPAREN arguments? RPAREN object_initializer?

object_initializer ::= LCURL member_initializer (COMMA member_initializer)? RCURL

member_initializer ::= IDENTIFIER ASSIGNMENT expression

array_creation_expression ::= (LBRACKET RBRACKET)* (LBRACKET array_size? RBRACKET)? initializer?

array_size ::= expression (COMMA expression)*

member ::= (GLOBAL_NS IDENTIFIER | IDENTIFIER) type_arguments? (member_access)*

type_arguments ::= LT type (COMMA type)* GT

yield_expression ::= KW_YIELD (base_access DOT)? member method_call

method_call ::= LPAREN (arguments)? RPAREN (object_initializer)?

sizeof_expression ::= KW_SIZEOF LPAREN type RPAREN

typeof_expression ::= KW_TYPEOF LPAREN type RPAREN

simple_name ::= (GLOBAL_NS IDENTIFIER | IDENTIFIER) (type_arguments)?

lambda_expression ::= lambda_expression_params LAMBDA lambda_expression_body

lambda_expression_params ::= IDENTIFIER | (LPAREN (IDENTIFIER (COMMA IDENTIFIER)*)? RPAREN)

lambda_expression_body ::= expression | block

member_declaration_modifiers ::= member_declaration_modifier+

member_declaration_modifier ::= KW_ASYNC | KW_CLASS | KW_EXTERN | KW_INLINE | KW_STATIC
                        | KW_ABSTRACT | KW_VIRTUAL | KW_OVERRIDE | KW_NEW

constructor_declaration ::= (constructor_declaration_modifiers)? KW_CONSTRUCT block

constructor_declaration_modifiers ::= constructor_declaration_modifier+

constructor_declaration_modifier ::= KW_ASYNC | KW_CLASS | KW_EXTERN | KW_INLINE | KW_STATIC
                        | KW_ABSTRACT | KW_VIRTUAL | KW_OVERRIDE

destructor_declaration ::= (constructor_declaration_modifiers)? BITWISE_NOT IDENTIFIER LPAREN RPAREN block

class_declaration ::= (access_modifier)? (type_declaration_modifiers)? KW_CLASS symbol (type_arguments)? (COLON base_types)?
                    LCURL (class_member)* RCURL

base_types ::= type (COMMA type)*

class_member ::= (attributes)? ( class_declaration | struct_declaration | enum_declaration
                | delegate_declaration | method_declaration | signal_declaration | field_declaration
                | constant_declaration | property_declaration | constructor_declaration
                | creation_method_declaration | destructor_declaration )

access_modifier ::= KW_PRIVATE | KW_PROTECTED | KW_INTERNAL | KW_PUBLIC

type_declaration_modifiers ::= type_declaration_modifier+

type_declaration_modifier ::= KW_ABSTRACT | KW_EXTERN | KW_STATIC

enum_declaration ::= (access_modifier)? (type_declaration_modifiers)? KW_ENUM symbol
                LCURL (enum_member)* RCURL

enum_member ::= (attributes? (method_declaration | constant_declaration)) | enum_values

enum_values ::= enum_value (COMMA enum_value)* COMMA? SEMICOLON?

enum_value ::= (attributes)? IDENTIFIER (ASSIGNMENT expression)?

errordomain_declaration ::= (access_modifier)? (type_declaration_modifiers)? KW_ERRORDOMAIN symbol
                LCURL errorcodes (SEMICOLON (method_declaration)*)? RCURL

errorcodes ::= errorcode (COMMA errorcode)* COMMA?

errorcode ::= (attributes)? IDENTIFIER (ASSIGNMENT expression)?

interface_declaration ::= access_modifier? type_declaration_modifiers? KW_INTERFACE symbol type_parameters? (COLON base_types)?
                LCURL (interface_member)* RCURL

type_parameters ::= LT IDENTIFIER (COMMA IDENTIFIER)* GT

interface_member ::= attributes? (class_declaration | struct_declaration | enum_declaration
            | delegate_declaration | method_declaration | signal_declaration | field_declaration
            | constant_declaration | property_declaration)

namespace_declaration ::= KW_NAMESPACE symbol LCURL (using_directive)* (namespace_member)* RCURL

struct_declaration ::= access_modifier? type_declaration_modifiers? KW_STRUCT symbol (COLON base_types)?
            LCURL (struct_member)* RCURL

struct_member ::= attributes? (method_declaration | field_declaration | constant_declaration
            | property_declaration | creation_method_declaration)

creation_method_declaration ::= access_modifier? constructor_declaration_modifiers? symbol parameters
            throws_part? requires_decl? ensures_decl? (SEMICOLON | block)

parameters ::= LPAREN parameters_decl? RPAREN

parameters_decl ::= parameter (COMMA parameter)*

parameter ::= attributes? (ELIPSIS | (KW_PARAMS? (KW_OWNED | KW_UNOWNED)? (KW_REF | KW_OUT)?
            type IDENTIFIER (ASSIGNMENT expression)?))

throws_part ::= KW_THROWS type (COMMA type)*

requires_decl ::= KW_REQUIRES LPAREN expression RPAREN (requires_decl)?

ensures_decl ::= KW_ENSURES LPAREN expression RPAREN (ensures_decl)?

delegate_declaration ::= access_modifier? delegate_declaration_modifiers? KW_DELEGATE type symbol
                type_parameters? parameters throws_part? SEMICOLON

delegate_declaration_modifiers ::= delegate_declaration_modifier+

delegate_declaration_modifier ::= KW_ASYNC | KW_CLASS | KW_EXTERN | KW_INLINE | KW_ABSTRACT
            | KW_VIRTUAL | KW_OVERRIDE | KW_STATIC

signal_declaration ::= access_modifier? signal_declaration_modifiers? KW_SIGNAL type IDENTIFIER
            parameters (SEMICOLON | block)

signal_declaration_modifiers ::= signal_declaration_modifier+

signal_declaration_modifier ::= KW_ASYNC | KW_EXTERN | KW_INLINE | KW_ABSTRACT | KW_VIRTUAL
            | KW_OVERRIDE | KW_NEW

method_declaration ::= access_modifier? member_declaration_modifiers? type IDENTIFIER
            type_parameters? parameters throws_part? requires_decl? ensures_decl?
            (SEMICOLON | block)

constant_declaration ::= access_modifier? member_declaration_modifiers? KW_CONST type
            IDENTIFIER inline_array_type? (ASSIGNMENT expression)? SEMICOLON

inline_array_type ::= LBRACKET INTEGER RBRACKET

field_declaration ::= access_modifier? member_declaration_modifiers? type IDENTIFIER
            (LBRACKET RBRACKET)? (ASSIGNMENT expression)? SEMICOLON

property_declaration ::= access_modifier? property_declaration_modifiers? type IDENTIFIER
            LCURL property_declaration_part* RCURL

property_declaration_part ::= (KW_DEFAULT ASSIGNMENT expression SEMICOLON) | property_accessor

property_accessor ::= attributes? access_modifier? (KW_OWNED | KW_UNOWNED)?
            ((property_get_accessor property_set_construct_accessor)
            | (property_set_construct_accessor property_get_accessor)
            | (property_set_construct_accessor) | (property_get_accessor))

property_get_accessor ::= access_modifier? KW_GET (SEMICOLON | block)

property_set_construct_accessor ::= (access_modifier? ((KW_SET KW_CONSTRUCT?) | KW_CONSTRUCT | (KW_CONSTRUCT KW_SET)))
            (SEMICOLON | block)

property_declaration_modifiers ::= property_declaration_modifier+

property_declaration_modifier ::= KW_CLASS | KW_STATIC | KW_EXTERN | KW_INLINE | KW_ABSTRACT
            | KW_VIRTUAL | KW_OVERRIDE | KW_NEW

block ::= LCURL statement* RCURL

statement ::= block | SEMICOLON | if_statement | switch_statement | while_statement
            | do_statement | for_statement | foreach_statement | break_statement
            | continue_statement | return_statement | yield_statement | throw_statement
            | try_statement | lock_statement | delete_statement | local_variable_declarations
            | expression_statement

if_statement ::= KW_IF LPAREN expression RPAREN embedded_statement
            (KW_ELSE embedded_statement)?

embedded_statement ::= block | embedded_statement_without_block

embedded_statement_without_block ::= SEMICOLON | if_statement | switch_statement | while_statement
        | do_statement | for_statement | foreach_statement | break_statement | continue_statement
        | return_statement | yield_statement | throw_statement | try_statement | lock_statement
        | delete_statement | expression_statement

switch_statement ::= KW_SWITCH LPAREN expression RPAREN LCURL (switch_section)* RCURL

switch_section ::= ((KW_CASE expression) | KW_DEFAULT) COLON statement* KW_BREAK?

while_statement ::= KW_WHILE LPAREN expression RPAREN embedded_statement

do_statement ::= KW_DO embedded_statement KW_WHILE LPAREN expression RPAREN SEMICOLON

for_statement ::= KW_FOR LPAREN (SEMICOLON | for_initializer) expression? SEMICOLON for_iterator? RPAREN embedded_statement

for_initializer ::= local_variable_declarations | (statement_expression (COMMA statement_expression)* SEMICOLON)

for_iterator ::= statement_expression (COMMA statement_expression)*

statement_expression ::= expression

foreach_statement ::= KW_FOREACH LPAREN (type | KW_VAR) IDENTIFIER KW_IN expression RPAREN embedded_statement

break_statement ::= KW_BREAK SEMICOLON

continue_statement ::= KW_CONTINUE SEMICOLON

return_statement ::= KW_RETURN expression? SEMICOLON

yield_statement ::= KW_YIELD (expression_statement | KW_RETURN expression)? SEMICOLON

throw_statement ::= KW_THROW expression SEMICOLON

try_statement ::= KW_TRY block catch_clause* finally_clause?

catch_clause ::= KW_CATCH (LPAREN type IDENTIFIER RPAREN)? block

finally_clause ::= KW_FINALLY block

lock_statement ::= KW_LOCK LPAREN expression RPAREN embedded_statement

delete_statement ::= KW_DELETE expression SEMICOLON

local_variable_declarations ::= (KW_VAR | type) local_variable_declaration (COMMA local_variable_declaration)* SEMICOLON

local_variable_declaration ::= local_tuple_declaration | local_variable

local_tuple_declaration ::= LPAREN IDENTIFIER (COMMA IDENTIFIER)* RPAREN ASSIGNMENT expression

local_variable ::= IDENTIFIER inline_array_type? (ASSIGNMENT expression)?

expression_statement ::= statement_expression SEMICOLON
