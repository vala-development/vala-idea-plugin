package vala.grammar.parser.psi.impl;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NotNull;
import vala.grammar.parser.psi.ValaCompositeElement;

public class ValaCompositeElementImpl extends ASTWrapperPsiElement implements ValaCompositeElement {

    public ValaCompositeElementImpl(@NotNull ASTNode node) {
        super(node);
    }

    @Override
    public IElementType getTokenType() {
        return getNode().getElementType();
    }
}
