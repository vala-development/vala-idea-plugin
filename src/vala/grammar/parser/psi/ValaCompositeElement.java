package vala.grammar.parser.psi;

import com.intellij.psi.NavigatablePsiElement;
import com.intellij.psi.tree.IElementType;

public interface ValaCompositeElement extends NavigatablePsiElement {
    IElementType getTokenType();
}