package vala.grammar.parser.psi;

import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import vala.ValaLanguage;

public class ValaTokenType extends IElementType {
    public ValaTokenType(@NotNull @NonNls String debugName) {
        super(debugName, ValaLanguage.INSTANCE);
    }

    @Override
    public String toString() {
        return "ValaTokenType." + super.toString();
    }
}