package vala.configuration;

import com.intellij.execution.ui.CommonProgramParametersPanel;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleManager;
import com.intellij.openapi.module.ModuleType;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.options.SettingsEditor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.projectRoots.Sdk;
import com.intellij.openapi.projectRoots.SdkAdditionalData;
import com.intellij.openapi.projectRoots.SdkType;
import com.intellij.openapi.projectRoots.SdkTypeId;
import com.intellij.openapi.roots.ModuleRootManager;
import com.intellij.openapi.ui.LabeledComponent;
import com.intellij.ui.ListCellRendererWrapper;
import com.intellij.ui.PanelWithAnchor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import vala.compiler.ValaCommandLineUtils;
import vala.module.ValaModuleComponent;
import vala.module.ValaModuleType;
import vala.sdk.ValaSdkData;
import vala.sdk.ValaSdkPlatformData;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ValaRunSettingsEditor extends SettingsEditor<ValaRunConfiguration> implements PanelWithAnchor {
    private final Project project;

    private CommonProgramParametersPanel parametersPanel;
    private JPanel pnlMain;
    private JComboBox cmbModule;
    private JComboBox cmbPlatform;
    private JRadioButton rbRelease;
    private JRadioButton rbDebug;

    private JComponent anchor;

    public ValaRunSettingsEditor(Project project) {
        this.project = project;

        cmbModule.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                parametersPanel.setModuleContext((Module)cmbModule.getSelectedItem());
            }
        });
        cmbModule.setRenderer(new ListCellRendererWrapper() {
            @Override
            public void customize(JList list, Object value, int index, boolean selected, boolean hasFocus) {
                if (value instanceof Module) {
                    final Module module = (Module)value;
                    setText(module.getName());
                }
            }
        });

        cmbPlatform.setRenderer(new ListCellRendererWrapper() {
            @Override
            public void customize(JList list, Object value, int index, boolean selected, boolean hasFocus) {
                if (value instanceof ValaSdkPlatformData) {
                    final ValaSdkPlatformData platformData = (ValaSdkPlatformData)value;
                    setText(platformData.PLATFORM + " / " + platformData.ARCH);
                }
            }
        });
    }

    @Override
    protected void resetEditorFrom(ValaRunConfiguration configuration) {
        parametersPanel.reset(configuration);

        Module selectedModule = configuration.getConfigurationModule().getModule();
        cmbModule.removeAll();
        final Module[] modules = ModuleManager.getInstance(configuration.getProject()).getModules();
        for (final Module module : modules) {
            if (ModuleType.get(module) == ValaModuleType.getInstance()) {
                ValaModuleComponent component = ValaModuleComponent.getInstance(module);
                if (component.getModuleType() == ValaModuleComponent.ModuleType.APPLICATION ||
                    component.getModuleType() == ValaModuleComponent.ModuleType.CONSOLE_APPLICATION) {
                    cmbModule.addItem(module);
                }
            }
        }
        cmbModule.setSelectedItem(selectedModule);

        ValaSdkPlatformData selectedPlatformData = null;
        String homePath = ModuleRootManager.getInstance(selectedModule).getSdk().getHomePath();
        ValaSdkData sdkData = ValaCommandLineUtils.getSdkData(homePath);
        cmbPlatform.removeAll();
        if (sdkData != null)
        {
            for (ValaSdkPlatformData platformData : sdkData.platforms) {
                cmbPlatform.addItem(platformData);

                if (platformData.PLATFORM.equals(configuration.getPlatform()) &&
                    platformData.ARCH.equals(configuration.getArch())) {
                    selectedPlatformData = platformData;
                }
            }
        }
        cmbPlatform.setSelectedItem(selectedPlatformData);

        rbRelease.setSelected(!configuration.getIsDebug());
        rbDebug.setSelected(configuration.getIsDebug());
    }

    @Override
    protected void applyEditorTo(ValaRunConfiguration configuration) throws ConfigurationException {
        parametersPanel.applyTo(configuration);
        configuration.setModule((Module)cmbModule.getSelectedItem());
        configuration.setPlatform(((ValaSdkPlatformData)cmbPlatform.getSelectedItem()).PLATFORM);
        configuration.setArch(((ValaSdkPlatformData)cmbPlatform.getSelectedItem()).ARCH);
        configuration.setIsDebug(rbDebug.isSelected());
    }

    @NotNull
    @Override
    protected JComponent createEditor() {
        return pnlMain;
    }

    @Override
    protected void disposeEditor() {
        super.disposeEditor();
        pnlMain.setVisible(false);
    }

    @Override
    public JComponent getAnchor() {
        return anchor;
    }

    @Override
    public void setAnchor(@Nullable JComponent anchor) {
        this.anchor = anchor;
        parametersPanel.setAnchor(anchor);
    }

    private void createUIComponents() {
    }
}
