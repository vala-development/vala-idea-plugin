package vala.configuration;

import com.intellij.execution.configurations.RunConfigurationModule;
import com.intellij.openapi.project.Project;

public class ValaModuleBasedConfiguration extends RunConfigurationModule {
    public ValaModuleBasedConfiguration(Project project) {
        super(project);
    }
}
