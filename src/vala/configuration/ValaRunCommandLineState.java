package vala.configuration;

import com.intellij.execution.ExecutionException;
import com.intellij.execution.configurations.CommandLineState;
import com.intellij.execution.process.ProcessHandler;
import com.intellij.execution.runners.ExecutionEnvironment;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;

public class ValaRunCommandLineState extends CommandLineState {
    private final Project project;
    private final ValaRunConfiguration valaConfiguration;

    public ValaRunCommandLineState(Project project, ExecutionEnvironment env, ValaRunConfiguration valaConfiguration) {
        super(env);

        this.project = project;
        this.valaConfiguration = valaConfiguration;
    }

    @NotNull
    @Override
    protected ProcessHandler startProcess() throws ExecutionException {
        return null;
    }
}
