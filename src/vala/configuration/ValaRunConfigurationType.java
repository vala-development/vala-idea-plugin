package vala.configuration;

import com.intellij.execution.configurations.ConfigurationFactory;
import com.intellij.execution.configurations.ConfigurationType;
import com.intellij.execution.configurations.RunConfiguration;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;
import vala.ValaIcons;

import javax.swing.*;

public class ValaRunConfigurationType implements ConfigurationType {
    public static final String ID = "ValaRunConfigurationType";
    private final ConfigurationFactory configurationFactory;

    public ValaRunConfigurationType() {
        configurationFactory = new ValaConfigurationFactory(this);
    }

    @Override
    public String getDisplayName() {
        return "Vala";
    }

    @Override
    public String getConfigurationTypeDescription() {
        return "Vala";
    }

    @Override
    public Icon getIcon() {
        return ValaIcons.VALA_ICON_16;
    }

    @NotNull
    @Override
    public String getId() {
        return ID;
    }

    @Override
    public ConfigurationFactory[] getConfigurationFactories() {
        return new ConfigurationFactory[] { configurationFactory };
    }

    private class ValaConfigurationFactory extends ConfigurationFactory {
        protected ValaConfigurationFactory(@NotNull ConfigurationType type) {
            super(type);
        }

        @Override
        public RunConfiguration createTemplateConfiguration(Project project) {
            return new ValaRunConfiguration(project, this);
        }
    }
}
