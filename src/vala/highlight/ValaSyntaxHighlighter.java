package vala.highlight;

import com.intellij.lexer.FlexAdapter;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.editor.HighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.tree.TokenSet;
import org.jetbrains.annotations.NotNull;
import vala.grammar.lexer.ValaLexer;
import vala.grammar.parser.psi.ValaTypes;

import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

public class ValaSyntaxHighlighter extends SyntaxHighlighterBase {
    private static final Map<IElementType, TextAttributesKey> keys;

    public static class TokenGroups {
        public static final TokenSet KEYWORDS = TokenSet.create(ValaTypes.KW_ABSTRACT,
                ValaTypes.KW_AS, ValaTypes.KW_ASYNC, ValaTypes.KW_BASE, ValaTypes.KW_BREAK,
                ValaTypes.KW_CASE, ValaTypes.KW_CATCH, ValaTypes.KW_CLASS, ValaTypes.KW_CONST,
                ValaTypes.KW_CONSTRUCT, ValaTypes.KW_CONTINUE, ValaTypes.KW_DEFAULT, ValaTypes.KW_DELEGATE,
                ValaTypes.KW_DELETE, ValaTypes.KW_DO, ValaTypes.KW_DYNAMIC, ValaTypes.KW_ELSE,
                ValaTypes.KW_ENSURES, ValaTypes.KW_ENUM, ValaTypes.KW_ERRORDOMAIN, ValaTypes.KW_EXTERN,
                ValaTypes.KW_FALSE, ValaTypes.KW_FINALLY, ValaTypes.KW_FOR, ValaTypes.KW_FOREACH,
                ValaTypes.KW_GET, ValaTypes.KW_IF, ValaTypes.KW_IN, ValaTypes.KW_INLINE,
                ValaTypes.KW_INTERFACE, ValaTypes.KW_INTERNAL, ValaTypes.KW_IS, ValaTypes.KW_LOCK,
                ValaTypes.KW_NAMESPACE, ValaTypes.KW_NEW, ValaTypes.KW_NULL, ValaTypes.KW_OUT,
                ValaTypes.KW_OVERRIDE, ValaTypes.KW_OWNED, ValaTypes.KW_PARAMS, ValaTypes.KW_PRIVATE,
                ValaTypes.KW_PROTECTED, ValaTypes.KW_PUBLIC, ValaTypes.KW_REF, ValaTypes.KW_REQUIRES,
                ValaTypes.KW_RETURN, ValaTypes.KW_SET, ValaTypes.KW_SIGNAL, ValaTypes.KW_SIZEOF,
                ValaTypes.KW_STATIC, ValaTypes.KW_STRUCT, ValaTypes.KW_SWITCH, ValaTypes.KW_THIS,
                ValaTypes.KW_THROW, ValaTypes.KW_THROWS, ValaTypes.KW_TRUE, ValaTypes.KW_TRY,
                ValaTypes.KW_TYPEOF, ValaTypes.KW_UNOWNED, ValaTypes.KW_USING, ValaTypes.KW_VAR,
                ValaTypes.KW_VIRTUAL, ValaTypes.KW_VOID, ValaTypes.KW_WEAK, ValaTypes.KW_WHILE,
                ValaTypes.KW_YIELD);

        public static final TokenSet NUMBERS = TokenSet.create(ValaTypes.INTEGER,
                ValaTypes.REAL, ValaTypes.HEX);

        public static final TokenSet STRINGS = TokenSet.create(ValaTypes.STRING,
                ValaTypes.LITERAL, ValaTypes.REGEX_LITERAL, ValaTypes.TEMPLATE_LITERAL,
                ValaTypes.VERBATIM_LITERAL);

        public static final TokenSet OPERATIONS = TokenSet.create(
                ValaTypes.AND, ValaTypes.AND_ASSIGN, ValaTypes.ASSIGNMENT,
                ValaTypes.BITWISE_NOT, ValaTypes.BOOL_AND, ValaTypes.BOOL_OR,
                ValaTypes.COALESCE, ValaTypes.DECREMENT, ValaTypes.DECR_ASSIGN,
                ValaTypes.DIV, ValaTypes.DIV_ASSIGN, ValaTypes.EQUAL,
                ValaTypes.GT, ValaTypes.GTEQ, ValaTypes.INCREMENT,
                ValaTypes.INCR_ASSIGN, ValaTypes.LT, ValaTypes.LTEQ,
                ValaTypes.MINUS,
                ValaTypes.MOD, ValaTypes.MOD_ASSIGN, ValaTypes.MULTIPLY,
                ValaTypes.MUL_ASSIGN, ValaTypes.NOT, ValaTypes.NOT_EQUAL,
                ValaTypes.OR, ValaTypes.OR_ASSIGN, ValaTypes.PLUS,
                ValaTypes.POINTER_ACCESS, ValaTypes.QUESTION, ValaTypes.SHIFT_LEFT,
                ValaTypes.SHIFT_RIGHT, ValaTypes.SHL_ASSIGN, ValaTypes.SHR_ASSIGN,
                ValaTypes.XOR, ValaTypes.XOR_ASSIGN);
    }

    public static class ColorGroups {
        public static final TextAttributesKey BAD_CHARACTER = TextAttributesKey.createTextAttributesKey(
                "VALA.BAD_CHARACTER", HighlighterColors.BAD_CHARACTER);

        public static final TextAttributesKey LINE_COMMENT = TextAttributesKey.createTextAttributesKey(
                "VALA.LINE_COMMENT", DefaultLanguageHighlighterColors.LINE_COMMENT);

        public static final TextAttributesKey BLOCK_COMMENT = TextAttributesKey.createTextAttributesKey(
                "VALA.BLOCK_COMMENT", DefaultLanguageHighlighterColors.BLOCK_COMMENT);

        public static final TextAttributesKey COMMA = TextAttributesKey.createTextAttributesKey(
                "VALA.COMMA", DefaultLanguageHighlighterColors.COMMA);

        public static final TextAttributesKey DOT = TextAttributesKey.createTextAttributesKey(
                "VALA.DOT", DefaultLanguageHighlighterColors.DOT);

        public static final TextAttributesKey SEMICOLON = TextAttributesKey.createTextAttributesKey(
                "VALA.SEMICOLON", DefaultLanguageHighlighterColors.SEMICOLON);

        public static final TextAttributesKey IDENTIFIER = TextAttributesKey.createTextAttributesKey(
                "VALA.IDENTIFIER", DefaultLanguageHighlighterColors.IDENTIFIER);

        public static final TextAttributesKey KEYWORDS = TextAttributesKey.createTextAttributesKey(
                "VALA.KEYWORD", DefaultLanguageHighlighterColors.KEYWORD);

        public static final TextAttributesKey NUMBER = TextAttributesKey.createTextAttributesKey(
                "VALA.NUMBER", DefaultLanguageHighlighterColors.NUMBER);

        public static final TextAttributesKey STRING = TextAttributesKey.createTextAttributesKey(
                "VALA.STRING", DefaultLanguageHighlighterColors.STRING);

        public static final TextAttributesKey OPERATIONS = TextAttributesKey.createTextAttributesKey(
                "VALA.OPERATION", DefaultLanguageHighlighterColors.OPERATION_SIGN);

        public static final TextAttributesKey BRACKETS = TextAttributesKey.createTextAttributesKey(
                "VALA.BRACKET", DefaultLanguageHighlighterColors.BRACKETS);

        public static final TextAttributesKey PARENTHESES = TextAttributesKey.createTextAttributesKey(
                "VALA.PARENTHESIS", DefaultLanguageHighlighterColors.PARENTHESES);

        public static final TextAttributesKey BRACES = TextAttributesKey.createTextAttributesKey(
                "VALA.BRACE", DefaultLanguageHighlighterColors.BRACES);
    }

    static {
        keys = new HashMap<>();

        keys.put(TokenType.BAD_CHARACTER, ColorGroups.BAD_CHARACTER);
        keys.put(ValaTypes.LINE_COMMENT, ColorGroups.LINE_COMMENT);
        keys.put(ValaTypes.BLOCK_COMMENT, ColorGroups.BLOCK_COMMENT);
        keys.put(ValaTypes.COMMA, ColorGroups.COMMA);
        keys.put(ValaTypes.DOT, ColorGroups.DOT);
        keys.put(ValaTypes.SEMICOLON, ColorGroups.SEMICOLON);
        keys.put(ValaTypes.IDENTIFIER, ColorGroups.IDENTIFIER);
        keys.put(ValaTypes.LBRACKET, ColorGroups.BRACKETS);
        keys.put(ValaTypes.RBRACKET, ColorGroups.BRACKETS);
        keys.put(ValaTypes.LPAREN, ColorGroups.PARENTHESES);
        keys.put(ValaTypes.RPAREN, ColorGroups.PARENTHESES);
        keys.put(ValaTypes.LCURL, ColorGroups.BRACES);
        keys.put(ValaTypes.RCURL, ColorGroups.BRACES);

        SyntaxHighlighterBase.fillMap(keys, TokenGroups.KEYWORDS, ColorGroups.KEYWORDS);
        SyntaxHighlighterBase.fillMap(keys, TokenGroups.NUMBERS, ColorGroups.NUMBER);
        SyntaxHighlighterBase.fillMap(keys, TokenGroups.STRINGS, ColorGroups.STRING);
        SyntaxHighlighterBase.fillMap(keys, TokenGroups.OPERATIONS, ColorGroups.OPERATIONS);
    }

    @NotNull
    @Override
    public Lexer getHighlightingLexer() {
        return new FlexAdapter(new ValaLexer((Reader) null));
    }

    @NotNull
    @Override
    public TextAttributesKey[] getTokenHighlights(final IElementType iElementType) {
        return pack(keys.get(iElementType));
    }
}
