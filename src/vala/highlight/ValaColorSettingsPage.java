package vala.highlight;

import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.options.colors.AttributesDescriptor;
import com.intellij.openapi.options.colors.ColorDescriptor;
import com.intellij.openapi.options.colors.ColorSettingsPage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import vala.ValaFileType;
import vala.ValaIcons;

import javax.swing.*;
import java.util.Map;

public class ValaColorSettingsPage implements ColorSettingsPage {
    private static final AttributesDescriptor[] DESCRIPTORS = new AttributesDescriptor[]{
            new AttributesDescriptor("Bad Character", ValaSyntaxHighlighter.ColorGroups.BAD_CHARACTER),
            new AttributesDescriptor("Block Comment", ValaSyntaxHighlighter.ColorGroups.BLOCK_COMMENT),
            new AttributesDescriptor("Line Comment", ValaSyntaxHighlighter.ColorGroups.LINE_COMMENT),
            new AttributesDescriptor("Keyword", ValaSyntaxHighlighter.ColorGroups.KEYWORDS),
            new AttributesDescriptor("Identifier", ValaSyntaxHighlighter.ColorGroups.IDENTIFIER),
            new AttributesDescriptor("Operation", ValaSyntaxHighlighter.ColorGroups.OPERATIONS),
            new AttributesDescriptor("Number", ValaSyntaxHighlighter.ColorGroups.NUMBER),
            new AttributesDescriptor("String", ValaSyntaxHighlighter.ColorGroups.STRING),
            new AttributesDescriptor("Comma", ValaSyntaxHighlighter.ColorGroups.COMMA),
            new AttributesDescriptor("Dot", ValaSyntaxHighlighter.ColorGroups.DOT),
            new AttributesDescriptor("Semicolon", ValaSyntaxHighlighter.ColorGroups.SEMICOLON),
            new AttributesDescriptor("Brace", ValaSyntaxHighlighter.ColorGroups.BRACES),
            new AttributesDescriptor("Bracket", ValaSyntaxHighlighter.ColorGroups.BRACKETS),
            new AttributesDescriptor("Parenthesis", ValaSyntaxHighlighter.ColorGroups.PARENTHESES),
    };

    @Nullable
    @Override
    public Icon getIcon() {
        return ValaIcons.VALA_ICON_16;
    }

    @NotNull
    @Override
    public SyntaxHighlighter getHighlighter() {
        return new ValaSyntaxHighlighter();
    }

    @NotNull
    @Override
    public String getDemoText() {
        return "using Gtk;\n" +
                "\n" +
                "/*\n" +
                "    New class. Block comment.\n" +
                "*/\n" +
                "public class SyncSample : Window {\n" +
                "\n" +
                "    private SpinButton spin_box;\n" +
                "    private Scale slider, array[20];\n" +
                "\n" +
                "    public SyncSample ( ) {\n" +
                "        this.title = \"Enter your age\";\n" +
                "        this.window_position = WindowPosition.CENTER;\n" +
                "        this.destroy.connect(Gtk.main_quit);\n" +
                "        set_default_size(300, 20); // line comment\n" +
                "\n" +
                "\n" +
                "        spin_box = new SpinButton.with_range (0, (130 * 2 + 1) % 55, 1);\n" +
                "        slider = new Scale.with_range (Orientation.HORIZONTAL, 0, 130, 1);\n" +
                "        spin_box.adjustment.value_changed.connect (() => {\n" +
                "            slider.adjustment.value = spin_box.adjustment.value;\n" +
                "        });\n" +
                "    }\n" +
                "}";
    }

    @Nullable
    @Override
    public Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap() {
        return null;
    }

    @NotNull
    @Override
    public AttributesDescriptor[] getAttributeDescriptors() {
        return DESCRIPTORS;
    }

    @NotNull
    @Override
    public ColorDescriptor[] getColorDescriptors() {
        return ColorDescriptor.EMPTY_ARRAY;
    }

    @NotNull
    @Override
    public String getDisplayName() {
        return "Vala";
    }
}
